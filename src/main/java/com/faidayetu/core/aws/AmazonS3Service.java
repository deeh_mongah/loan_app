package com.faidayetu.core.aws;

import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;

public interface AmazonS3Service {

    String uploadbyteArray(byte[] file, String fileName) throws IOException;

    String uploadMultipart(MultipartFile file, String fileName) throws IOException;

    String uploadFile(File file, String fileName);

    String fetchFileUrl(String fileKeyName);

    public boolean deleteFile(String filename);

}
