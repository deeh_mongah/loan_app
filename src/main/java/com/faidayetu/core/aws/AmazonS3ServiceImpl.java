/*
 * The MIT License
 *
 * Copyright 2016 Binary Limited.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package com.faidayetu.core.aws;


import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.HttpMethod;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.DeleteObjectRequest;
import com.amazonaws.services.s3.model.GeneratePresignedUrlRequest;
import com.amazonaws.services.s3.model.PutObjectRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Paths;

import static java.lang.System.err;

/**
 *
 * @category    
 * @package
 * @author      Anthony Mwawughanga
 * @version     1.0.0
 * @since       Dec 12, 2016
 *
 */

@Service
public class AmazonS3ServiceImpl implements AmazonS3Service {
    private static final Logger logger = LoggerFactory.getLogger(AmazonS3Service.class);
    @Value("${app.aws.accessKey}")
    private String accessKey;
    @Value("${app.aws.secretKey}")
    private String secretKey;
    @Value("${app.aws.bucket}")
    private String bucketName;
    @Value("${app.aws.s3Url}")
    private String s3BaseUrl;

    private AmazonS3 s3Client;

//    public AmazonS3ServiceImpl() {
//
//    }

    public String uploadbyteArray(byte[] file, String fileName) throws IOException{
        String directory = System.getProperty("java.io.tmpdir");
        String filepath = Paths.get(directory, fileName).toString();


        try (BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(new File(filepath)))) {
            stream.write(file);
        }
        File uploadFile = new File(filepath);
        err.println(">>>>>>>>>>>>>>>>>>>>byte original file name:" + uploadFile.getName()) ;

        return  uploadFile(uploadFile, fileName);
    }


    public String uploadMultipart(MultipartFile file, String fileName)throws IOException{
        String directory = System.getProperty("java.io.tmpdir");
        String filepath = Paths.get(directory, fileName).toString();

        try (BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(new File(filepath)))) {
            stream.write(file.getBytes());
        }
        File uploadFile = new File(filepath);
        return  uploadFile(uploadFile, fileName);

    }

    public String uploadFile(File file, String fileName) {

        err.println("accessKey: "+ accessKey);
        BasicAWSCredentials awsCreds = new BasicAWSCredentials(accessKey, secretKey);
         AmazonS3 s3Client = new AmazonS3Client( awsCreds );

        try {
            err.println("Uploading file to S3 Bucket");
            err.println("==================================================================");
            //Upload file
            if (s3Client.doesBucketExist(bucketName)) {
                s3Client.putObject(new PutObjectRequest(bucketName, fileName, file)
                                    .withCannedAcl( CannedAccessControlList.PublicRead )
                );
                err.println("==================================================================");
            }
        } catch (AmazonServiceException ase) {
            err.println("Request made it to Amazon Web Service but returned with an error:");
            err.println("==================================================================");
            err.println("Error Message:    " + ase.getMessage());
            err.println("HTTP Status Code: " + ase.getStatusCode());
            err.println("AWS Error Code:   " + ase.getErrorCode());
            err.println("Error Type:       " + ase.getErrorType());
            err.println("Request ID:       " + ase.getRequestId());
            err.println("==================================================================");

        } catch (AmazonClientException ace) {
            err.println("The client encountered an internal server error while performing this request:");
            err.println("==================================================================");
            err.println("Error Message: " + ace.getMessage());
            err.println("==================================================================");
        }
        //Generate file url
        if( !StringUtils.isEmpty( fileName )){
            UriComponents uriComponents = UriComponentsBuilder.newInstance()
                    .scheme("http")
                    .host( s3BaseUrl )
                    .path("/"+bucketName+"/"+fileName)
                    .build()
                    .encode();
            return uriComponents.toString();
        }
        return "";
    }
    
    public String fetchFileUrl(String fileKeyName) {
        BasicAWSCredentials awsCreds = new BasicAWSCredentials(accessKey, secretKey);
        AmazonS3 s3Client = new AmazonS3Client( awsCreds );
        GeneratePresignedUrlRequest generatePresignedUrlRequest = new GeneratePresignedUrlRequest(bucketName, fileKeyName);
        generatePresignedUrlRequest.setMethod(HttpMethod.GET);
        URL url = s3Client.generatePresignedUrl(generatePresignedUrlRequest);

        /*Return a short version of URL*/
        return url.toString();
    }


    public boolean deleteFile(String filename){
        boolean deleted = false;
        try {
            //Delete file
            if (s3Client.doesBucketExistV2( bucketName )) {
                boolean exists = s3Client.doesObjectExist(bucketName, filename);
                if( exists ) s3Client.deleteObject( new DeleteObjectRequest(bucketName, filename));

                deleted = true;
            }
        } catch (AmazonServiceException ase) {
            logger.error("Request made it to Amazon Web Service but returned with an error:");
            logger.error("==================================================================");
            logger.error("Error Message:    " + ase.getMessage());
            logger.error("HTTP Status Code: " + ase.getStatusCode());
            logger.error("AWS Error Code:   " + ase.getErrorCode());
            logger.error("Error Type:       " + ase.getErrorType());
            logger.error("Request ID:       " + ase.getRequestId());
            logger.error("==================================================================");

        } catch (AmazonClientException ace) {
            logger.error("The client encountered an internal server error while performing this request:");
            logger.error("==================================================================");
            logger.error("Error Message: " + ace.getMessage());
            logger.error("==================================================================");
        }
        return deleted;
    }

}
