package com.faidayetu.core.utils;

/**
 * Created by neshoj on 2018-05-25.
 */
public class AppConstants {
    public static final String STATUS_NEWRECORD = "0";
    public static final String STATUS_ACTIVERECORD = "1";
    public static final String STATUS_EDITEDRECORD = "2";
    public static final String STATUS_DEACTIVATED = "3";
    public static final String STATUS_INACTIVERECORD = "4";
    public static final String STATUS_DECLINED_REQUEST = "5";
    public static final String STATUS_SOFTDELETED = "6";


    public static final String STATUS_SAVED = "0";
    public static final String STATUS_PUBLISHED = "1";
}
