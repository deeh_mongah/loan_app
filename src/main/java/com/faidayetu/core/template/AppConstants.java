package com.faidayetu.core.template;

public final class AppConstants {

    public static final String MAPPED_AS_LOCATION = "0";
    public static final String MAPPED_AS_LANDMARK = "1";

    /*Maximum no of records to be fetched from the db*/
    public static final int MAX_RECORDS = 100;

    /*Bean status*/
    public static final String STATUS_NEWRECORD = "0";
    public static final String STATUS_ACTIVERECORD = "1";
    public static final String STATUS_EDITEDRECORD = "2";
    public static final String STATUS_DEACTIVATED = "3";
    public static final String STATUS_INACTIVERECORD = "4";
    public static final String STATUS_DECLINED_REQUEST = "5";
    public static final String STATUS_SOFTDELETED = "6";
    public static final String STATUS_SUSPENDED = "7";

    /*Global CRUD processing actions*/
    public static final String ACTION_EDIT = "edit";
    public static final String ACTION_APRROVE_NEW = "approve-new";
    public static final String ACTION_DECLINE_NEW = "decline-new";
    public static final String ACTION_DEACTIVATE = "deactivate";
    public static final String ACTION_VIEW_EDITCHANGES = "vedit";
    public static final String ACTION_APRROVE_EDIT = "approve-edit";
    public static final String ACTION_DECLINE_EDIT = "decline-edit";
    public static final String ACTION_VIEW_DEACTIVATION_REASON = "vdeactivation";
    public static final String ACTION_DELETE = "delete";
    public static final String ACTION_ACTIVATE = "activate";
    public static final String ACTION_APPROVE_DEACTIVATION = "approve-deactivation";
    public static final String ACTION_DECLINE_DEACTIVATION = "decline-deactivation";
    public static final String ACTION_DECLINE_REQUEST="decline-request";
    public static final String ACTION_SOFT_DELETE="soft-delete";
    public static final String ACTION_PUBLISH="publish";

//    Time Period
    public static final String INTERVAL_SECONDS = "seconds";
    public static final String INTERVAL_MINUTES = "minutes";
    public static final String INTERVAL_HOURS = "hours";
    public static final String INTERVAL_DAYS = "days";

    /*Prevent the caller of this class from instantiating this class:
     This enables the caller to reference the constants using AppConstants.CONSTANT
     */
    private AppConstants() {
        /*this prevents even the native class from calling this actor as well */
        throw new AssertionError();
    }

}
