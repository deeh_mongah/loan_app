package com.faidayetu.core.http;


import com.faidayetu.api.ApiAuthentication.AuthentationService;
import com.faidayetu.api.vm.NTML;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.sun.mail.auth.Ntlm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;

import java.util.Map;

import static org.springframework.web.util.UriComponentsBuilder.fromHttpUrl;

@Component
public class HttpFaidaService {

    @Autowired
    RestTemplate restTemplate;
    @Autowired
    ObjectMapper objectMapper;
    @Autowired
    AuthentationService authentationService;

    @Value("${faida.baseurl}")
    private String baseUrl;


    public ResponseEntity<String> postRequest(String uri,JsonNode request) throws Exception{
        Map<String, Object> userAuthorization = authentationService.findUserAuthentication(this);

        NTML ntml = new NTML();
        ntml.setUser(String.valueOf(userAuthorization.get("username")));
        ntml.setPassword(String.valueOf(userAuthorization.get("password")));
        ntml.setDomain(String.valueOf(userAuthorization.get("Domain")));

        JsonNode authNode = objectMapper.convertValue(ntml,JsonNode.class);
        ((ObjectNode) request)
                .put("auth", authNode );

        UriComponents uriComponents =
                fromHttpUrl( baseUrl )
                        .path(uri)
                        .build()
                        .encode();

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<?> httpEntity = new HttpEntity<>(request.toString(), httpHeaders);

        ResponseEntity<String> responseEntity = restTemplate.exchange(
                uriComponents.toString(),
                HttpMethod.POST,
                httpEntity,
                String.class
        );

        return responseEntity;

    }

    public ResponseEntity<?> postWithoutAuth(String uri,Object request) throws Exception{
        //Fetch Host
        /*Pack the URL for this request*/
        UriComponents uriComponents =
                fromHttpUrl( baseUrl )
                        .path( uri )
                        .build()
                        .encode();

        String jsonPayload = objectMapper.writeValueAsString( request );
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<?> httpEntity = new HttpEntity<>(jsonPayload, httpHeaders);
        ResponseEntity<String> responseEntity = restTemplate.exchange(
                uriComponents.toString(),
                HttpMethod.POST,
                httpEntity,
                String.class
        );

        return responseEntity;

    }




}
