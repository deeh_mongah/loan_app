package com.faidayetu.web.Loan;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

public interface LoanServiceInterface {


    public Map<String, Object> approveLoan(HttpServletRequest request);

    public Map<String, Object> repayLoan(HttpServletRequest request);

    public Map<String, Object> denyLoan(HttpServletRequest request);

    public Map<String,Object> warningLoan(HttpServletRequest request);
}
