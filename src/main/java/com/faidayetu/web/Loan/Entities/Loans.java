package com.faidayetu.web.Loan.Entities;

import com.faidayetu.web.usermanager.entities.Users;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;

@Entity
@Table(name = "loan_application")
public class Loans {


    public static final String NEW_LOAN = "New";
    public static final String APPROVED_LOAN = "Approved";
    public static final String DECLINED_LOAN = "Declined";
    public static final String REPAID_LOAN = "Paid";
    public static final String DUE_LOAN = "Due";
    public static final String OVERDUE_LOAN = "Overdue";

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private  Long id;

    @Column(name="MEMBERNO")
    private String memberNo;

    @Column(name = "REPAYMENT_OPTION")
    private String repaymentOption;

    @Column(name = "DISBURSEMENT_TYPE")
    private String disbursementType;

    @Column(name = "DISBURSEMENT_ACCOUNT")
    private String disbursemenrAccount;

    @Column(name = "AMOUNT")
    private BigDecimal amount;

    @Column(name = "APPLICATION_DATE")
    private Date applicationDate;

//    @Column(name = "APPLICATION_DATE_IN_MILLISECOND")
//    private Long applicationDateInMilliseconds;

    @Column(name = "status", length = 20)
    private String status;

    @Column(name = "MEMBER_NAME")
    private String memberName;

    @Column(name = "Company_Code")
    private String companyCode;

    @Column(name = "CREATED_ON")
    private Date createdOn;

    @Column(name = "LOAN_PRODUCT")
    private String loanProduct;


    @Column(name = "UPDATED_ON")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedOn;

    @Column(name = "DELETED_ON")
    @Temporal(TemporalType.TIMESTAMP)
    private Date deletedOn;


    @Column(name = "UPDATED_BY")
    private Long updatedBy;


//    @OneToOne(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
//    @JoinColumn(name = "status",updatable = false,insertable = false)
//    private Status loanStatus;


    @JoinColumn(name = "CREATED_BY", referencedColumnName = "ID", nullable = false, insertable = false, updatable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Users createdByLink;

    @JoinColumn(name = "UPDATED_BY", referencedColumnName = "ID", nullable = false, insertable = false, updatable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Users updatedByLink;



    public String getLoanProduct() {
        return loanProduct;
    }

    public void setLoanProduct(String loanProduct) {
        this.loanProduct = loanProduct;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public Date getApplicationDate() {
        return applicationDate;
    }

    public void setApplicationDate(Date applicationDate) {
        this.applicationDate = applicationDate;
    }

    public String getMemberNo() {
        return memberNo;
    }

    public void setMemberNo(String memberNo) {
        this.memberNo = memberNo;
    }

    public String getRepaymentOption() {
        return repaymentOption;
    }

    public void setRepaymentOption(String repaymentOption) {
        this.repaymentOption = repaymentOption;
    }

    public String getDisbursementType() {
        return disbursementType;
    }

    public void setDisbursementType(String disbursementType) {
        this.disbursementType = disbursementType;
    }

    public String getDisbursemenrAccount() {
        return disbursemenrAccount;
    }

    public void setDisbursemenrAccount(String disbursemenrAccount) {
        this.disbursemenrAccount = disbursemenrAccount;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    public Date getDeletedOn() {
        return deletedOn;
    }

    public void setDeletedOn(Date deletedOn) {
        this.deletedOn = deletedOn;
    }

    public Long getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Long updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Loans createdOn(Long userId){
        this.updatedOn = new Date( System.currentTimeMillis() );
        this.createdOn = new Date( System.currentTimeMillis() );
        this.updatedBy = userId;
        return this;
    }

    public Loans updatedOn(Long userId){
        this.updatedOn = new Date( System.currentTimeMillis() );
        this.updatedBy = userId;
        return this;
    }


}
