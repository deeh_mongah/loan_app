package com.faidayetu.web.Loan.Entities;

import com.faidayetu.web.usermanager.entities.Users;

import javax.persistence.*;

@Entity
@Table(name = "status")
public class Status {

    public static final String STATUS_PROCESSING = "STATUS_PROCESSING" ;
    public static final String STATUS_APPROVED_NOT_REPAID = "STATUS_APPROVED_NOT_REPAID";
    public static final String STATUS_REPAID = "STATUS_REPAID";
    public static final String STATUS_DENIED = "STATUS_DENIED";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "NAME")
    private String name;

//    @OneToOne(mappedBy = "loanStatus")
//    private Loans loans;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}
