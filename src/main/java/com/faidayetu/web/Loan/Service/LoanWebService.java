package com.faidayetu.web.Loan.Service;

import com.faidayetu.core.sms.SmsOption;
import com.faidayetu.core.sms.SmsService;
import com.faidayetu.web.Loan.Entities.Loans;
import com.faidayetu.web.Loan.LoanRepository;
import com.faidayetu.web.Loan.LoanServiceInterface;
import com.faidayetu.web.usermanager.entities.Users;
import com.faidayetu.web.usermanager.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@Service
@Transactional
public class LoanWebService implements LoanServiceInterface {

    @Autowired
    private LoanRepository loanRepo;

    @Autowired
    private UserRepository userRepo;

    @Autowired
    private SmsService smsService;

    @Override
    public Map<String, Object> approveLoan(HttpServletRequest request) {
        Map<String, Object> map = new HashMap<>();
        String index = request.getParameter("index");
        String action = request.getParameter("action");

        System.err.println(index);
        try {
            Loans loans = loanRepo.findById(Long.valueOf(index)).get();
            Users users = userRepo.findByMemberNo(loans.getMemberNo()).get();
            loans.setStatus(Loans.APPROVED_LOAN);
            String message = "Dear "+ users.getFullName() +" Your Salary advance  has been approved";
            SmsOption smsOption = new SmsOption();
            smsOption.setMessage(message);
            smsOption.setPhoneNumber(users.getPhone());
            smsService.sendSms(smsOption);



            loanRepo.save(loans);
            map.put("status","00");
        }catch (Exception e){
            e.printStackTrace();
            map.put("status","01");
        }

         return map;
    }

    @Override
    public Map<String, Object> repayLoan(HttpServletRequest request) {
        Map<String, Object> map = new HashMap<>();
        String index = request.getParameter("index");
        String action = request.getParameter("action");

        try {
            Loans loans = loanRepo.findById(Long.valueOf(index)).get();
            Users users = userRepo.findByMemberNo(loans.getMemberNo()).get();
            loans.setStatus(Loans.REPAID_LOAN);
            String message = "Dear "+ users.getFullName() +" Your Salary advance has been fully repaid";
            SmsOption smsOption = new SmsOption();
            smsOption.setMessage(message);
            smsOption.setPhoneNumber(users.getPhone());
            smsService.sendSms(smsOption);



            loanRepo.save(loans);
            map.put("status","00");
        }catch (Exception e){
            e.printStackTrace();
            map.put("status","01");
        }

        return map;
    }

    @Override
    public Map<String, Object> denyLoan(HttpServletRequest request) {
        Map<String, Object> map = new HashMap<>();
        String index = request.getParameter("index");

        try {
            Loans loans = loanRepo.findById(Long.valueOf(index)).get();
            Users users = userRepo.findByMemberNo(loans.getMemberNo()).get();
            loans.setStatus(Loans.DECLINED_LOAN);
            String message = "Dear "+ users.getFullName() +"Your Salary advance application has been declined.";
            SmsOption smsOption = new SmsOption();
            smsOption.setMessage(message);
            smsOption.setPhoneNumber(users.getPhone());
            smsService.sendSms(smsOption);

            loanRepo.save(loans);
            map.put("status","00");
        }catch (Exception e){
            e.printStackTrace();
            map.put("status","01");

        }
        return map;
    }

    @Override
    public Map<String, Object> warningLoan(HttpServletRequest request) {
        Map<String,Object> map = new HashMap<>();
        String index = request.getParameter("index");
        String action = request.getParameter("action");

        try {
            Loans loans = loanRepo.findById(Long.valueOf(index)).get();
            Users users =   userRepo.findByMemberNo(loans.getMemberNo()).get();

            String message = "Dear "+ users.getFullName() + "your Salary advance is almost due please repay";
            SmsOption smsOption = new SmsOption();
            smsOption.setMessage(message);
            smsOption.setPhoneNumber(users.getPhone());

            map.put("status","00");

        }catch (Exception e){
            e.printStackTrace();
            map.put("status","01");
        }


        return map;
    }


}
