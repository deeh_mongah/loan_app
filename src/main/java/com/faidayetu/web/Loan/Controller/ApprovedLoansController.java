package com.faidayetu.web.Loan.Controller;

import com.faidayetu.core.export.ExportService;
import com.faidayetu.core.http.HttpFaidaService;
import com.faidayetu.core.template.AjaxUtils;
import com.faidayetu.core.template.View;
import com.faidayetu.core.template.datatables.DataTable;
import com.faidayetu.web.Loan.LoanServiceInterface;
import com.faidayetu.web.reports.controllers.ReportAbstractController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@Controller
public class ApprovedLoansController extends ReportAbstractController {

    @Autowired
    private LoanServiceInterface loanServiceInterface;

    @RequestMapping(value = "/loan-application")
    public ModelAndView index(HttpServletRequest request){
        View view = new View("loan/loan-application");

        if (AjaxUtils.isAjaxRequest(request)) {
            String action = request.getParameter("action");

            if( null != action )
                return handleRequest(request, view);

                //When fetching table data
            else
                return fetchTableInfo(request, view);

            //When fetching table data
        }

        return view.getView();
    }

    public ModelAndView handleRequest(HttpServletRequest request, View view) {
        Map<String, Object> map = new HashMap<>();
        try {
            String action = request.getParameter("action");
            if("approve".equals(action)){
                map =  loanServiceInterface.approveLoan(request);
            }
            else if("deny".equals(action)){
                map = loanServiceInterface.denyLoan(request);
            }
            else if("repaid".equals(action)){
                map = loanServiceInterface.repayLoan(request);
            }else if("warning".equals(action)){
                map = loanServiceInterface.warningLoan(request);
            }

        }catch (Exception e){
            e.printStackTrace();
            map.put("status","01");
            map.put("message","contact system admin");
        }

        return view.sendJSON(map);
    }

    public  ModelAndView fetchTableInfo(HttpServletRequest httpServletRequest, View view){


        dataTable
                .nativeSQL(true)
                .select("DATE_FORMAT(a.application_date; '%Y-%m-%d %T'),a.member_name, a.amount,a.disbursement_account,b.name,a.status,a.id")
                .from("loan_application a ")
                .from("LEFT JOIN corporates b ON b.code = a.company_code")

        ;

        setTransactionStatus(httpServletRequest, "a");

      return   view.sendJSON(dataTable.showTable());
}

}
