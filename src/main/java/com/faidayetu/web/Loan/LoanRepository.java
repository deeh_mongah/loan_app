package com.faidayetu.web.Loan;

import com.faidayetu.web.Loan.Entities.Loans;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface LoanRepository extends CrudRepository<Loans,String> {

    List<Loans> findALLByMemberNo(String memberNo);

    Optional<Loans> findById(Long id);

    List<Loans> findByMemberNo(String memberNo);
}
