package com.faidayetu.web.usermanager.repository;

import com.faidayetu.web.usermanager.entities.GroupPermissions;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Administrator on 2/27/2019.
 */
@Repository
public interface GroupPermissionsRepository extends CrudRepository<GroupPermissions, Long> {




//    @Query( value = "SELECT a.name FROM permissions a LEFT JOIN group_permissions b On b.PERMISSION_ID = a.id WHERE b.USER_GROUP_ID =?1",
//            nativeQuery = true)
//    public List<Object[]> fetchPermissions(long userGroupNo);




    @Query(value = "select * from group_permissions where user_group_id=?1", nativeQuery = true)
    List<GroupPermissions> findByGroupId(long id);
}
