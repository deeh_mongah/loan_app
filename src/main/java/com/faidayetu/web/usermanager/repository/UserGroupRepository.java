package com.faidayetu.web.usermanager.repository;


import com.faidayetu.web.usermanager.entities.UserGroup;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Deeh on 1/30/2019.
 */
@Repository
public interface UserGroupRepository extends CrudRepository<UserGroup, Long> {

    List<UserGroup> findAllByFlag(String flag);

    List<UserGroup> findByName(String name);

//    @Query(value = "from UserGroup where branchGroup.branchNo=?1")
//    List<UserGroup> findByGroupId (Long id);

    List<UserGroup> findByFlagAndBaseType(String flag, String baseType);

    UserGroup findByBaseType(String baseType);

}
