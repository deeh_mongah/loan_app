package com.faidayetu.web.usermanager.repository;


import com.faidayetu.web.usermanager.entities.UserTypes;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserTypesRepository extends CrudRepository<UserTypes, Long> {

    /**
     * Fetch records given  a list of codes
     *
     * @param codes
     * @return  List<UserTypes>
     */

    public List<UserTypes> findByCodeIn(List codes);

    /**
     * Fetch record by code
     *
     * @param code
     * @return Optional<UserTypes>
     */
//    public Optional<UserTypes> findByCode(String code);


    UserTypes findByCode(String code);
}
