package com.faidayetu.web.usermanager.controllers;


import com.faidayetu.core.template.AjaxUtils;
import com.faidayetu.core.template.View;
import com.faidayetu.core.template.datatables.DatatablesInterface;
import com.faidayetu.web.configs.ReasonCodeServiceInterface;
import com.faidayetu.web.usermanager.UserGroupServiceInterface;
import com.faidayetu.web.usermanager.entities.UserTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Deeh on 1/30/2019.
 */
@Controller
public class UserGroupsController {

    @Autowired
    private DatatablesInterface dataTable;
    @Autowired
    private UserGroupServiceInterface entityService;
    @Autowired
    private ReasonCodeServiceInterface reasonCodesService;

    @RequestMapping( value = "/system-groups")
    public ModelAndView index(HttpServletRequest request){
        String userType = (String)request.getSession().getAttribute("_userType");
        View view = new View("user-manager/system-groups");

        if ( AjaxUtils.isAjaxRequest(request) ) {
            String action = request.getParameter("action");

            if( null != action ){
                return handleRequests(request, view);
            }

            //when fetching table data
            else return fetchTableInfo(request, view);
        }

        return view
                .addAttribute("permissions", entityService.fetchPermissions( request) )
                .addAttribute("reasoncodes", reasonCodesService.fetchRecords() )
                .getView();
    }

    /**
     * Handle various client requests
     *
     * @param request Current Request
     * @param view Current view
     * @return ModelAndView
     */
    private ModelAndView handleRequests(HttpServletRequest request, View view){
        Map<String, Object> map = new HashMap<>();
        try
        {
            String action = request.getParameter("action");

            //When creating a record
            if("new".equals( action ) )
                map = this.entityService.saveRecord( request );

            else if("edit".equals( action ))
                map = this.entityService.editRecord( request );

                //When fetching a record
            else if("fetch-record".equals( action ) )
                map = this.entityService.fetchRecord( request );

                //View record changes
            else if("vedit".equals( action ) )
                map = this.entityService.fetchRecordChanges( request );

                //When approving changes
            else if("approve-edit".equals( action ) || "decline-edit".equals( action) )
                map = this.entityService.approveEditChanges( request );

                //View deactivation reasons
            else if("vdeactivation".equals( action ) )
                map = this.entityService.fetchDeactivationInfo( request );

                //When deactivating an object
            else if("deactivate".equals( action ))
                map = this.entityService.deactivateRecord( request );

                //When toggling entity status
            else if("decline-deactivation".equals( action ) || "approve-deactivation".equals( action )
                    || "activate".equals( action ) || "approve-new".equals( action )
                    || "delete".equals( action ) || "decline-new".equals( action ) )
                map = this.entityService.flagRecords( request );
        }
        catch(Exception e){
            e.printStackTrace();
            map.put("status", "01");
            map.put("message", "Internal server error: contact admin");
        }

        return view.sendJSON(map);
    }

    public ModelAndView fetchTableInfo(HttpServletRequest request, View view){
        String status = request.getParameter("fetch-table");
        Long parentNo = (Long)request.getSession().getAttribute("_userParentNo");
        String userType = (String)request.getSession().getAttribute("_userType");

        if (UserTypes.SYSTEM_ADMIN.equals(userType)) {
            dataTable
                    .nativeSQL(true)
                    .select("a.name, a.description,DATE_FORMAT(a.created_on; '%Y-%m-%d %T'), DATE_FORMAT(a.created_on; '%Y-%m-%d %T'), a.id")
                    .from("user_groups a")
                    .where("a.flag = :flag")
                    .setParameter("flag", status);
        }

        return view.sendJSON(dataTable.showTable());
    }

}
