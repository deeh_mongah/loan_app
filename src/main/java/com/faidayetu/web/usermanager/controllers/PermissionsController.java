package com.faidayetu.web.usermanager.controllers;

import com.faidayetu.web.usermanager.PermissionsServiceInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by Administrator on 2/27/2019.
 */

@RestController
@RequestMapping( value = "/permissions")
public class PermissionsController {

    @Autowired
    private PermissionsServiceInterface resourceService;

//    /**
//     * Fetch module permissions
//     *
//     * @param
//     * @param request
//     * @return Object
//     */
//    @PostMapping( value = "/{module}")
//    public ModulePermissionsVm fetchModulePermissions(@PathVariable String module, HttpServletRequest request){
//        return resourceService.fetchModulePermissions( module, request );
//    }

    @GetMapping("/v1/institutions")
    public List<String> chart(HttpServletRequest request) {

        System.err.println("we here, what will you do about it");
        return resourceService.fetchPermissions(request);
    }

}
