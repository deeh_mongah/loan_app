package com.faidayetu.web.usermanager.controllers;

import com.faidayetu.core.template.AjaxUtils;
import com.faidayetu.core.template.View;
import com.faidayetu.core.template.datatables.DatatablesInterface;
import com.faidayetu.web.reports.controllers.ReportAbstractController;
import com.faidayetu.web.usermanager.MobileUserServiceInterface;
import com.faidayetu.web.usermanager.entities.UserTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@Controller
public class MobileUsersController extends ReportAbstractController {

    @Autowired
    private DatatablesInterface dataTable;
    @Autowired private MobileUserServiceInterface entityService;

    @RequestMapping( value = "/mobile-users")
    public ModelAndView index(HttpServletRequest request){
        View view = new View("user-manager/mobile-users");

        if (AjaxUtils.isAjaxRequest(request)) {
            String action = request.getParameter("action");

            if( null != action )
                return handleRequests(request, view);

                //When fetching table data
            else
                return fetchTableInfo(request, view);
        }

        return view.getView();
    }


    /**
     * Handle various client requests
     *
     * @param request Current Request
     * @param view Current view
     * @return ModelAndView
     */
    private ModelAndView handleRequests(HttpServletRequest request, View view){
        Map<String, Object> map = new HashMap<String, Object>();
        try
        {
            String action = request.getParameter("action");
            if("edit".equals( action ))
                map = this.entityService.editRecord( request );

                //When fetching a record
            else if("fetch-record".equals( action ) )
                map = this.entityService.fetchRecord( request );

                //View record changes
            else if("vedit".equals( action ) )
                map = this.entityService.fetchRecordChanges( request );

                //When approving changes
            else if("approve-edit".equals( action ) || "decline-edit".equals( action) )
                map = this.entityService.approveEditChanges( request );

                //When toggling entity status
            else if("activate".equals( action )|| "suspend".equals( action )|| "disable".equals( action )){
                map = this.entityService.flagRecords(request);
            }

        }
        catch(Exception e){
            e.printStackTrace();
            map.put("status", "01");
            map.put("message", "Internal server error: contact admin");
        }

        return view.sendJSON( map );
    }

    /**
     * Fetch table information
     *
     * @param request Current Request
     * @param view Current View
     * @return JsonView - json structure that DataTable can consume
     */
    private ModelAndView fetchTableInfo(HttpServletRequest request, View view){

        dataTable
                .nativeSQL(true)
                .select("a.member_No,a.fullname, a.email, a.phone ,DATE_FORMAT(a.created_on; '%Y-%m-%d %T'),a.status,a.id")
                .from("users a")
                .from("LEFT JOIN user_types b ON b.id = a.user_type_no")
                .where("b.code = :userType ")
                .setParameter("userType", UserTypes.MOBILE_USERS )
        ;
        setTransactionStatus(request, "a");

        return view.sendJSON( dataTable.showTable() );
    }
}