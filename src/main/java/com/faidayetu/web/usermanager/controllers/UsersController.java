package com.faidayetu.web.usermanager.controllers;


import com.faidayetu.core.template.AjaxUtils;
import com.faidayetu.core.template.View;
import com.faidayetu.core.template.datatables.DatatablesInterface;
import com.faidayetu.web.configs.ReasonCodeServiceInterface;
import com.faidayetu.web.usermanager.UserGroupServiceInterface;
import com.faidayetu.web.usermanager.UserServiceInterface;
import com.faidayetu.web.usermanager.UserTypeServiceInterface;
import com.faidayetu.web.usermanager.entities.UserTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@Controller
public class UsersController {

    @Autowired
    private UserServiceInterface entityService;
    @Autowired
    private UserTypeServiceInterface userTypeServiceInterface;
    @Autowired
    private UserGroupServiceInterface userGroupService;
    @Autowired
    private DatatablesInterface dataTable;
    @Autowired
    private ReasonCodeServiceInterface reasonCodeService;
    @Autowired
    private HttpServletRequest servletRequest;

    @Value("${app.endpoint}")
    private String baseURL;

    @RequestMapping(value = "/system-users")
    public ModelAndView index(HttpServletRequest request) {
        View view = new View("user-manager/system-users");

        if (AjaxUtils.isAjaxRequest(request)) {
            String action = request.getParameter("action");

            if (null != action)
                return handleRequests(request, view);

                //When fetching table data
            else
                return fetchTableInfo(request, view);

        }
        return view
                .addAttribute("reasoncodes", reasonCodeService.fetchRecords())
                .addAttribute("userTypes", userTypeServiceInterface.fetchRecords(request))
                .addAttribute("userGroups", userGroupService.fetchAllRecords(request))
                .getView();
    }

    /**
     * Handle various client requests
     *
     * @param request Current Request
     * @param view    Current view
     * @return ModelAndView
     */
    private ModelAndView handleRequests(HttpServletRequest request, View view) {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            String action = request.getParameter("action");

            //When creating a record
            if ("new".equals(action))
                map = this.entityService.saveRecord(request);

            else if ("edit".equals(action))
                map = this.entityService.editRecord(request);

                //When fetching a record
            else if ("fetch-record".equals(action))
                map = this.entityService.fetchRecord(request);

                //View record changes
            else if ("vedit".equals(action))
                map = this.entityService.fetchRecordChanges(request);

                //When approving changes
            else if ("approve-edit".equals(action) || "decline-edit".equals(action))
                map = this.entityService.approveEditChanges(request);

                //View deactivation reasons
            else if ("vdeactivation".equals(action))
                map = this.entityService.fetchDeactivationInfo(request);

                //When deactivating an object
            else if ("deactivate".equals(action))
                map = this.entityService.deactivateRecord(request);

                //When toggling entity status
            else if ("decline-deactivation".equals(action) || "approve-deactivation".equals(action)
                    || "activate".equals(action) || "approve-new".equals(action)
                    || "delete".equals(action) || "decline-new".equals(action)) {

                map = this.entityService.flagRecords(request);

                //When to send an email
                if (!ObjectUtils.isEmpty(map.get("sendMail")) && "approve-new".equals(action)) {
                    String email = String.valueOf(map.get("email"));
                    String token = String.valueOf(map.get("token"));
                    String names = String.valueOf(map.get("names"));
                    String userName = String.valueOf(map.get("userName"));

                  //TODO: send mail
                }
            }

            //When unlocking a user
            else if ("unlock".equals(action))
                map = this.entityService.unlockUser(request);
            else if ("fetch-userGroupNo".equals(action)) {
                return view.sendJSON(userGroupService.fetchRecords(request));
            }
        } catch (Exception e) {
            e.printStackTrace();
            map.put("status", "01");
            map.put("message", "Internal server error: contact admin");
        }

        return view.sendJSON(map);
    }

    /**
     * Fetch table information
     *
     * @param request Current Request
     * @param view    Current View
     * @return JsonView - json structure that DataTable can consume
     */
    private ModelAndView fetchTableInfo(HttpServletRequest request, View view) {
        String state = request.getParameter("fetch-table");
        String userType = (String) servletRequest.getSession().getAttribute("_userType");
        Long parentNo = (Long) servletRequest.getSession().getAttribute("_userParentNo");

        System.err.println(userType);

        System.err.println("parentNo" + parentNo);

        if (userType.equals(UserTypes.SYSTEM_ADMIN)) {
            dataTable
                    .select("a.fullName,a.phone,a.passport, a.email, str(a.createdOn; 'YYYY-MM-DD HH24:MI:SS'), str(a.updatedOn; 'YYYY-MM-DD HH24:MI:SS'), a.id")
                    .from("Users a ")
                    .from("LEFT JOIN a.userTypeLink b")
                    .where("a.flag = :flag")
                    .setParameter("flag", state)
            ;
        }

        // Let's make sure we display system users only
        dataTable
                .where("b.code <> :userType")
                .setParameter("userType", UserTypes.MOBILE_USERS);

        return view.sendJSON(dataTable.showTable());
    }
}
