package com.faidayetu.web.usermanager;



import com.faidayetu.core.template.forms.BaseServiceInterface;
import com.faidayetu.web.usermanager.entities.UserGroup;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by Administrator on 1/30/2019.
 */
public interface UserGroupServiceInterface extends BaseServiceInterface {

    /**
     * Fetch a list of permissions
     *
     * @param request
     * @return List<Object>
     */
    public List<Object> fetchPermissions(HttpServletRequest request);

    /**
     * Fetch  a list of records given their status
     *
     * @return List<UserGroup>
     */
    public List<UserGroup> fetchAllRecords(HttpServletRequest request);

   public List<UserGroup> fetchRecords(HttpServletRequest request);


   public List<UserGroup> fetchRecordsBybranchNo(HttpServletRequest request);
}
