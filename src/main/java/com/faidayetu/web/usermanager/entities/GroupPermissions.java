package com.faidayetu.web.usermanager.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * Created by Ben Muuo on 12/14/2018.
 */
@Entity
@Table(name = "group_permissions")
public class GroupPermissions implements Serializable {

    @Id
    @Column(name = "permission_id")
    private long permissionId;

    @Column(name = "user_group_id")
    private  long userGroupId;

    public long getPermissionId() {
        return permissionId;
    }

    public void setPermissionId(long permissionId) {
        this.permissionId = permissionId;
    }

    public long getUserGroupId() {
        return userGroupId;
    }

    public void setUserGroupId(long userGroupId) {
        this.userGroupId = userGroupId;
    }
}



