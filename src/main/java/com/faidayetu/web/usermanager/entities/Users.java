package com.faidayetu.web.usermanager.entities;

import com.faidayetu.core.template.forms.EditDataWrapper;
import com.faidayetu.core.template.forms.MutableField;
import com.faidayetu.web.configs.entities.ReasonCodes;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author deeh
 */
@Entity
@Table(name = "users")
public class Users implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID", nullable = false)
    private Long id;

    @Size(max = 100)
    @Column(name = "FULLNAME", length = 100)
    private String fullName;

    @Size(max = 250)
    @MutableField( name = "Email")
    @Column(name = "EMAIL", length = 250)
    private String email;

    @Size(max = 100)
    @Column(name = "PASSWORD", length = 100)
    private String password;

    @Size(max = 100)
    @Column(name = "PASSPORT", length = 100)
    private String passport;

    @Size(max = 100)
    @Column(name = "PHONE", length = 100)
    private String phone;

    @Lob
    @Size(max = 2147483647)
    @Column(name = "PHOTO_URL", length = 2147483647)
    private String photoUrl;

    @Size(max = 200)
    @Column(name = "PHOTO_KEY", length = 200)
    private String photoKey;

    @Column(name = "EXPIRY")
    @Temporal(TemporalType.TIMESTAMP)
    private Date expiry;

    @Column(name = "ENABLED")
    private Boolean enabled = false;

    @Column(name = "NONLOCKED")
    private Boolean nonlocked = true ;

    @Column(name = "CREATED_ON")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdOn;

    @Column(name = "UPDATED_ON")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedOn;

    @Column(name = "DELETED_ON")
    @Temporal(TemporalType.TIMESTAMP)
    private Date deletedOn;

    @Column(name = "CREATED_BY")
    private Long createdBy;

    @Column(name = "UPDATED_BY")
    private Long updatedBy;

    @Size(max = 200)
    @Column(name = "EMAIL_TOKEN", length = 200)
    private String emailToken;

    @Size(max = 600)
    @Column(name = "FCM_TOKEN", length = 600)
    private String fcmToken;

    @Size(max = 200)
    @Column(name = "RESET_KEY", length = 200)
    private String resetKey;

    @Column(name = "MOBILE_VERIFIED")
    private boolean mobileVerified = false;

    @Column(name = "EMAIL_VERIFIED")
    private boolean emailVerified = false;

    @Size(max = 20)
    @Column(name = "FLAG", length = 20)
    private String flag;

    @Column(name = "member_No")
    private String memberNo;

    @Column(name = "corporates", length = 200)
    private String corporates;

    @Lob
    @EditDataWrapper
    @Size(max = 2147483647)
    @Column(name = "EDIT_DATA", length = 2147483647)
    private String editData;

    @Size(max = 200)
    @Column(name = "REASON_DESCRIPTION", length = 200)
    private String reasonDescription;

    @Column(name = "LAST_TIME_PASSWORD_UPDATED")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastTimePasswordUpdated;

    @Column(name = "PASSWORD_NEVER_EXPIRES")
    private Boolean passwordNeverExpires;

    @Column(name = "RESET_REQ_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date resetReqDate;

    @Column(name = "firsttime_log_in")
    private Boolean firstTimeLogIn = false;

    @Size(max = 200)
    @Column(name = "ACTIVATION_KEY", length = 200)
    private String activationKey;

    @Size(max = 200)
    @Column(name = "STATUS", length = 200)
    private String mobileUserStatus;

    @Column( name = "REASON_CODE_NO")
    private Long reasonCodeNo;

    @Column(name = "USER_TYPE_NO")
    private Long userTypeNo;

    @Column(name = "company_Code")
    private String companyCode;


    @MutableField( name = "Usergroup", entity = "UserGroup", reference = "name")
    @Column(name = "USER_GROUP_NO")
    private Long userGroupNo;

    @JoinColumn(name = "REASON_CODE_NO", referencedColumnName = "ID", insertable = false, updatable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private ReasonCodes reasonCodeLink;

    @JoinColumn(name = "USER_TYPE_NO", referencedColumnName = "ID", insertable = false, updatable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private UserTypes userTypeLink;

    @JoinColumn(name = "USER_GROUP_NO", referencedColumnName = "ID", nullable = false, insertable = false, updatable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private UserGroup userGroupLink;

    @JoinColumn(name = "CREATED_BY", referencedColumnName = "ID", nullable = false, insertable = false, updatable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Users createdByLink;

    @JoinColumn(name = "UPDATED_BY", referencedColumnName = "ID", nullable = false, insertable = false, updatable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Users updatedByLink;

    public Users() { }


    public String getMobileUserStatus() {
        return mobileUserStatus;
    }

    public void setMobileUserStatus(String mobileUserStatus) {
        this.mobileUserStatus = mobileUserStatus;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public String getCorporates() {
        return corporates;
    }

    public void setCorporates(String corporates) {
        this.corporates = corporates;
    }

    public String getMemberNo() {
        return memberNo;
    }

    public void setMemberNo(String memberNo) {
        this.memberNo = memberNo;
    }

    public String getFullName() {
        return fullName;
    }

    public Users setFullName(String fullName) {
        this.fullName = fullName;
        return this;
    }

    public Boolean getFirstTimeLogIn() {
        return firstTimeLogIn;
    }

    public void setFirstTimeLogIn(Boolean firstTimeLogIn) {
        this.firstTimeLogIn = firstTimeLogIn;
    }

    public String getPassport() {
        return passport;
    }

    public void setPassport(String passport) {
        this.passport = passport;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public Users setEmail(String email) {
        this.email = email;
        return this;
    }


    public String getPassword() {  return password;}
    public Users setPassword(String password) {
        this.password = password;
        return this;
    }

    public Long getUserGroupNo() {
        return userGroupNo;
    }

    public Users setUserGroupNo(Long userGroupNo) {
        this.userGroupNo = userGroupNo;
        return this;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public String getPhotoKey() {   return photoKey; }
    public Users setPhotoKey(String photoKey) {
        this.photoKey = photoKey;
        return this;
    }

    public Date getExpiry() { return expiry; }
    public void setExpiry(Date expiry) {
        this.expiry = expiry;
    }

    public Boolean getEnabled() {  return enabled; }
    public Users setEnabled(Boolean enabled) {
        this.enabled = enabled;
        return this;
    }

    public Boolean getNonlocked() {
        return nonlocked;
    }

    public void setNonlocked(Boolean nonlocked) {
        this.nonlocked = nonlocked;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public Users setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
        return this;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    public Date getDeletedOn() {
        return deletedOn;
    }

    public void setDeletedOn(Date deletedOn) {
        this.deletedOn = deletedOn;
    }

    public Long getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Long createdBy) {
        this.createdBy = createdBy;
    }

    public Long getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Long updatedBy) {
        this.updatedBy = updatedBy;
    }


    public String getEmailToken() {  return emailToken; }
    public Users setEmailToken(String emailToken) {
        this.emailToken = emailToken;
        return this;
    }

    public String getFcmToken() {
        return fcmToken;
    }

    public void setFcmToken(String fcmToken) {
        this.fcmToken = fcmToken;
    }

    public String getResetKey() {
        return resetKey;
    }

    public void setResetKey(String resetKey) {
        this.resetKey = resetKey;
    }

    public boolean isMobileVerified() {
        return mobileVerified;
    }

    public void setMobileVerified(boolean mobileVerified) {
        this.mobileVerified = mobileVerified;
    }

    public boolean isEmailVerified() {    return emailVerified; }
    public Users setEmailVerified(boolean emailVerified) {
        this.emailVerified = emailVerified;
        return this;
    }

    public String getFlag() {
        return flag;
    }

    public Users setFlag(String flag) {
        this.flag = flag;
        return this;
    }

    public String getEditData() {
        return editData;
    }

    public void setEditData(String editData) {
        this.editData = editData;
    }

    public String getReasonDescription() {
        return reasonDescription;
    }

    public void setReasonDescription(String reasonDescription) {
        this.reasonDescription = reasonDescription;
    }

    public Date getLastTimePasswordUpdated() {
        return lastTimePasswordUpdated;
    }

    public void setLastTimePasswordUpdated(Date lastTimePasswordUpdated) {
        this.lastTimePasswordUpdated = lastTimePasswordUpdated;
    }

    public Boolean getPasswordNeverExpires() {
        return passwordNeverExpires;
    }

    public void setPasswordNeverExpires(Boolean passwordNeverExpires) {
        this.passwordNeverExpires = passwordNeverExpires;
    }

    public Date getResetReqDate() {    return resetReqDate;  }
    public Users setResetReqDate(Date resetReqDate) {
        this.resetReqDate = resetReqDate;
        return this;
    }

    public String getActivationKey() {
        return activationKey;
    }

    public Users setActivationKey(String activationKey) {
        this.activationKey = activationKey;
        return this;
    }

    public Long getReasonCodeNo() {
        return reasonCodeNo;
    }

    public void setReasonCodeNo(Long reasonCodeNo) {
        this.reasonCodeNo = reasonCodeNo;
    }

    public Long getUserTypeNo() { return userTypeNo; }
    public Users setUserTypeNo(Long userTypeNo) {
        this.userTypeNo = userTypeNo;
        return this;
    }

    @JsonIgnore
    public UserTypes getUserTypeLink() { return userTypeLink;  }
    @JsonIgnore
    public ReasonCodes getReasonCodeLink() { return reasonCodeLink;  }
    @JsonIgnore
    public Users getCreatedByLink() {  return createdByLink; }
    @JsonIgnore
    public Users getUpdatedByLink() {  return updatedByLink; }

    @JsonIgnore
    public UserGroup getUserGroupLink() { return userGroupLink; }

    public void setUserGroupLink(UserGroup userGroupLink) {
        this.userGroupLink = userGroupLink;
    }

    public Users createdOn(Long userId){
        this.updatedOn = new Date( System.currentTimeMillis() );
        this.createdOn = new Date( System.currentTimeMillis() );
        this.updatedBy = userId;
        this.createdBy = userId;
        return this;
    }

    public Users updatedOn(Long userId){
        this.updatedOn = new Date( System.currentTimeMillis() );
        this.updatedBy = userId;
        return this;
    }

    /**
     * Set the expiry date of the current records credentials
     *
     * @param expiryWindow
     * @return Users
     */
    public Users setExpiryDate(String expiryWindow ){
        // Set the account as expired that one may activate it
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, Integer.parseInt( expiryWindow ) );
        this.setExpiry( calendar.getTime() );
        return this;
    }

    /**
     * Check if the user's credentials have expired
     *
     * @return  boolean
     */
    @JsonIgnore
    public boolean isExpired() {
        // If this is null, do not bother checking
        if ( null == this.expiry ) return true;//Enforce

        // Run the test
        return this.expiry.before(new Date(System.currentTimeMillis()));
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Users)) {
            return false;
        }
        Users other = (Users) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Users[ id=" + id + " ]";
    }
    
}
