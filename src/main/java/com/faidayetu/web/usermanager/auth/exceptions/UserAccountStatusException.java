package com.faidayetu.web.usermanager.auth.exceptions;

import org.springframework.security.core.AuthenticationException;

/**
 * This class will wrap all exceptions relating to user accounts for this application
 * @author Anthony Mwawughanga
 * Date    April 9, 2016
 */
public class UserAccountStatusException extends AuthenticationException {
    
    public UserAccountStatusException(String msg) {
        super(msg);
    }
    
    public UserAccountStatusException(String msg, Throwable t) {
        super(msg, t);
    }
    
}
