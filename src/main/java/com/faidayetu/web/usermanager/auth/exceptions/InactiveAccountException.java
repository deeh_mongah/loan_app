package com.faidayetu.web.usermanager.auth.exceptions;

/**
 * This class will handle all exceptions resulting from inactive accounts
 * @author Anthony Mwawughanga
 */
public class InactiveAccountException extends UserAccountStatusException{
    
    public InactiveAccountException(String msg) {
        super(msg);
    }
    
    public InactiveAccountException(String msg, Throwable t) {
        super(msg, t);
    }
    
}
