package com.faidayetu.web.usermanager.auth.exceptions;

/** 
 * This class will handle all exceptions resulting from user accounts with insufficient details to
 * grant access to the system, e.g. a user who has not been assigned an branch
 * @author Anthony Mwawughanga
 */
public class IncompleteAccountDetailsException extends UserAccountStatusException{
    
    public IncompleteAccountDetailsException(String msg) {
        super(msg);
    }
    
    public IncompleteAccountDetailsException(String msg, Throwable t) {
        super(msg, t);
    }
    
    
}
