package com.faidayetu.web.usermanager.auth.exceptions;

/**
 * This class will handle all exceptions resulting from deactivated accounts
 * @author Anthony Mwawughanga
 * Date    April 9, 2016
 */
public class DeactivatedAccountException extends UserAccountStatusException{
    
    public DeactivatedAccountException(String msg) {
        super(msg);
    }
    
    public DeactivatedAccountException(String msg, Throwable t) {
        super(msg, t);
    }
    
}
