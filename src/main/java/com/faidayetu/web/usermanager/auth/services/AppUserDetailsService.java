package com.faidayetu.web.usermanager.auth.services;

import com.faidayetu.web.usermanager.auth.exceptions.UserAccountStatusException;
import com.faidayetu.web.usermanager.entities.Permissions;
import com.faidayetu.web.usermanager.entities.UserTypes;
import com.faidayetu.web.usermanager.entities.Users;
import com.faidayetu.web.usermanager.repository.UserGroupRepository;
import com.faidayetu.web.usermanager.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * This class creates an instance of a custom spring security UserDetails
 *
 * @author Anthony Mwawughanga Date April 6, 2016
 */
@Service("appUserDetailsService")
@Transactional
public class AppUserDetailsService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserAttemptsService userAttemptsService;
    @Autowired
    private UserGroupRepository userGroupRepository;
    
    /**
     * Custom UserDetails
     *
     * @param email
     * @return
     * @throws UsernameNotFoundException
     * @throws UserAccountStatusException
     */
    @Override
    public UserDetails loadUserByUsername(final String email) throws UsernameNotFoundException, UserAccountStatusException {
        Optional<Users> oUser = userRepository.findByEmail( email );
//        System.err.println(" oUser ->" +oUser.isPresent() );
        // If the user was not found, throw an exception
        if( !oUser.isPresent() ) throw new UsernameNotFoundException("The email '" + email + "' was not found");
        
        return buildUserForAuthentication( oUser.get() );
    }

    private User buildUserForAuthentication(Users user) {
        List<GrantedAuthority> roles = new ArrayList<>();
        /*Populate user permissions*/
        Set<Permissions> permissions = userGroupRepository.findById(user.getUserGroupNo()).get().getPermissions();

        for (Permissions permObject : permissions) {
            roles.add(new SimpleGrantedAuthority(permObject.getName()));
        }

        String userTypeCode = user.getUserTypeLink().getCode();

        //Populate additional permissions for manipulating menu items
        switch (userTypeCode) {
            case UserTypes.SYSTEM_ADMIN:

                roles.add(new SimpleGrantedAuthority("ROLE_SUPERADMIN"));
                break;
        }

        if (roles.isEmpty()) roles = AuthorityUtils.NO_AUTHORITIES;




        /*Check if email has been verified: by default, user account is inactive
         * TODO: According to PCI, set a default password for each account to avoid password check
         */
//        if (!user.isEmailVerified()|| (user.getPassword() == null)) {
//            //            throw new UnconfirmedAccountException("Sorry! Your account is inactive. Go to your email to activate your account");
//            throw new InactiveAccountException("Sorry! Your account is inactive. Go to your email to activate your account");
//        }

        /*Check if user account is locked; if yes, check if elapsed time has been exhausted*/
        boolean isAccountNonLocked = userAttemptsService.isAccountNonLocked( user.getEmail() );

        /*Return Spring Security User object*/
        return new User(user.getEmail(), user.getPassword(), user.getEnabled(), true, true, isAccountNonLocked, roles);
    }

//    private User buildUserForAuthentication(Users user) {
//        List<GrantedAuthority> roles = new ArrayList<>();
//
//        // Check if email has been verified: by default, user account is inactive
//        if (!user.isEmailVerified() || StringUtils.isEmpty( user.getPassword() )) {
//            throw new InactiveAccountException("Sorry! Your account is inactive. Go to your email to activate your account");
//        }
//
//         // Check if user account is locked; if yes, check if elapsed time has been exhausted
//        boolean isAccountNonLocked = userAttemptsService.isAccountNonLocked( user.getEmail() );
//
//        /*Return Spring Security User object*/
////        return new User(
////                user.getEmail(),
////                user.getPassword(),
////                user.getEnabled(),
////                true,
////                !user.isExpired(),
////                isAccountNonLocked,
////                roles
////        );
//         /*Return Spring Security User object*/
//        return new User(user.getEmail(), user.getPassword(), user.getEnabled(), true, true, isAccountNonLocked, roles);
//    }
    
    

}
