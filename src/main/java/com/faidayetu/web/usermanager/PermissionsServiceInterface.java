package com.faidayetu.web.usermanager;

import com.faidayetu.web.usermanager.vm.ModulePermissionsVm;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by Administrator on 2/27/2019.
 */
public interface PermissionsServiceInterface {


    public ModulePermissionsVm fetchModulePermissions(String module, HttpServletRequest request);

      public  List<String> fetchPermissions(HttpServletRequest request);
}
