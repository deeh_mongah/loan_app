package com.faidayetu.web.usermanager.forms;


import com.faidayetu.core.template.forms.Form;
import com.faidayetu.web.usermanager.entities.Users;
import org.springframework.stereotype.Component;

@Component
public class UsersForm extends Form<Users> {

    public UsersForm(){
        setMapping( Users.class );
        setRole("ROLE_USERS", "Users", "super-admin,group-admin");
    }
}
