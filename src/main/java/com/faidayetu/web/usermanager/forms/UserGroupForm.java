package com.faidayetu.web.usermanager.forms;


import com.faidayetu.core.template.forms.Form;
import com.faidayetu.web.usermanager.entities.UserGroup;
import org.springframework.stereotype.Component;

/**
 * Created by Administrator on 1/30/2019.
 */
@Component
public class UserGroupForm extends Form<UserGroup> {

    public UserGroupForm() {
        setMapping(UserGroup.class);
        setRole("ROLE_USER_GROUPS", "User Groups", "system-admin, group-admin");
    }
}
