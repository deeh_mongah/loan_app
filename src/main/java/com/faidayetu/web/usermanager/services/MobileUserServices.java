package com.faidayetu.web.usermanager.services;

import com.faidayetu.core.template.AppConstants;
import com.faidayetu.core.template.forms.AuditData;
import com.faidayetu.web.configs.entities.AuditTrail;
import com.faidayetu.web.configs.repository.AuditTrailRepository;
import com.faidayetu.web.configs.services.AuditService;
import com.faidayetu.web.usermanager.MobileUserServiceInterface;
import com.faidayetu.web.usermanager.entities.Users;
import com.faidayetu.web.usermanager.forms.UsersForm;
import com.faidayetu.web.usermanager.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by User on 2/3/2020.
 */

@Service
@Transactional
public class MobileUserServices implements MobileUserServiceInterface {
    @Autowired
    private UsersForm usersForm;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private AuditService auditLogService;
    @Autowired
    private AuditTrailRepository auditTrailRepository;

    @Override
    public Map<String, Object> saveRecord(HttpServletRequest request) {
        return null;
    }

    /**
     * Edit a record
     *
     * @param request
     * @return Map<String, Object>
     */
    @Override
    public Map<String, Object> editRecord(HttpServletRequest request) {
        Map<String, Object> map = new HashMap<>();
        AuditTrail log = new AuditTrail();
        Long userNo = (Long) request.getSession().getAttribute("_userNo");

        String action = request.getParameter("action");

        boolean isModified = this.usersForm.handleEditRequest(request);
        Users record = this.usersForm.getEntity();

        //If record has changes
        if (isModified) {

            record
                    .setFlag(AppConstants.STATUS_EDITEDRECORD)
                    .updatedOn(userNo);
            //Persist record
            userRepository.save(record);


//            Long index = Long.valueOf(request.getParameter("index"));
            String actionChange = "approve-edit";

            Users recordChanged = this.userRepository.findById(record.getId()).get();
            boolean proceed = usersForm.applyMakerChecker(recordChanged, actionChange);

            map.put("message", usersForm.getResponse());
            if (proceed) {
                record = usersForm.getEntity();
                this.userRepository.save(record);
//                map.put("status", "00");
            }
// else {
////                map.put("status", "01");
//            }



//            //Generate log
            AuditData auditData = usersForm.auditData();
            log
                    .setLogType(AuditTrail.USER_GENERATED)
                    .setActivity(String.format("Edited user - %s : %s", record.getFullName(), auditData.getDescription()))
                    .setNewValues(auditData.getNewValue())
                    .setOldValues(auditData.getOldValue())
                    .setStatus("Success")
                    .setUserNo(userNo);

            map.put("status", "00");
            map.put("message", "Request processed successfully");
        }
        //No changes were made
        else {
            //Generate log
            log
                    .setLogType(AuditTrail.USER_GENERATED)
                    .setActivity(String.format("Attempt to update user - %s : no changes made", record.getFullName()))
                    .setNewValues("N/A")
                    .setOldValues("N/A")
                    .setStatus("Failed")
                    .setUserNo(userNo);

            map.put("status", "01");
            map.put("message", "No changes were made to this record");
        }

        auditTrailRepository.save(log);
        return map;
    }

    /**
     * Approve edit changes
     *
     * @param request
     * @return Map<String, Object>
     */
    @Override
    public Map<String, Object> approveEditChanges(HttpServletRequest request) {
        Map<String, Object> map = new HashMap<>();
        Long userNo = (Long) request.getSession().getAttribute("_userNo");


        Long index = Long.valueOf(request.getParameter("index"));
        String action = request.getParameter("action");

        Users record = this.userRepository.findById(index).get();
        boolean proceed = usersForm.applyMakerChecker(record, action);

        map.put("message", usersForm.getResponse());
        if (proceed) {
            record = usersForm.getEntity();
            this.userRepository.save(record);
            map.put("status", "00");
        } else {
            map.put("status", "01");
        }

        //Insert logs
        AuditTrail log = usersForm.getLog().setUserNo(userNo);
        String activity = String.format("%s Reference: %s", log.getActivity(), record.getFullName());
        log.setActivity(activity);
        auditTrailRepository.save(log);
        return map;
    }

    /**
     * Fetch a record information
     *
     * @param request
     * @return Map<String, Object>
     */
    @Override
    public Map<String, Object> fetchRecord(HttpServletRequest request) {
        String index = request.getParameter("index");
        return this.usersForm.transformEntity(index);
    }

    /**
     * Fetch edit changes
     *
     * @param request
     * @return Map<String, Object>
     */
    @Override
    public Map<String, Object> fetchRecordChanges(HttpServletRequest request) {
        String index = request.getParameter("index");
        return this.usersForm.fetchChanges(index);
    }

    /**
     * Update record status
     *
     * @param request
     * @return Map<String, Object>
     */
    @Override
    public Map<String, Object> flagRecords(HttpServletRequest request) {
        Map<String, Object> map = new HashMap<>();
        StringBuilder activity = new StringBuilder();

        String action = request.getParameter("action");
        Long index = Long.valueOf(request.getParameter("index"));
        Long userNo = (Long) request.getSession().getAttribute("_userNo");
        Users record = this.userRepository.findById(index).get();

        boolean success = usersForm.applyMakerChecker(record, action);
        map.put("message", usersForm.getResponse());

        if (success) {
            record = usersForm.getEntity();
            switch (action) {
                case "activate":

                    record
                            .setFlag( AppConstants.STATUS_ACTIVERECORD)
                            .setMobileUserStatus("Active");
                    break;
                case "suspend":
                    record
                            .setFlag( AppConstants.STATUS_SUSPENDED)
                            .setMobileUserStatus("Suspended");
                    break;
            }
            userRepository.save(record);
            map.put("status", "00");
            map.put("message", "Request Successful");
        } else {
            map.put("status", "01");
            map.put("message", "Request Failed");
        }

        //Insert logs
        AuditTrail log = usersForm.getLog().setUserNo(userNo);
        String logActivity = String.format("%s Reference: %s", log.getActivity(), record.getFullName());
        log.setActivity(logActivity);
        auditTrailRepository.save(log);
        return map;
    }

    @Override
    public Map<String, Object> deactivateRecord(HttpServletRequest request) {
        return null;
    }

    @Override
    public Map<String, Object> fetchDeactivationInfo(HttpServletRequest request) {
        return null;
    }

}
