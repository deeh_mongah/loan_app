package com.faidayetu.web.usermanager.services;

import com.faidayetu.web.configs.entities.MakerChecker;
import com.faidayetu.web.configs.repository.MakerCheckerRepository;
import com.faidayetu.web.usermanager.PermissionsServiceInterface;
import com.faidayetu.web.usermanager.entities.AppRoles;
import com.faidayetu.web.usermanager.entities.GroupPermissions;
import com.faidayetu.web.usermanager.entities.Permissions;
import com.faidayetu.web.usermanager.entities.Users;
import com.faidayetu.web.usermanager.repository.AppRolesRepository;
import com.faidayetu.web.usermanager.repository.GroupPermissionsRepository;
import com.faidayetu.web.usermanager.repository.PermissionsRepository;
import com.faidayetu.web.usermanager.repository.UserRepository;
import com.faidayetu.web.usermanager.vm.ModulePermissionsVm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * Created by Administrator on 2/27/2019.
 */

@Service
@Transactional
public class PermissionsServices implements PermissionsServiceInterface {
    @Autowired
    private MakerCheckerRepository makerCheckerRepository;
    @Autowired
    private AppRolesRepository appRolesRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private GroupPermissionsRepository groupPermissionsRepository;
    @Autowired
    private PermissionsRepository permissionsRepository;


    /**
     * Fetch module permissions
     *
     * @param module
     * @param request
     * @return ModulePermissionsVm
     */
    @Override
    public ModulePermissionsVm fetchModulePermissions(String module, HttpServletRequest request){
        ModulePermissionsVm data = new ModulePermissionsVm();
        //Long userNo = (Long)request.getSession().getAttribute("_userNo");
        Optional<AppRoles> oAppRole = appRolesRepository.findByName( module );

        if( oAppRole.isPresent() ){
            AppRoles appRole = oAppRole.get();
            Set<Permissions> permissionsSet = appRole.getPermissions();
            //List<ModulePermissionsVm.ModulePermission> modulePermissions = new ArrayList<>();

            //If maker-checker is enabled
            Optional<MakerChecker> oMakerChecker = makerCheckerRepository.findByModule( module );
            if( oMakerChecker.isPresent() ){
                MakerChecker makerChecker = oMakerChecker.get();
                data.setMakerChecker( makerChecker.isEnabled() );
            }

            for( Permissions permission : permissionsSet ){

                String action = permission.getName();

                //Check within the session object if user has the current permission( cause we;re in a loop)
                boolean hasRole = request.isUserInRole( permission.getName() );
                ModulePermissionsVm.ModulePermission node = new ModulePermissionsVm.ModulePermission(
                        action, hasRole
                );

                //How does this even work?
                data.getRoles().add( node );

            }

        }
        System.err.println("testModule"+data);

        return data;
    }
    @Override
    public ArrayList<String> fetchPermissions(HttpServletRequest request) {
        Long userNo = (Long)request.getSession().getAttribute("_userNo");


        Optional<Users> oUsers = userRepository.findById(userNo);

        ArrayList<String> permissionList = new ArrayList<>();

        if (oUsers.isPresent()){



            Users users = oUsers.get();
            Long userGroupNo = users.getUserGroupNo();

            List<GroupPermissions> userGroupList = groupPermissionsRepository.findByGroupId(userGroupNo);

            System.err.println("permissions size"+userGroupList.size() );
            for (GroupPermissions permissions : userGroupList) {


                Long permissionId = permissions.getPermissionId();

            Optional<Permissions> permissionsOptional = permissionsRepository.findById(permissionId);

            if (permissionsOptional.isPresent()){

                Permissions permissions1 = permissionsOptional.get();

                String permissionName = permissions1.getName();

                permissionList.add(permissionName);
            }



//

            }

        }

        System.err.println("arraySize"+permissionList.size());
        return permissionList;



    }

}
