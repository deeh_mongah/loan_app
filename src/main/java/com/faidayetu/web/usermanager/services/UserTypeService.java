package com.faidayetu.web.usermanager.services;


import com.faidayetu.web.usermanager.UserTypeServiceInterface;
import com.faidayetu.web.usermanager.entities.UserTypes;
import com.faidayetu.web.usermanager.repository.UserTypesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author      deeh
 * @version     1.0.0
 * @project     Quickgas
 * @since       05/05/2017 10:44
 */
@Service
@Transactional
public class UserTypeService implements UserTypeServiceInterface {

    @Autowired
    private UserTypesRepository userTypesRepository;

    /**
     * Fetch a list of user types
     *
     * @return List<UserType>
     */
    @Override
    public List<UserTypes> fetchRecords(HttpServletRequest request){
        List<UserTypes> data = new ArrayList<>();
        String userType = (String)request.getSession().getAttribute("_userType");
        List<String> codes = new ArrayList<>();

        // When system admin is accessing the system
        if( UserTypes.SYSTEM_ADMIN.equals( userType ) ){
            codes = Arrays.asList(UserTypes.SYSTEM_ADMIN);
        }
        data = userTypesRepository.findByCodeIn( codes );
        return data;
    }
}
