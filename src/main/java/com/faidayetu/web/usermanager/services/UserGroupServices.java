package com.faidayetu.web.usermanager.services;

import com.faidayetu.core.template.AppConstants;
import com.faidayetu.core.template.forms.AuditData;
import com.faidayetu.web.configs.entities.AuditTrail;
import com.faidayetu.web.configs.entities.ReasonCodes;
import com.faidayetu.web.configs.repository.AuditTrailRepository;
import com.faidayetu.web.usermanager.UserGroupServiceInterface;
import com.faidayetu.web.usermanager.entities.*;
import com.faidayetu.web.usermanager.entities.UserGroup;
import com.faidayetu.web.usermanager.forms.UserGroupForm;
import com.faidayetu.web.usermanager.repository.AppRolesRepository;
import com.faidayetu.web.usermanager.repository.PermissionsRepository;
import com.faidayetu.web.usermanager.repository.UserGroupRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

import static com.faidayetu.web.usermanager.entities.UserTypes.SYSTEM_ADMIN;

/**
 * Created by Administrator on 1/30/2019.
 */
@Service
@Transactional
public class UserGroupServices implements UserGroupServiceInterface {

    @Autowired
    private UserGroupRepository entityRepository;
    @Autowired
    private AuditTrailRepository auditTrailRepository;
    @Autowired
    private PermissionsRepository permissionsRepository;
    @Autowired
    private AppRolesRepository appRolesRepository;
    @Autowired
    private UserGroupForm entityForm;


    @Override
    public List<Object> fetchPermissions(HttpServletRequest request) {
        List<Object> permissionRolesList = new ArrayList<>();
        Iterator<AppRoles> rolesSet = appRolesRepository.findAll().iterator();

        while( rolesSet.hasNext() ){
            AppRoles role = rolesSet.next();

            List<Object> permissionsList = new ArrayList<>();
            Set<Permissions> permissionSet = role.getPermissions();

            for( Permissions permission: permissionSet ){
                Map<String, Object> map = new HashMap<>();
                map.put("id", permission.getId() );
                map.put("name", permission.getName() );

                permissionsList.add( map );
            }

            Map<String, Object> map = new HashMap<>();
            map.put("role", role.getName() );
            map.put("function", role.getAppFunction() );
            map.put("permissions", permissionsList);
            permissionRolesList.add( map );
        }

        return permissionRolesList;
    }


    @Override
    public List<UserGroup> fetchAllRecords(HttpServletRequest request) {
        List<UserGroup> groups = new ArrayList<>();
        String userType = (String) request.getSession().getAttribute("_userType");
        Long parentNo = (Long) request.getSession().getAttribute("_userParentNo");

        //When serving super-admin
        if( SYSTEM_ADMIN.equals( userType ) ){
            groups = entityRepository.findAllByFlag(AppConstants.STATUS_ACTIVERECORD);
        }

        return groups;
    }

    @Override
    public List<UserGroup> fetchRecords(HttpServletRequest request) {
        List<UserGroup> data = new ArrayList<>();
        Long parentNo = (Long) request.getSession().getAttribute("_userParentNo");
        String parentType = (String) request.getSession().getAttribute("_userParentType");
        String userType = (String) request.getSession().getAttribute("_userType");

        String SYSTEM_ADMIN = "1";
        String MOBILE_USER = "2";


//        System.err.println("branchNo"+branchNo);
        System.err.println("parenNo"+parentNo);
        System.err.println("parentType"+parentType);

        String flag = AppConstants.STATUS_ACTIVERECORD;

        //When a super-admin is fetching records
        if( SYSTEM_ADMIN.equals( userType ) ) {

            String userTypeNo = request.getParameter("branchNo");


            if (Objects.isNull(userTypeNo)) {

                data = null;

            }else {

                if (userTypeNo.equals(SYSTEM_ADMIN)){
                    data = entityRepository.findByFlagAndBaseType(flag, "system-admin");
                }else if (userTypeNo.equals(MOBILE_USER)){
                    data = entityRepository.findByFlagAndBaseType(flag, "mobile-user");
                }

            }
            //Return results
        }

//        //When an branch users are fetching records
//        else {
//            String action = request.getParameter("action");
//            if( "fetch-userGroupNo".equals( action )) {
//
////                parentNo = Long.valueOf( branchNo );
//                data =  entityRepository.findAllByFlagAndBranchGroup_BranchNo(flag, Long.valueOf(branchNo));
//            }
//        }

        return data;
    }

    @Override
    public List<UserGroup> fetchRecordsBybranchNo(HttpServletRequest request) {
        List<UserGroup> data = new ArrayList<>();
        String userType = (String) request.getSession().getAttribute("_userType");
        String index = request.getParameter("branchNo");

        String flag = AppConstants.STATUS_ACTIVERECORD;
//
//        if( SYSTEM_ADMIN.equals( userType ) ) {
//
//            if (Objects.isNull(index)) {
//
//                data = entityRepository.findByFlagAndBaseType(flag, "systemadmin");
//
//            }else{
//
//                data = entityRepository.findByGroupId(  Long.valueOf( index )  );
//            }
//
//        }else {
//            data = entityRepository.findByGroupId(  Long.valueOf( index )  );
//        }
        return data;
    }

    @Override
    public Map<String, Object> saveRecord(HttpServletRequest request) {
        Map<String, Object> map = new HashMap<>();

        String userType = (String) request.getSession().getAttribute("_userType");
        Long parentNo = (Long) request.getSession().getAttribute("_userParentNo");
        Long userNo = (Long) request.getSession().getAttribute("_userNo");

        String branchNo = request.getParameter("branchNo");

        AuditTrail log = new AuditTrail();
        UserGroup entity = this.entityForm.handleRequests( request );

        List<UserGroup> foundRecords = entityRepository.findByName( entity.getName().trim() );
        boolean isUnique = foundRecords.size() < 1 ;
        if ( !isUnique ) {
            log
                    .setLogType(AuditTrail.USER_GENERATED)
                    .setActivity(String.format("Attempt to create a new User Group failed : %s. Record with similar details exists.", entity.getName()))
                    .setNewValues("N/A")
                    .setOldValues("N/A")
                    .setStatus("Success")
                    .setUserNo(userNo);

            map.put("status", "01");
            map.put("message", "A record with similar details exists");
        }
        else {

            String[] indices = request.getParameterValues("permission");
            Set<Permissions> permissions = new HashSet<>();
            for (String index : indices) {

                Permissions record = permissionsRepository.findById( Long.valueOf( index ) ).get();
                if ( !ObjectUtils.isEmpty( record ) ) permissions.add( record );
            }

            entity
                    .setFlag(AppConstants.STATUS_NEWRECORD)
                    .setPermissions(permissions)
                    .createdOn( userNo );

            /*Save record*/
            entityRepository.save( entity );


            //Populate log
            log
                    .setLogType(AuditTrail.USER_GENERATED)
                    .setActivity("Created a new User Group : " + entity.getName())
                    .setNewValues(entity.getName())
                    .setOldValues("N/A")
                    .setStatus("Success")
                    .setUserNo(userNo);


            map.put("status", "00");
            map.put("message", "Request processed successfully");
        }

        auditTrailRepository.save( log );
        return map;
    }

    @Override
    public Map<String, Object> editRecord(HttpServletRequest request) {
        Map<String, Object> map = new HashMap<>();

        Long userNo = (Long) request.getSession().getAttribute("_userNo");
        AuditTrail log = new AuditTrail().setLogType( AuditTrail.USER_GENERATED ).setUserNo( userNo );

        //Always update permissions
        boolean entityIsModified = this.entityForm.handleEditRequest(request);
        UserGroup record = this.entityForm.getEntity();
        boolean isModified = false;

        String[] indices = request.getParameterValues("permission");

        List<Long> pks = new ArrayList<>();
        for (String index : indices) {
            pks.add(Long.valueOf(index));
        }

        Set<Permissions> newPermissions = permissionsRepository.fetchByIds( pks );
        Set<Permissions> oldPermissions = record.getPermissions();

        //Check if permissions were updated
        String changeActivity = PermSetIsModified( oldPermissions, newPermissions);
        if( !changeActivity.isEmpty() ){
            isModified = true;
            record.setPermissions( newPermissions );
        }

        //Check if the record was updated
        if( entityIsModified  ) {
            isModified = true;
            record.setFlag( AppConstants.STATUS_EDITEDRECORD ).updatedOn( userNo );
        }

        //When there were changes in the record
        if( isModified ){

            if( entityIsModified ){
                map.put("status", "00");
                map.put("message", "Request processed successfully");
            }

            if( !changeActivity.isEmpty() ){
                map.put("status", "00");
                map.put("message", "Permissions updated successfully");
            }

            /*Persist record*/
            entityRepository.save( record );
            AuditData auditData = entityForm.auditData();
            log
                    .setActivity("Edited user group :" + record.getName() +" record - " + auditData.getDescription() + " " + changeActivity )
                    .setNewValues( auditData.getNewValue() )
                    .setOldValues( auditData.getOldValue() )
                    .setStatus("Success")
            ;
        }

        //When there are no changes
        else{
            log
                    .setActivity("Attempt to update user group :" + record.getName() +" record: no changes made" )
                    .setNewValues( "N/A" )
                    .setOldValues("N/A" )
                    .setStatus("Failed");

            map.put("status", "01");
            map.put("message", "No changes were made to this record");
        }

        auditTrailRepository.save(log);
        return map;
    }

    private String PermSetIsModified(Set<Permissions> oldPermissions, Set<Permissions> newPermissions) {
        Set<Permissions> oldSet = new HashSet<>();
        Set<Permissions> newSet = new HashSet<>();

        oldSet.addAll( oldPermissions );
        newSet.addAll( newPermissions );

        String changeActivity = "";
        if( (oldSet.isEmpty() && newSet.isEmpty()) && ( oldSet.containsAll( newSet )) ){
            changeActivity = "";
        }

        //Save a temporary copy of the new set to use it later
        final Set<Permissions> newSetCopy = new HashSet<>( newSet );

        //Retrieve added permissions
        newSet.removeAll( oldSet );
        Set<Permissions> addedSet = newSet;

        //Retrieve 'deleted' permissions
        oldSet.removeAll( newSetCopy );
        Set<Permissions> deletedSet = oldSet;

        //Generate log message for added permissions
        if( !addedSet.isEmpty() ) {
            changeActivity = "Permissions added: ";

            for(Permissions node : addedSet ){
                changeActivity += node.getName() + ", ";
            }
        }

        //Generate log message for 'deleted' permissions
        if( !deletedSet.isEmpty() ){
            if( !changeActivity.isEmpty() ) changeActivity += "; Permissions removed: ";
            else changeActivity = "Permissions removed: ";

            for(Permissions node : deletedSet ){
                changeActivity += node.getName() + ", ";
            }
        }

        //Return consumable log response
        return changeActivity;
    }


    @Override
    public Map<String, Object> approveEditChanges(HttpServletRequest request) {
        Map<String, Object> map = new HashMap<>();
        Long index = Long.valueOf( request.getParameter("index") );
        String action = request.getParameter("action");
        Long userNo = (Long) request.getSession().getAttribute("_userNo");

        UserGroup record = this.entityRepository.findById( index ).get();
        boolean proceed = entityForm.applyMakerChecker(record, action);

        map.put("message", entityForm.getResponse());
        if ( proceed ) {
            record = entityForm.getEntity();
            entityRepository.save( record );
            map.put("status", "00");
        } else {
            map.put("status", "01");
        }

        //Insert logs
        AuditTrail log = entityForm.getLog().setUserNo(userNo);
        String activity = String.format( "%s Reference: %s", log.getActivity(), record.getName());
        log.setActivity( activity );
        auditTrailRepository.save(log);
        return map;
    }

    @Override
    public Map<String, Object> fetchRecord(HttpServletRequest request) {
        String index = request.getParameter("index");
        Map<String, Object> map =  this.entityForm.transformEntity( index );
        List<Long> permissions = new ArrayList<>();
        for( Permissions row : (this.entityForm.getEntity()).getPermissions() ){
            permissions.add(row.getId() );
        }
        map.put("permissions", permissions);
        return map;
    }

    @Override
    public Map<String, Object> fetchRecordChanges(HttpServletRequest request) {
        String index = request.getParameter("index");
        return this.entityForm.fetchChanges( index );
    }

    @Override
    public Map<String, Object> flagRecords(HttpServletRequest request) {
        Map<String, Object> map = new HashMap<>();

        String action = request.getParameter("action");
        Long index = Long.valueOf( request.getParameter("index") );
        Long userNo = (Long) request.getSession().getAttribute("_userNo");
        UserGroup record = this.entityRepository.findById( index ).get();

        boolean success = entityForm.applyMakerChecker(record, action);
        map.put("message", entityForm.getResponse());

        if ( success ) {
            record = entityForm.getEntity();
            entityRepository.save( record );

            map.put("status", "00");
        } else {
            map.put("status", "01");
        }

        //Insert logs
        AuditTrail log = entityForm.getLog().setUserNo( userNo );
        String logActivity = String.format( "%s Reference: %s", log.getActivity(), record.getName() );
        log.setActivity( logActivity );
        auditTrailRepository.save(log);
        return map;
    }

    @Override
    public Map<String, Object> deactivateRecord(HttpServletRequest request) {
        Map<String, Object> map = new HashMap<>();
        Long userNo = (Long) request.getSession().getAttribute("_userNo");
        UserGroup record = this.entityForm.deactivateRequest( request );
        record
                .setFlag( AppConstants.STATUS_DEACTIVATED )
                .updatedOn( userNo );
        entityRepository.save( record );

        AuditTrail log = new AuditTrail()
                .setActivity(String.format("Deactivated a user group successfully. Reference %s", record.getName() ) )
                .setStatus("Success")
                .setOldValues("Active").setNewValues("Deactivated")
                .setUserNo( userNo );
        auditTrailRepository.save( log );

        map.put("status", "00");
        map.put("message", "Request processed successfully");

        return map;
    }

    @Override
    public Map<String, Object> fetchDeactivationInfo(HttpServletRequest request) {
        Map<String, Object> map = new HashMap<>();
        Long index = Long.valueOf( request.getParameter("index") );

        UserGroup record = this.entityRepository.findById( index ).get();
        map.put("index", record.getId() );
        map.put("editor", record.getUpdatedByLink().getFullName());

        if( null != record.getReasonCodeLink() ){
            ReasonCodes reasonCode = record.getReasonCodeLink();
            map.put("reason", reasonCode.getName() );
            map.put("description", record.getReasonDescription() );
        }
        else{
            map.put("reason", "" );
            map.put("description", "");
        }
        map.put("status", "00");
        return map;
    }
}
