package com.faidayetu.web.dashboard.vm;

public class DashboardVm {

    private long systemUsers = 0;
    private long totalGroups = 0;
    private long totalMembers = 0;
    private long totalRevenue = 0;
    private long transactions = 0;


    private long totalAttendance = 0;
    private long totalSaving = 0;
    private long totalPenalty = 0;
    private long totalLoans = 0;


    public long getTotalRevenue() {
        return totalRevenue;
    }

    public DashboardVm setTotalRevenue(long totalRevenue) {
        this.totalRevenue = totalRevenue;
        return this;
    }

    public long getTransactions() {
        return transactions;
    }

    public DashboardVm setTransactions(long transactions) {
        this.transactions = transactions;
        return this;
    }

    public long getTotalAttendance() {
        return totalAttendance;
    }

    public DashboardVm setTotalAttendance(long totalAttendance) {
        this.totalAttendance = totalAttendance;
        return this;
    }

    public long getTotalSaving() {
        return totalSaving;
    }

    public DashboardVm setTotalSaving(long totalSaving) {
        this.totalSaving = totalSaving;
        return this;
    }

    public long getTotalPenalty() {
        return totalPenalty;
    }

    public DashboardVm setTotalPenalty(long totalPenalty) {
        this.totalPenalty = totalPenalty;
        return this;
    }

    public long getTotalLoans() {
        return totalLoans;
    }

    public DashboardVm setTotalLoans(long totalLoans) {
        this.totalLoans = totalLoans;
        return this;
    }

    public long getSystemUsers() {
        return systemUsers;
    }

    public DashboardVm setSystemUsers(long systemUsers) {
        this.systemUsers = systemUsers;
        return this;
    }

    public long getTotalGroups() {
        return totalGroups;
    }

    public DashboardVm setTotalGroups(long totalGroups) {
        this.totalGroups = totalGroups;
        return this;
    }

    public long getTotalMembers() {
        return totalMembers;
    }

    public DashboardVm setTotalMembers(long totalMembers) {
        this.totalMembers = totalMembers;
        return this;
    }
}
