package com.faidayetu.web.dashboard.services;

import com.faidayetu.web.dashboard.DashboardServiceInterface;
import com.faidayetu.web.dashboard.vm.DashboardVm;
import com.faidayetu.web.usermanager.entities.UserTypes;
import org.hibernate.Session;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.math.BigInteger;

@Service
@Transactional
public class DashboardService implements DashboardServiceInterface {

    @PersistenceContext
    private EntityManager entityManager;


    /**
     * Fetch dashboard data
     *
     * @param request
     * @return  DashboardVm
     */
    @Override
    public DashboardVm fetchDashboardData(HttpServletRequest request){
        DashboardVm dashboardVm = new DashboardVm();
        Session session = entityManager.unwrap( Session.class );

        BigInteger systemUsers = (BigInteger)session.createNativeQuery("SELECT COUNT(*) FROM users")
                .uniqueResult();


        BigInteger totalMembers = (BigInteger)session.createNativeQuery("SELECT COUNT(*) FROM users a LEFT JOIN user_types b ON b.id = a.user_type_no WHERE b.code = :code ")
                .setParameter("code", UserTypes.EMPLOYEE)
                .uniqueResult();


        BigDecimal totalRevenue = (BigDecimal) session.createQuery("SELECT COALESCE( SUM(a.amount), 0) FROM Transactions a WHERE a.flag = 'Success'")
                .uniqueResult();

        BigInteger transactions = (BigInteger)session.createNativeQuery("SELECT COUNT(*) FROM transactions").uniqueResult();



        dashboardVm
//
                .setSystemUsers( systemUsers.longValue() )
                .setTotalRevenue( totalRevenue.longValue() )
                .setTotalMembers( totalMembers.longValue() )
                .setTransactions( transactions.longValue())

        ;

        return dashboardVm;
    }

    /**
     * Fetch chart data
     *
     * @return List<WeekSummary>
     */
//    @Override
//    public List<WeekSummary> fetchChartData(){
//        List<WeekSummary> data = new ArrayList<>();
//        List<Object[]> result = routesRepository.fetchCurrentWeekSummary();
//        Iterator<Object[]> it = result.iterator();
//
//        while( it.hasNext() ){
//            Object[] row = it.next();
//            WeekSummary node = new WeekSummary( row );
//            data.add( node );
//        }
//
//        return data;
//    }


}
