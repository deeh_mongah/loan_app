package com.faidayetu.web.dashboard;

import com.faidayetu.web.dashboard.vm.DashboardVm;

import javax.servlet.http.HttpServletRequest;

public interface DashboardServiceInterface {

    /**
     * Fetch dashboard data
     *
     * @param request
     * @return  DashboardVm
     */
    public DashboardVm fetchDashboardData(HttpServletRequest request);

    /**
     * Fetch chart data
     *
     * @return List<WeekSummary>
     */
//    public List<WeekSummary> fetchChartData();

}
