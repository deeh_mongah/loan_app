package com.faidayetu.web.dashboard.controllers;

import com.faidayetu.core.template.View;
import com.faidayetu.web.dashboard.DashboardServiceInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

@Controller
public class DashboardController {

    @Autowired
    private DashboardServiceInterface dashboardService;

    @RequestMapping("/")
    public ModelAndView index(HttpServletRequest request) {
        View view = new View("dashboard/default-view");
//        if ( AjaxUtils.isAjaxRequest( request )) {
////            return view.sendJSON( dashboardService.fetchChartData( ) );
//        }

        return view
                .addAttribute("data", dashboardService.fetchDashboardData( request ) )
                .getView();
    }


}
