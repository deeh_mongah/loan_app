package com.faidayetu.web.corporates.services;

import com.faidayetu.core.template.AppConstants;
import com.faidayetu.core.template.forms.AuditData;
import com.faidayetu.web.corporates.CorporateServiceInterface;
import com.faidayetu.web.corporates.entities.Corporate;
import com.faidayetu.web.corporates.forms.CorporateForm;
import com.faidayetu.web.corporates.repository.CorporateRepository;
import com.faidayetu.web.configs.entities.AuditTrail;
import com.faidayetu.web.configs.entities.ReasonCodes;
import com.faidayetu.web.configs.repository.AuditTrailRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by User on 1/9/2020.
 */
@Service
@Transactional
public class CorporateServices implements CorporateServiceInterface {

    @Autowired private CorporateRepository entityRepository;
    @Autowired private CorporateForm entityForm;
    @Autowired private AuditTrailRepository auditTrailRepository;

    @Override
    public Map<String, Object> saveRecord(HttpServletRequest request) throws FileNotFoundException {
        Map<String, Object> map = new HashMap<>();
        Long userId = (Long)request.getSession().getAttribute("_userNo");
        Long parentNo = (Long) request.getSession().getAttribute("_userParentNo");
        String userType = (String)request.getSession().getAttribute("_userType");
        Corporate entity = entityForm.handleRequests( request );

        if ( entityRepository.findByNameAndCode( entity.getName(), entity.getCode() ).isPresent() ) {
            map.put("status", "01");
            map.put("message",  "Corporate already exists.");
            return map;
        }
        entity
                .setName( entity.getName() )
                .setCode( entity.getCode());

        /*Save record*/
        entityRepository.save( entity );

        AuditTrail log = new AuditTrail();
        log
                .setLogType( AuditTrail.USER_GENERATED )
                .setActivity("Created a new corporate : " + entity.getName() )
                .setNewValues( entity.getName() )
                .setOldValues("N/A")
                .setStatus("Success")
                .setUserNo( userId );

        auditTrailRepository.save( log );

        map.put("status", "00");
        map.put("message", "Request processed successfully");
        return map;
    }

    /**
     * Edit a record
     *
     * @param request
     * @return  Map<String, Object>
     */
    @Override
    public Map<String, Object> editRecord(HttpServletRequest request){
        return null;
    }

    /**
     * Approve edit changes
     *
     * @param request
     * @return  Map<String, Object>
     */
    @Override
    public Map<String, Object> approveEditChanges(HttpServletRequest request){

        return null;
    }

    /**
     * Fetch a record information
     * @param request
     * @return Map<String, Object>
     */
    @Override
    public Map<String, Object> fetchRecord(HttpServletRequest request){
        String index = request.getParameter("index");

        return entityForm.transformEntity(index);
    }

    /**
     * Fetch edit changes
     * @param request
     * @return Map<String, Object>
     */
    @Override
    public Map<String, Object> fetchRecordChanges(HttpServletRequest request){
        String index = request.getParameter("index");
        return entityForm.fetchChanges( index );
    }

    /**
     * Update record status
     *
     * @param request
     * @return Map<String, Object>
     */
    @Override
    public Map<String, Object> flagRecords( HttpServletRequest request ){

        return null;
    }

    /**
     * Deactivate a record
     *
     * @param request
     * @return Map<String, Object>
     */
    @Override
    public Map<String, Object> deactivateRecord(HttpServletRequest request){
        return null;
    }

    /**
     * Fetch deactivation details
     *
     * @param request
     * @return Map<String, Object>
     */
    @Override
    public Map<String, Object> fetchDeactivationInfo(HttpServletRequest request){

        return null;
    }
}
