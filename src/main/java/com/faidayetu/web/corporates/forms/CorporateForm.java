package com.faidayetu.web.corporates.forms;

import com.faidayetu.core.template.forms.Form;
import com.faidayetu.web.corporates.entities.Corporate;
import org.springframework.stereotype.Component;

/**
 * Created by User on 1/9/2020.
 */
@Component
public class CorporateForm extends Form<Corporate> {
    public CorporateForm(){
        setMapping( Corporate.class );
        setRole("ROLE_GROUP", "Groups", "super-admin");
    }
}
