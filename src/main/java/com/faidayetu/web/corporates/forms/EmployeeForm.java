package com.faidayetu.web.corporates.forms;

import com.faidayetu.core.template.forms.Form;
import com.faidayetu.web.corporates.entities.Employee;
import org.springframework.stereotype.Component;

/**
 * Created by User on 1/9/2020.
 */
@Component
public class EmployeeForm extends Form<Employee> {

    public EmployeeForm(){
        setMapping( Employee.class );
        setRole("ROLE_MEMBERS", "Employee", "super-admin");
    }
}
