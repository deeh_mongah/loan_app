package com.faidayetu.web.corporates.entities;

import com.faidayetu.core.template.forms.EditDataWrapper;
import com.faidayetu.core.template.forms.MutableField;
import com.faidayetu.web.configs.entities.ReasonCodes;
import com.faidayetu.web.usermanager.entities.Users;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by User on 1/9/2020.
 */
@Entity
@Table(name = "corporates")
public class Corporate implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID", nullable = false)
    private Long id;

    @Size(max = 100)
    @Column(name = "code", length = 100)
    private String code;

    @Size(max = 100)
    @Column(name = "name", length = 100)
    private String name;

    public Corporate() {
    }

    public Long getId() {
        return id;
    }

    public Corporate setId(Long id) {
        this.id = id;
        return this;
    }

    public String getCode() {
        return code;
    }

    public Corporate setCode(String code) {
        this.code = code;
        return this;
    }

    public String getName() {
        return name;
    }

    public Corporate setName(String name) {
        this.name = name;
        return this;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Corporate)) {
            return false;
        }
        Corporate other = (Corporate) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Corporate[ id=" + id + " ]";
    }
}
