package com.faidayetu.web.corporates.repository;

import com.faidayetu.web.corporates.entities.Corporate;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Created by User on 1/9/2020.
 */
@Repository
public interface CorporateRepository extends CrudRepository<Corporate, Long> {

    public Optional<Corporate> findByNameAndCode(String name, String code);
}
