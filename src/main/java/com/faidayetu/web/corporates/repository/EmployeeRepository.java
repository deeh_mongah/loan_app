package com.faidayetu.web.corporates.repository;

import com.faidayetu.web.corporates.entities.Employee;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Created by User on 1/9/2020.
 */
@Repository
public interface EmployeeRepository extends CrudRepository<Employee, Long> {

    public Optional<Employee> findByNameAndPassport(String name, String passport);
}
