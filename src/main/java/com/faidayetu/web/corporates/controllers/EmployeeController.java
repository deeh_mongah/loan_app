package com.faidayetu.web.corporates.controllers;

import com.faidayetu.core.template.AjaxUtils;
import com.faidayetu.core.template.View;
import com.faidayetu.core.template.datatables.DatatablesInterface;
import com.faidayetu.web.corporates.EmployeeServiceInterface;
import com.faidayetu.web.corporates.repository.CorporateRepository;
import com.faidayetu.web.configs.ReasonCodeServiceInterface;
import com.faidayetu.web.usermanager.entities.UserTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by User on 1/9/2020.
 */
@Controller
public class EmployeeController {

    @Autowired
    private EmployeeServiceInterface entityService;
    @Autowired private CorporateRepository corporateRepository;
    @Autowired
    private DatatablesInterface dataTable;
    @Autowired
    private ReasonCodeServiceInterface reasonCodeService;
    @Autowired
    private HttpServletRequest servletRequest;


    @RequestMapping( value = "/member")
    public ModelAndView index(HttpServletRequest request){
        View view = new View("corporates/member");
        String userType = (String)request.getSession().getAttribute("_userType");

//        else view.addAttribute("institutions", institutionService.fetchInstitutionRecords( request ));

        if (AjaxUtils.isAjaxRequest(request)) {
            String action = request.getParameter("action");

            if( null != action )
                return handleRequests(request, view);

                //When fetching table data
            else
                return fetchTableInfo(request, view);

        }

        return view
                .addAttribute("groupNo", corporateRepository.findAll() )
                .addAttribute("reasoncodes", reasonCodeService.fetchRecords() )
                .getView();
    }

    /**
     * Handle various client requests
     *
     * @param request Current Request
     * @param view Current view
     * @return ModelAndView
     */
    private ModelAndView handleRequests(HttpServletRequest request, View view){
        Map<String, Object> map = new HashMap<String, Object>();
        try
        {
            String action = request.getParameter("action");

            //When creating a record
            if("new".equals( action ) )
                map = this.entityService.saveRecord( request );

            else if("edit".equals( action ))
                map = this.entityService.editRecord( request );

                //When fetching a record
            else if("fetch-record".equals( action ) )
                map = this.entityService.fetchRecord( request );

                //View record changes
            else if("vedit".equals( action ) )
                map = this.entityService.fetchRecordChanges( request );

                //When approving changes
            else if("approve-edit".equals( action ) || "decline-edit".equals( action) )
                map = this.entityService.approveEditChanges( request );

                //View deactivation reasons
            else if("vdeactivation".equals( action ) )
                map = this.entityService.fetchDeactivationInfo( request );

                //When deactivating an object
            else if("deactivate".equals( action ))
                map = this.entityService.deactivateRecord( request );

                //When toggling entity status
            else if("decline-deactivation".equals( action ) || "approve-deactivation".equals( action )
                    || "activate".equals( action ) || "approve-new".equals( action )
                    || "delete".equals( action ) || "decline-new".equals( action ) )
                map = this.entityService.flagRecords( request );

        }
        catch(Exception e){
            e.printStackTrace();
            map.put("status", "01");
            map.put("message", "Internal server error: contact admin");
        }

        return view.sendJSON( map );
    }

    /**
     * Fetch table information
     *
     * @param request Current Request
     * @param view Current View
     * @return JsonView - json structure that DataTable can consume
     */
    private ModelAndView fetchTableInfo(HttpServletRequest request, View view){
        String state = request.getParameter("fetch-table");
        String userType = (String) servletRequest.getSession().getAttribute("_userType");
        Long parenNo = (Long) servletRequest.getSession().getAttribute("_userParentNo");


        if ( UserTypes.SYSTEM_ADMIN.equals( userType )) {
            dataTable
                    .nativeSQL(true)
                    .select("c.name, a.name, a.phone, a.passport, a.gender, a.age, b.group_name, DATE_FORMAT(a.created_on; '%Y-%m-%d %T'), DATE_FORMAT(a.updated_on; '%Y-%m-%d %T'), a.id")
                    .from("members a")
                    .from("LEFT JOIN chama_groups b ON b.id = a.group_no ")
                    .from("LEFT JOIN member_type c ON c.id = a.member_type_no")
                    ;

        }
        else {
            dataTable
                    .nativeSQL(true)
                    .select("c.name, a.name, a.phone, a.passport, a.gender, a.age, b.group_name, DATE_FORMAT(a.created_on; '%Y-%m-%d %T'), DATE_FORMAT(a.updated_on; '%Y-%m-%d %T'), a.id")
                    .from("members a")
                    .from("LEFT JOIN chama_groups b ON b.id = a.group_no ")
                    .from("LEFT JOIN member_type c ON c.id = a.member_type_no")
            ;

        }

        // Apply table filters
        dataTable
                .where("a.flag = :flag ")
                .setParameter("flag", state);

        return view.sendJSON(dataTable.showTable());
    }
}
