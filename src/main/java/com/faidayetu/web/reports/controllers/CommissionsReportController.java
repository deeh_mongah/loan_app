package com.faidayetu.web.reports.controllers;

import com.faidayetu.core.export.ExportService;
import com.faidayetu.core.template.AjaxUtils;
import com.faidayetu.core.template.View;
import com.faidayetu.web.transactions.entities.Transactions;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller
public class CommissionsReportController extends ReportAbstractController{

    @RequestMapping( value = "/commission-report")
    public ModelAndView index(HttpServletRequest request){
        View view = new View("reports/commisiion");

        if (AjaxUtils.isAjaxRequest(request)) {
            String action = request.getParameter("action");

            //When fetching table data
            return fetchTableInfo(request, view);
        }

        return view.getView();
    }

    /**
     * Fetch table information
     *
     * @param request Current Request
     * @param view Current View
     * @return JsonView - json structure that DataTable can consume
     */
    private ModelAndView fetchTableInfo(HttpServletRequest request, View view) {
        String transactionAlias = "";
        String strTransactionTypeNo = request.getParameter("data");

        String[] columns;

        if(StringUtils.isEmpty( strTransactionTypeNo ) ) {

            columns = new String[]{
                    "Transaction Type",
                    "Volume",
                    "Total Commission"
            };
//
//            dataTable
//                    .nativeSQL(true)
//                    .select("a.name,  FORMAT(COALESCE(SUM(b.amount); 0); 2) as 'Volume', FORMAT(COALESCE(SUM(b.sacco_fee); 0); 2) as 'SACCO Commission', FORMAT(COALESCE( SUM(b.binary_fee); 0); 2) as 'Binary Commission', FORMAT( COALESCE( (SUM(b.sacco_fee) + SUM(b.binary_fee)); 0); 2), a.id")
//                    .from("transaction_types a ")
//                    .from("LEFT JOIN transactions b ON b.transaction_type_no = a.id ")
//                    .where( "b.flag = :flag" )
//                    .groupBy("a.id")
//                    .setParameter("flag", Transactions.SUCCESS )
//            ;

            transactionAlias = "b";
        }
        else{
            columns = new String[]{
                    "Date/Time",
                    "Reference No",
                    "Amount",
                    "SACCO Commission",
                    "Binary Commission",
                    "Total Commission"
            };

            Long transactionTypeNo = Long.valueOf( strTransactionTypeNo );
            dataTable
                    .nativeSQL( true )
                    .select("DATE_FORMAT(a.created_on; '%Y-%m-%d %T'), a.reference_no, FORMAT(a.amount; 2), FORMAT(a.sacco_fee; 2), FORMAT(a.binary_fee; 2), FORMAT((a.sacco_fee + a.binary_fee ); 2) ")
                    .from("transactions a ")
                    .from("LEFT JOIN transaction_types b ON b.id = a.transaction_type_no ")
                    .from("LEFT JOIN users c ON c.id = a.user_no ")
                    .where("b.id = :transactionTypeNo ")
                    .setParameter("transactionTypeNo", transactionTypeNo)
                    .where( "a.flag = :flag" )
                    .setParameter("flag", Transactions.SUCCESS )
            ;

            transactionAlias = "a";
        }

        //Set-up filters
        setDateFilters( request, transactionAlias );
        ExportService.init(dataTable, request, columns, true);

        return view.sendJSON( dataTable.showTable() );
    }


    @RequestMapping(value = "/commissions-report/{format}", method = RequestMethod.GET)
    public void export (
            HttpServletRequest request, HttpServletResponse response, @PathVariable("format") String format
    ) throws IOException {
        generateDoc (
                request, response, "commissions-report", format
        );
    }
}
