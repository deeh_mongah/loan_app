package com.faidayetu.web.reports.controllers;


import com.faidayetu.core.export.ExportService;
import com.faidayetu.core.template.AjaxUtils;
import com.faidayetu.core.template.View;
import com.faidayetu.web.usermanager.entities.UserTypes;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller
public class SignUpReportController extends ReportAbstractController{

    @RequestMapping( value = "/signup-report")
    public ModelAndView index(HttpServletRequest request){
        View view = new View("reports/signup-report");

        if (AjaxUtils.isAjaxRequest(request)) {
            String action = request.getParameter("action");

            //When fetching table data
            return fetchTableInfo(request, view);
        }

        return view.getView();
    }

    @RequestMapping(value = "/signup-report/{format}", method = RequestMethod.GET)
    public void export (
            HttpServletRequest request, HttpServletResponse response, @PathVariable("format") String format
    ) throws IOException {
        generateDoc (
                request, response, "signup-report", format
        );
    }

    /**
     * Fetch table information
     *
     * @param request Current Request
     * @param view Current View
     * @return JsonView - json structure that DataTable can consume
     */
    private ModelAndView fetchTableInfo(HttpServletRequest request, View view) {

        String[] columns = new String[]{
                "Names",
                "Member ID",
                "Email",
                "Phone",
                "Branch",
                "Date Joined"
        };

        dataTable
                .nativeSQL(true)
                .select("CONCAT(a.first_name;' ';a.middle_name;' ';a.surname), a.member_id, a.email, a.phone, d.name, DATE_FORMAT(a.created_on; '%Y-%m-%d %T')")
                .from("users a")
                .from("LEFT JOIN user_types b ON b.id= a.user_type_no")
                .from("LEFT JOIN branches_users c ON c.id= a.id ")
                .from("LEFT JOIN branches d ON d.id= c.branch_no ")
                .where("b.code = :userType")
                .setParameter("userType", UserTypes.EMPLOYEE )
        ;
        setDateFilters( request, "a" );
        ExportService.init(dataTable, request, columns, true);

        return view.sendJSON( dataTable.showTable() );
    }
}
