package com.faidayetu.web.reports.controllers;

import com.faidayetu.core.export.ExportService;
import com.faidayetu.core.template.AjaxUtils;
import com.faidayetu.core.template.View;
import com.faidayetu.web.transactions.entities.Transactions;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller
public class FailedTransactionsController extends ReportAbstractController{

    @RequestMapping( value = "/failed-transactions")
    public ModelAndView index(HttpServletRequest request){
        View view = new View("reports/failed-transactions");

        if (AjaxUtils.isAjaxRequest(request)) {
            String action = request.getParameter("action");

            //When fetching table data
            return fetchTableInfo(request, view);
        }

        return view.getView();
    }

    @RequestMapping(value = "/failed-transactions/{format}", method = RequestMethod.GET)
    public void export (
            HttpServletRequest request, HttpServletResponse response, @PathVariable("format") String format
    ) throws IOException {
        generateDoc (
                request, response, "failed-transactions", format
        );
    }

    /**
     * Fetch table information
     *
     * @param request Current Request
     * @param view Current View
     * @return JsonView - json structure that DataTable can consume
     */
    private ModelAndView fetchTableInfo(HttpServletRequest request, View view) {

        String[] columns = new String[]{
                "Date/Time",
                "Reference No",
                "Amount",
                "Transaction Type",
                "Component",
                "Message"
        };
//
        dataTable
                .nativeSQL( true )
                .select("DATE_FORMAT(a.created_on; '%Y-%m-%d %T'), a.reference_no, a.amount, b.name")
                .from("transactions a ")
                .from("LEFT JOIN transaction_types b ON b.id = a.transaction_type_no ")
                .where("a.flag = :flag ")
                .setParameter("flag", Transactions.FAILED )
        ;

        setDateFilters( request, "a" );
        ExportService.init(dataTable, request, columns, true);

        return view.sendJSON( dataTable.showTable() );
    }
}
