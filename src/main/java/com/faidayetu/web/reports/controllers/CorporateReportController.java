package com.faidayetu.web.reports.controllers;


import com.faidayetu.core.export.ExportService;
import com.faidayetu.core.template.AjaxUtils;
import com.faidayetu.core.template.View;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller
public class CorporateReportController extends ReportAbstractController {


    @RequestMapping(value = "/corporates-report")
    public ModelAndView index(HttpServletRequest request) {
        View view = new View("reports/corporates");

        if (AjaxUtils.isAjaxRequest(request)) {
            String action = request.getParameter("action");

            //When fetching table data
            return fetchTableInfo(request, view);
        }

        return view
                .getView();
    }

    /**
     * Fetch table information
     *
     * @param request Current Request
     * @param view    Current View
     * @return JsonView - json structure that DataTable can consume
     */
    private ModelAndView fetchTableInfo(HttpServletRequest request, View view) {
        String transactionAlias = "";
        String strCorporateNo = request.getParameter("data");

        String[] columns;

        if (StringUtils.isEmpty(strCorporateNo)) {

            columns = new String[]{
                    "Corporate",
                    "Code",
                    "Total Amount"
            };
//
            dataTable
                    .nativeSQL(true)
                    .select("a.name, a.code, FORMAT( COALESCE(SUM(b.amount); 0); 2), a.id")
                    .from("corporates a")
                    .from("LEFT JOIN transactions b ON b.corporate_id = a.id")
                    .groupBy("a.id")
            ;

            transactionAlias = "b";
        } else {
            columns = new String[]{
                    "Date/Time",
                    "Reference No",
                    "Amount",
                    "Transaction Type",
                    "Status",
                    "User",
                    "Member No"
            };

            Long corporateNo = Long.valueOf(strCorporateNo);
            dataTable
                    .nativeSQL(true)
                    .select("DATE_FORMAT(a.created_on; '%Y-%m-%d %T'), a.reference_no, a.amount, d.name, a.flag, c.fullname, c.member_No")
                    .from("transactions a")
                    .from("LEFT JOIN corporates b ON b.id = a.corporate_id")
                    .from("LEFT JOIN users c ON c.id = b.id")
                    .from("LEFT JOIN transaction_types d ON d.id=a.transaction_type_no")
                    .where("b.id = :corporateNo")
                    .setParameter("corporateNo", corporateNo)
            ;

            transactionAlias = "a";
        }

        //Set-up filters
        setDateFilters(request, transactionAlias);
        setTransactionStatus(request, transactionAlias);
        ExportService.init(dataTable, request, columns, true);

        return view.sendJSON(dataTable.showTable());
    }


    @RequestMapping(value = "/corporate-report/{format}", method = RequestMethod.GET)
    public void export(
            HttpServletRequest request, HttpServletResponse response, @PathVariable("format") String format
    ) throws IOException {
        generateDoc(
                request, response, "corporate-report", format
        );
    }
}
