package com.faidayetu.web.transactions.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.faidayetu.web.usermanager.entities.Users;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "transactions")
public class Transactions implements Serializable {

    public static final String SUCCESS = "Success";
    public static final String FAILED = "Failed";

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Size(max = 100)
    @Column(name = "reference_no", length = 100)
    private String referenceNo;

    @Column(name = "amount", precision = 18, scale = 2)
    private BigDecimal amount;

    @Column(name = "interest", precision = 18, scale = 2)
    private BigDecimal interest;

    @Column(name = "repayment_period")
    private Long repaymentPeriod;


    @Column( name = "user_no")
    private Long userNo;

    @Column( name = "transaction_type_no")
    private Long transactionTypeNo;

    @Column(name = "flag", length = 100)
    private String flag;


    @Column(name = "created_on")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdOn = new Date( System.currentTimeMillis() );

    @JoinColumn(name = "user_no", referencedColumnName = "id", nullable = false, insertable = false, updatable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Users userLink;

    @JoinColumn(name = "transaction_type_no", referencedColumnName = "id", nullable = false, insertable = false, updatable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private TransactionTypes transactionTypeLink;

    @OneToOne(cascade = CascadeType.ALL, mappedBy = "transactionComponentLink", fetch = FetchType.LAZY)
    private FailedTransactionsLog failedTransactionsLog;



    public Transactions(){}

    public Long getId() { return id; }
    public Transactions setId(Long id) {
        this.id = id;
        return this;
    }

    public String getReferenceNo() { return referenceNo; }
    public Transactions setReferenceNo(String referenceNo) {
        this.referenceNo = referenceNo;
        return this;
    }

    public BigDecimal getAmount() { return amount; }
    public Transactions setAmount(BigDecimal amount) {
        this.amount = amount;
        return this;
    }


    public Long getUserNo() { return userNo; }
    public Transactions setUserNo(Long userNo) {
        this.userNo = userNo;
        return this;
    }

    public Long getTransactionTypeNo() {  return transactionTypeNo; }
    public Transactions setTransactionTypeNo(Long transactionTypeNo) {
        this.transactionTypeNo = transactionTypeNo;
        return this;
    }

    public String getFlag() {  return flag; }
    public Transactions setFlag(String flag) {
        this.flag = flag;
        return this;
    }

    public Date getCreatedOn() {  return createdOn; }
    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public Users getUserLink() {  return userLink; }
    public TransactionTypes getTransactionTypeLink() { return transactionTypeLink;}
    public FailedTransactionsLog getFailedTransactionsLog() { return failedTransactionsLog; }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    public BigDecimal getInterest() {
        return interest;
    }

    public void setInterest(BigDecimal interest) {
        this.interest = interest;
    }

    public Long getRepaymentPeriod() {
        return repaymentPeriod;
    }

    public void setRepaymentPeriod(Long repaymentPeriod) {
        this.repaymentPeriod = repaymentPeriod;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Transactions)) return false;
        Transactions other = (Transactions) object;
        return (this.id != null || other.id == null) && (this.id == null || this.id.equals(other.id));
    }

    @Override
    public String toString() {
        return "Transactions [ id=" + id + " ]";
    }
}

