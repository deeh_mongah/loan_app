package com.faidayetu.web.transactions.entities;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table( name = "external_transactions_log")
public class ExternalTransactionsLog implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "id", nullable = false)
    private Long id;

    @Column( name = "external_payment_guid", length = 150)
    private String externalpaymentGuid;

    @Column( name = "query_count")
    private Long queryCount = 0L;

    @Column( name = "status", length = 100)
    private String status;


    @Size(max = 100)
    @Column(name = "lanster_reference", length = 100)
    private String lansterReference;

    @Size(max = 100)
    @Column(name = "external_reference", length = 100)
    private String externalReference;

    @Column(name = "date_requested")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateRequested = new Date( System.currentTimeMillis() );

    @Column(name = "date_completed")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCompleted = new Date( System.currentTimeMillis() );

    @JoinColumn(name = "id", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private Transactions transactionLink;

    public ExternalTransactionsLog(){}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getExternalpaymentGuid() {
        return externalpaymentGuid;
    }

    public void setExternalpaymentGuid(String externalpaymentGuid) {
        this.externalpaymentGuid = externalpaymentGuid;
    }

    public Long getQueryCount() {
        return queryCount;
    }

    public void setQueryCount(Long queryCount) {
        this.queryCount = queryCount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getLansterReference() {
        return lansterReference;
    }

    public void setLansterReference(String lansterReference) {
        this.lansterReference = lansterReference;
    }

    public String getExternalReference() {
        return externalReference;
    }

    public void setExternalReference(String externalReference) {
        this.externalReference = externalReference;
    }

    public Date getDateRequested() {
        return dateRequested;
    }

    public void setDateRequested(Date dateRequested) {
        this.dateRequested = dateRequested;
    }

    public Date getDateCompleted() {
        return dateCompleted;
    }

    public void setDateCompleted(Date dateCompleted) {
        this.dateCompleted = dateCompleted;
    }

    public Transactions getTransactionLink() {
        return transactionLink;
    }
}
