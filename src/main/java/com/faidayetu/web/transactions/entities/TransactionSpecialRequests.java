package com.faidayetu.web.transactions.entities;


import com.faidayetu.web.usermanager.entities.Users;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by Deeh on 8/28/2019.
 */

@Entity
@Table(name = "special_request")
public class TransactionSpecialRequests implements Serializable {

    private static final String ATM_CARD_APPLICATION = "atm_card_application";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name= "channel_type", length=100)
    private String channelType;

    @Size(max = 100)
    @Column(name = "account_no", length = 100)
    private String accountNo;

    @Size(max = 100)
    @Column(name = "request_type", length = 100)
    private String requestType;

    @Column(name= "retrieval_ref_no", length=100)
    private String retrievalRefNo;

    @Column(name= "system_trace_audit_no", length=100)
    private String systemsTraceAuditNo;

    @Column( name = "branch_no")
    private Long branchNo;

    @Column( name = "customer_id")
    private Long customerId;

    @Column(name = "status", length = 100)
    private String status;

    @Column(name = "created_on")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdOn = new Date( System.currentTimeMillis() );

    @JoinColumn(name = "user_no", referencedColumnName = "id", nullable = false, insertable = false, updatable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Users userLink;


    public TransactionSpecialRequests() {
    }

    public Long getBranchNo() {
        return branchNo;
    }

    public TransactionSpecialRequests setBranchNo(Long branchNo) {
        this.branchNo = branchNo;
        return this;
    }

    public String getRequestType() {
        return requestType;
    }

    public TransactionSpecialRequests setRequestType(String requestType) {
        this.requestType = requestType;
        return this;
    }

    public String getStatus() {
        return status;
    }

    public TransactionSpecialRequests setStatus(String status) {
        this.status = status;
        return this;
    }

    public Long getId() {
        return id;
    }

    public TransactionSpecialRequests setId(Long id) {
        this.id = id;
        return this;
    }

    public String getChannelType() {
        return channelType;
    }

    public TransactionSpecialRequests setChannelType(String channelType) {
        this.channelType = channelType;
        return this;
    }

    public String getAccountNo() {
        return accountNo;
    }

    public TransactionSpecialRequests setAccountNo(String accountNo) {
        this.accountNo = accountNo;
        return this;
    }

    public String getRetrievalRefNo() {
        return retrievalRefNo;
    }

    public TransactionSpecialRequests setRetrievalRefNo(String retrievalRefNo) {
        this.retrievalRefNo = retrievalRefNo;
        return this;
    }

    public String getSystemsTraceAuditNo() {
        return systemsTraceAuditNo;
    }

    public TransactionSpecialRequests setSystemsTraceAuditNo(String systemsTraceAuditNo) {
        this.systemsTraceAuditNo = systemsTraceAuditNo;
        return this;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public TransactionSpecialRequests setCustomerId(Long customerId) {
        this.customerId = customerId;
        return this;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public TransactionSpecialRequests setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
        return this;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TransactionSpecialRequests)) return false;
        TransactionSpecialRequests other = (TransactionSpecialRequests) object;
        return (this.id != null || other.id == null) && (this.id == null || this.id.equals(other.id));
    }

    @Override
    public String toString() {
        return "TransactionSpecialRequests [ id=" + id + " ]";
    }

}
