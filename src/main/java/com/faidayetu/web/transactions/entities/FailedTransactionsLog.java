package com.faidayetu.web.transactions.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Entity
@Table( name = "failed_transactions_log")
public class FailedTransactionsLog implements Serializable {

    @Id
    @Column(name = "id", nullable = false)
    private Long id;

    @Column( name = "transaction_component_no")
    private Long transactionComponentNo;

    @Column( name = "http_code")
    private int httpCode;

    @Column( name = "message")
    private String message;

    @Size(max = 100)
    @Column(name = "reversal_reference_no", length = 100)
    private String reversalReferenceNo;

    @Size(max = 100)
    @Column(name = "reversal_status", length = 100)
    private String reversalStatus;

    @Column( name = "reversal_attempts")
    private Long reversalAttempts = 0L;

    @JoinColumn(name = "id", referencedColumnName = "id", insertable = false, updatable = false)
    @OneToOne(fetch = FetchType.LAZY)
    private Transactions transactionLink;

    @JoinColumn(name = "transaction_component_no", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private TransactionComponents transactionComponentLink;

    public FailedTransactionsLog(){}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getTransactionComponentNo() {
        return transactionComponentNo;
    }

    public void setTransactionComponentNo(Long transactionComponentNo) {
        this.transactionComponentNo = transactionComponentNo;
    }

    public int getHttpCode() {
        return httpCode;
    }

    public void setHttpCode(int httpCode) {
        this.httpCode = httpCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getReversalReferenceNo() {
        return reversalReferenceNo;
    }

    public void setReversalReferenceNo(String reversalReferenceNo) {
        this.reversalReferenceNo = reversalReferenceNo;
    }

    public String getReversalStatus() {
        return reversalStatus;
    }

    public void setReversalStatus(String reversalStatus) {
        this.reversalStatus = reversalStatus;
    }

    public Long getReversalAttempts() {
        return reversalAttempts;
    }

    public void setReversalAttempts(Long reversalAttempts) {
        this.reversalAttempts = reversalAttempts;
    }

    @JsonIgnore public Transactions getTransactionLink() { return transactionLink; }
    @JsonIgnore public TransactionComponents getTransactionComponentLink() {   return transactionComponentLink; }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FailedTransactionsLog)) return false;
        FailedTransactionsLog other = (FailedTransactionsLog) object;
        return (this.id != null || other.id == null) && (this.id == null || this.id.equals(other.id));
    }

    @Override
    public String toString() {
        return "FailedTransactionsLog [ id=" + id + " ]";
    }
}
