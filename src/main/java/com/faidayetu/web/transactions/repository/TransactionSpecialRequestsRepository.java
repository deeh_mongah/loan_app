package com.faidayetu.web.transactions.repository;


import com.faidayetu.web.transactions.entities.TransactionSpecialRequests;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Deeh on 8/28/2019.
 */
@Repository
public interface TransactionSpecialRequestsRepository extends CrudRepository<TransactionSpecialRequests, Long> {
}
