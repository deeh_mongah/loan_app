package com.faidayetu.web.transactions.repository;

import com.faidayetu.web.transactions.entities.ExternalTransactionsLog;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ExternalTnxLogRepository extends CrudRepository<ExternalTransactionsLog, Long> {
}
