package com.faidayetu.web.transactions.repository;

import com.faidayetu.web.transactions.entities.TransactionComponents;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TransactionComponentsRepository extends CrudRepository<TransactionComponents, Long> {
}
