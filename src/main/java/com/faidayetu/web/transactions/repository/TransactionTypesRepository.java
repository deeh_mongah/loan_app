package com.faidayetu.web.transactions.repository;

import com.faidayetu.web.transactions.entities.TransactionTypes;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TransactionTypesRepository extends CrudRepository<TransactionTypes, Long> {

    Optional<TransactionTypes> findByCode(String code);
}
