package com.faidayetu.web.transactions.repository;

import com.faidayetu.web.transactions.entities.Transactions;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TransactionsRepository extends CrudRepository<Transactions, Long> {


    @Query( value = "SELECT b.name,COALESCE( SUM(a.amount), 0) FROM Transactions a " +
            "LEFT JOIN transaction_types b ON b.id = a.transaction_type_no " +
            "WHERE a.flag = 'Success'" +
            " AND YEARWEEK(a.created_on, 1) = YEARWEEK(CURDATE(), 1) GROUP BY 1", nativeQuery = true)
    public List<Object[]> fetchCurrentWeekSummary();
}
