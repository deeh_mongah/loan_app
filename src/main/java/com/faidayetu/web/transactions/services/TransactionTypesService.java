package com.faidayetu.web.transactions.services;

import com.faidayetu.web.transactions.TransactionTypesServiceInterface;
import com.faidayetu.web.transactions.entities.TransactionTypes;
import com.faidayetu.web.transactions.repository.TransactionTypesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Service
@Transactional
public class TransactionTypesService implements TransactionTypesServiceInterface {

    @Autowired
    private TransactionTypesRepository entityRepository;

    /**
     * Fetch all persisted records
     *
     * @return List<TransactionTypes>
     */
    @Override
    public List<TransactionTypes> fetchRecords(HttpServletRequest request){
        return (List)entityRepository.findAll();
    }
}
