package com.faidayetu.web.transactions.vm;

import java.math.BigDecimal;

public class WeekSummary {

    private int day;
    private BigDecimal withdrawals;
    private BigDecimal transfers;
    private BigDecimal bills;

    public WeekSummary(){}

    public WeekSummary(Object[] row){
        this.day = (int)row[0];
        this.withdrawals = (BigDecimal) row[1];
        this.transfers = (BigDecimal) row[2];
        this.bills = (BigDecimal) row[3];
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public BigDecimal getWithdrawals() {
        return withdrawals;
    }

    public void setWithdrawals(BigDecimal withdrawals) {
        this.withdrawals = withdrawals;
    }

    public BigDecimal getTransfers() {
        return transfers;
    }

    public void setTransfers(BigDecimal transfers) {
        this.transfers = transfers;
    }

    public BigDecimal getBills() {
        return bills;
    }

    public void setBills(BigDecimal bills) {
        this.bills = bills;
    }
}
