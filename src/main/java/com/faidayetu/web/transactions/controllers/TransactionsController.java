package com.faidayetu.web.transactions.controllers;

import com.faidayetu.core.export.ExportService;
import com.faidayetu.core.template.AjaxUtils;
import com.faidayetu.core.template.View;
import com.faidayetu.web.reports.controllers.ReportAbstractController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller
public class TransactionsController extends ReportAbstractController {

    @RequestMapping( value = "/transactions")
    public ModelAndView index(HttpServletRequest request){
        View view = new View("transactions/transactions");

        if (AjaxUtils.isAjaxRequest(request)) {
            String action = request.getParameter("action");

            //When fetching table data
            return fetchTableInfo(request, view);
        }

        return view.getView();
    }

    @RequestMapping(value = "/transactions/{format}", method = RequestMethod.GET)
    public void export (
            HttpServletRequest request, HttpServletResponse response, @PathVariable("format") String format
    ) throws IOException {
        generateDoc (
                request, response, "transactions", format
        );
    }

    /**
     * Fetch table information
     *
     * @param request Current Request
     * @param view Current View
     * @return JsonView - json structure that DataTable can consume
     */
    private ModelAndView fetchTableInfo(HttpServletRequest request, View view) {
        String[] columns = new String[]{
                "Date/Time",
                "Member Name",
                "Amount",
                "loan Product",
                "Status",
                "Member No"
        };

        dataTable
                .nativeSQL(true)
                .nativeSQL(true)
                .select("DATE_FORMAT(a.application_date; '%Y-%m-%d %T'),a.member_name, a.amount,a.disbursement_account,b.name,a.status,a.id")
                .from("loan_application a ")
                .from("LEFT JOIN corporates b ON b.code = a.company_code")
        ;

        setDateFilters(request, "a");
        setTransactionStatus(request, "a");
        ExportService.init(dataTable, request, columns, true);

        return view.sendJSON(dataTable.showTable());
    }


}
