package com.faidayetu.web.configs.repository;

import com.faidayetu.web.configs.entities.AppSettings;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AppSettingsRepository extends CrudRepository<AppSettings, Long> {
}
