package com.faidayetu.web.configs.repository;

import com.faidayetu.web.configs.entities.AuditTrail;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AuditTrailRepository extends CrudRepository<AuditTrail, Long> {
}
