package com.faidayetu.web.configs.repository;


import com.faidayetu.web.configs.entities.AppSettings;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface SettingsRepository extends CrudRepository<AppSettings, Long> {

    /**
     *
     * @param code
     * @return Optional<AppSettings>
     */
    public Optional<AppSettings> findByCode(String code);
}
