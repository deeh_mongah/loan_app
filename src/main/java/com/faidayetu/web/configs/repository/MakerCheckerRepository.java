package com.faidayetu.web.configs.repository;

import com.faidayetu.web.configs.entities.MakerChecker;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface MakerCheckerRepository extends CrudRepository<MakerChecker, Long> {

    public Optional<MakerChecker> findByModule(String module);


    public Optional<MakerChecker> findByCode(String module);
}
