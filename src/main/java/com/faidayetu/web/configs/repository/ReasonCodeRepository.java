package com.faidayetu.web.configs.repository;

import com.faidayetu.web.configs.entities.ReasonCodes;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ReasonCodeRepository extends CrudRepository<ReasonCodes, Long> {

    /**
     * Fetch all records by flag
     *
     * @param flag
     * @return List<ReasonCodes>
     */
    List<ReasonCodes> findAllByFlag(String flag);

    /**
     * Fetch all records by name
     *
     * @param name
     * @return List<ReasonCodes>
     */
    List<ReasonCodes> findAllByName(String name);

    /**
     * Fetch records exempting the given ID
     *
     * @param name
     * @param recordId
     * @return List<ReasonCodes>
     */
    List<ReasonCodes> findAllByNameAndIdNot(String name, Long recordId);

}
