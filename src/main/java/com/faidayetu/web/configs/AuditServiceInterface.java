package com.faidayetu.web.configs;


import com.faidayetu.web.configs.entities.AuditTrail;

public interface AuditServiceInterface {

    /**
     *  Save record given the current user's principal name
     *
     * @param trail
     * @param email
     */
    public void saveLog(AuditTrail trail, String email);

}
