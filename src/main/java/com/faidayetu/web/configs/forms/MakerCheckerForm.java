package com.faidayetu.web.configs.forms;

import com.faidayetu.core.template.forms.Form;
import com.faidayetu.web.configs.entities.MakerChecker;
import org.springframework.stereotype.Component;

@Component
public class MakerCheckerForm extends Form<MakerChecker> {

    public MakerCheckerForm() {
        setMapping( MakerChecker.class );
    }
}
