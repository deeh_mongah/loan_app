package com.faidayetu.web.configs.forms;

import com.faidayetu.core.template.forms.Form;
import com.faidayetu.web.configs.entities.AppSettings;
import org.springframework.stereotype.Component;

@Component
public class AppSettingsForm extends Form<AppSettings> {

    //This class constructor initializes the mapping managed by the Form class
    public AppSettingsForm() {
        setMapping( AppSettings.class );
    }
}
