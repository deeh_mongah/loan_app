package com.faidayetu.web.configs.forms;

import com.faidayetu.core.template.forms.Form;
import com.faidayetu.web.configs.entities.ReasonCodes;
import org.springframework.stereotype.Component;

/** 
 * @category
 * @package     okta
 * @since       Oct 23, 2016
 * @author      Anthony
 * @version     
 * 
 */
@Component
public class ReasonCodeForm extends Form<ReasonCodes> {

    //This class constructor initializes the mapping managed by the Form class
    public ReasonCodeForm() {
        setMapping( ReasonCodes.class );
    }
}
