package com.faidayetu.web.configs.controllers;

import com.faidayetu.core.template.AjaxUtils;
import com.faidayetu.core.template.View;
import com.faidayetu.core.template.datatables.DatatablesInterface;
import com.faidayetu.web.usermanager.entities.UserTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

@Controller
public class AuditLogController {

    @Autowired
    private DatatablesInterface datatable;
    @Autowired
    private HttpServletRequest servletRequest;

    @RequestMapping("/audit-logs")
    public ModelAndView index(HttpServletRequest request){
        String userType = (String) servletRequest.getSession().getAttribute("_userType");
        Long parenNo = (Long) servletRequest.getSession().getAttribute("_userParentNo");

        View view = new View("configs/audit-logs");

        System.err.println("===User type"+userType);

        // Fetch the table data
        if ( AjaxUtils.isAjaxRequest( request ) ) {

            //Set-up data
            if (userType.equals(UserTypes.SYSTEM_ADMIN)) {
                datatable
                        .select("str(a.createdOn; 'YYYY-MM-DD HH24:MI'),b.fullName, a.activity, a.status, a.oldValues, a.newValues ")
                        .from("AuditTrail a LEFT JOIN a.userLink b")
                ;
            }
            return view.sendJSON( datatable.showTable() );
        }

        return view.getView();
    }
}
