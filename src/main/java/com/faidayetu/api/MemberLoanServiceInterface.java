package com.faidayetu.api;

import com.faidayetu.api.vm.MemberDetailsRequest;

public interface MemberLoanServiceInterface {

    public Object getMemberLoan(String memberCode);
}
