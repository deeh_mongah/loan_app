package com.faidayetu.api;

import com.faidayetu.api.vm.*;

public interface LoanServiceInterface {

    public Object loanChargeRequest(LoanChargesRequest request);

    public Object loanProductRequest(LoanProducts request);

    public Object loanPerMember(MemberDetailsRequest request);

    public Object loanApplicationRequest(LoanApplicationRequest request);

    public Object getLoanApplicationRequest(GetLoanApplicationRequest request);
}
