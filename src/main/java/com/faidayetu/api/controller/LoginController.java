package com.faidayetu.api.controller;

import com.faidayetu.api.LoginServiceInterface;
import com.faidayetu.api.vm.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RequestMapping("/api/auth")
@RestController
public class LoginController {

    @Autowired
    LoginServiceInterface loginServiceInterface;

  /*  @PostMapping("/activate-user")
    public ResponseEntity<?> activateUser(@RequestBody @Valid ActiveUserRequest request){
        return ResponseEntity.ok().body(loginServiceInterface.activeRequest(request));
    }*/

    @PostMapping("/set-password")
    public ResponseEntity<?> setPassword(@RequestBody @Valid SetMobilePasswordRequest request){
        return ResponseEntity.ok().body(loginServiceInterface.setPassword(request));
    }

    @PostMapping("/otp")
    public ResponseEntity<?> otpValidate(@RequestBody @Valid ValidateOtpRequest request){
        return ResponseEntity.ok().body(loginServiceInterface.validateOtp(request));
    }
    @PostMapping("/reset-otp")
    public ResponseEntity<?> resetOtp(@RequestBody @Valid ResetOtpRequest request){
        return ResponseEntity.ok().body(loginServiceInterface.resetOtp(request));
    }
    @PostMapping("/login")
    public ResponseEntity<?> login(@RequestBody @Valid LoginRequest request){
        return ResponseEntity.ok().body(loginServiceInterface.login(request));
    }
    @PostMapping("/user-registration")
    public ResponseEntity<?> registration(@RequestBody @Valid MemberDetailsRequest request ){
        return ResponseEntity.ok().body(loginServiceInterface.registration(request));
    }
    @PostMapping("/validate-reset-otp")
    public ResponseEntity<?> validateResetOtp(@RequestBody @Valid ValidateResetOtp request){
        return ResponseEntity.ok().body(loginServiceInterface.validateresetotp(request));
    }


}
