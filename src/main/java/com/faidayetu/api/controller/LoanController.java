package com.faidayetu.api.controller;

import com.faidayetu.api.service.LoanService;
import com.faidayetu.api.vm.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.parameters.P;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RequestMapping("/api/Loan")
@RestController
public class LoanController {

    @Autowired
    LoanService loanService;

    @PostMapping("/loancharges")
    public ResponseEntity<?> loanCharges(@RequestBody @Valid LoanChargesRequest request){
        return ResponseEntity.ok().body(loanService.loanChargeRequest(request));
    }

    @PostMapping("/loanProducts")
    public ResponseEntity<?> loanProducts(@RequestBody @Valid LoanProducts request){
        return ResponseEntity.ok().body(loanService.loanProductRequest(request));
    }
    @PostMapping("/loanpermember")
    public ResponseEntity<?> loanPerMember(@RequestBody @Valid MemberDetailsRequest request){
        return ResponseEntity.ok().body(loanService.loanPerMember(request));
    }
    @PostMapping("/loanApplication")
    public ResponseEntity<?> loanApplication(@RequestBody @Valid LoanApplicationRequest request){
        return ResponseEntity.ok().body(loanService.loanApplicationRequest(request));
    }
    @PostMapping("/get-loan-application")
    public ResponseEntity<?> getLoanApplication(@RequestBody @Valid GetLoanApplicationRequest request){
        return ResponseEntity.ok().body(loanService.getLoanApplicationRequest(request));
    }

}
