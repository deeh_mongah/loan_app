package com.faidayetu.api.service;

import com.faidayetu.ResponseModel.ResponseModel;
import com.faidayetu.api.LoanServiceInterface;
import com.faidayetu.api.MemberLoanServiceInterface;
import com.faidayetu.api.vm.*;
import com.faidayetu.core.http.HttpFaidaService;
import com.faidayetu.web.Loan.Entities.Loans;
import com.faidayetu.web.Loan.LoanRepository;
import com.faidayetu.web.usermanager.entities.Users;
import com.faidayetu.web.usermanager.repository.UserRepository;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.*;


@Service
public class LoanService implements LoanServiceInterface , MemberLoanServiceInterface {
    @Autowired
    Environment environment;

    @Autowired
    HttpFaidaService httpFaidaService;

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    LoanRepository loanRepository;
    @Autowired
    private UserRepository userRepository;


    @Value("${faida.loan.loancharges}")
    private String loanChargesUri;


    @Value("${faida.loan.loanpermember}")
    private String memberLoanUri;


    @Override
    public Object loanChargeRequest(LoanChargesRequest request) {
        Map<String, Object> map = new HashMap<>();
        map.put("Code", request.getCode());
        map.put("Description", request.getDescription());

        try {
            String uri = environment.getProperty("faida.loan.loancharges");
            JsonNode node = objectMapper.convertValue(map, JsonNode.class);
            ResponseEntity<String> responseEntity = httpFaidaService.postRequest(uri, node);
            if (responseEntity.getStatusCode().is2xxSuccessful()) {
                return new ResponseModel<>("00", "sucess");
            }


        } catch (Exception e) {
            e.printStackTrace();
        }

        return new ResponseModel<>("00", "sucess");
    }

    @Override
    public Object loanProductRequest(LoanProducts request) {
        Map<String, Object> map = new HashMap<>();
        map.put("code", request.getCode());
        map.put("description", request.getDescription());
        map.put("interestRate", request.getInterestRate());
        map.put("interestCalculationMode", request.getInterestCalculationMode());
        map.put("insurancePercentage", request.getInsurancePercentage());
        map.put("repayPeriod", request.getRepayPeriod());
        map.put("minLoanAmount", request.getMinLoanAmount());
        map.put("maxLoanAmount", request.getMaxLoanAmount());
        map.put("loanAccount", request.getLoanAccount());
        map.put("loanInterestAccount", request.getLoanInterestAccount());
        map.put("repaymentMethod", request.getRepaymentMethod());
        map.put("Active", request.getActive());
        map.put("interestCalculationType", request.getInterestCalculationType());
        map.put("eLoan", request.getActive());
        map.put("loanRepaymentType", request.getLoanRepaymentType());
        map.put("backOfficeFrontOffice", request.getBackOfficeFrontOffice());
        map.put("dibursementAccount", request.getDibursementAccount());

        try {
            String uri = environment.getProperty("faida.loan.loanproduct");
            JsonNode node = objectMapper.convertValue(map, JsonNode.class);
            ResponseEntity<String> responseEntity = httpFaidaService.postRequest(uri, node);
            if (responseEntity.getStatusCode().is2xxSuccessful()) {
                return new ResponseModel<>("00", "sucess");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return new ResponseModel<>("00", "sucess");
    }

    @Override
    public Object getMemberLoan(String memberCode) {
        JsonNode node = objectMapper.createObjectNode();
        ((ObjectNode) node)
                .put("memberCode", memberCode);


        try {
            String uri = environment.getProperty("faida.loan.loanpermember");
            ResponseEntity<String> responseEntity = httpFaidaService.postRequest(uri, node);
            if (responseEntity.getStatusCode().is2xxSuccessful()) {
                return new ResponseModel<>("00", "sucess");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public Object loanPerMember(MemberDetailsRequest request) {
        Map<String, Object> map = new HashMap<>();
        map.put("memberCode", request.getMemberCode());

        try {
            String uri = environment.getProperty("faida.loan.loanpermember");
            JsonNode node = objectMapper.convertValue(map, JsonNode.class);
            ResponseEntity<String> responseEntity = httpFaidaService.postRequest(uri, node);
            if (responseEntity.getStatusCode().is2xxSuccessful()) {
                return new ResponseModel<>("00", "sucess");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return new ResponseModel<>("00", "sucess");
    }

    @Override
    public Object loanApplicationRequest(LoanApplicationRequest request) {
        System.err.println(request.getMemberCode());

        Optional<Users> oUsers = userRepository.findByMemberNo(request.getMemberCode());

        if (!oUsers.isPresent()) {
            return new ResponseModel<>("01", "Member not registered");
        }


//        Optional<Loans> Oloan = loanRepository.findByMemberNo(request.getMemberCode());
//       if(Oloan.isPresent()) {
//           Loans loans1 = Oloan.get();
//           System.err.println("<><><><>status<><><>"+loans1.getStatus());
//
//           if (!loans1.getStatus().equals(Loans.REPAID_LOAN)) {
//               return new ResponseModel<>("01", "sorry you have a pre-existing un-repaid Salary advance");
//           }
//       }


        List<Loans> lstLoans = loanRepository.findByMemberNo(request.getMemberCode());

        for (Loans p : lstLoans) {
            System.err.println("<><><><>status<><><>" + p.getStatus());
            if (p.getStatus().equals(Loans.APPROVED_LOAN) || p.getStatus().equals(Loans.NEW_LOAN) || p.getStatus().equals(Loans.OVERDUE_LOAN) || p.getStatus().equals(Loans.DUE_LOAN)) {
                return new ResponseModel<>("01", "sorry you have a pre-existing Salary advance");
            }
        }


//       }else if(!Oloan.isPresent()) {

        Map<String, Object> req = new HashMap<>();

        req.put("code", "LOAN");


        Users user = oUsers.get();
        try {
            String uri = environment.getProperty("faida.loan.loanproduct");
            JsonNode node = objectMapper.convertValue(req, JsonNode.class);
            ResponseEntity<String> responseEntity = httpFaidaService.postRequest(uri, node);

            if (responseEntity.getStatusCode().is2xxSuccessful()) {
                GetLoanProductResponse getLoanProductResponse = objectMapper.readValue(responseEntity.getBody(), GetLoanProductResponse.class);
                if (request.getLoanProductType().equals("LOAN")) {
                    GetLoanProductResponse.ExportLoanTypesBean.ContentBean Loan = getLoanProductResponse.getExportLoanTypes().getContent().get(0);
                }

                if (request.getLoanProductType().equals("SADANCE")) {
                    GetLoanProductResponse.ExportLoanTypesBean.ContentBean sadvance = getLoanProductResponse.getExportLoanTypes().getContent().get(1);

                    Loans loans = new Loans();

                    loans.setAmount(request.getRequestedAmount());
                    loans.setMemberNo(request.getMemberCode());
                    loans.setDisbursementType(request.getDisbursementType());
                    loans.setDisbursemenrAccount(request.getDisbursementAccount());
                    loans.setApplicationDate(new Date(System.currentTimeMillis()));
                    loans.setRepaymentOption(request.getRepaymentOption());
                    loans.setMemberName(request.getMemberName());
                    loans.setCompanyCode(request.getCompanyCode());
                    loans.setStatus(Loans.NEW_LOAN);
                    loans.createdOn(user.getId());
                    loans.setLoanProduct(request.getLoanProductType());


                    loanRepository.save(loans);

                    return new ResponseModel<>("00", "Salary Advance submitted successfully waiting for approval");
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return new ResponseModel<>("01", "loan application failed");
    }

    @Override
    public Object getLoanApplicationRequest(GetLoanApplicationRequest request) {
        System.err.println(request.getEndDate());
        System.err.println(request.getStartDate());
        System.err.println(request.getMemberNo());

        List<Loans> lstLoans = loanRepository.findALLByMemberNo(request.getMemberNo());

        if (lstLoans.size() == 0) {
            return new ResponseModel<>("01", "You have  no loan");
        }else if(lstLoans.size()>0) {



            Map<String, Object> map = new HashMap<>();
            for (Loans l : lstLoans) {
                Long requestTime = l.getApplicationDate().getTime();
                if (requestTime >= request.getStartDate() && requestTime <= request.getEndDate()) {
                    System.err.println("requestTime "+requestTime);
                    map.put("date", requestTime);
                    map.put("amount", l.getAmount());
                    map.put("status",l.getStatus());
                    map.put("repaymentOption",l.getRepaymentOption());
                    List<Map<String, Object>> loans = new ArrayList<>();
                    loans.add(map);
                    return new ResponseModel<>("00", "success", loans);
                }
            }
        }

        return new ResponseModel<>("01", "No loan application between this dates");

    }

       /* Map<String,Object> map = new HashMap<>();
        for ( Loans l : loans){
            if (l.getApplicationDate().after( request.getStartDate() ) && l.getApplicationDate().before( request.getEndDate() )){

                map.put("date",l.getApplicationDate());
                map.put("amount",l.getAmount());

                return new ResponseModel<>("00","success",map);
            }
        }*/



}