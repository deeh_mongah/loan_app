package com.faidayetu.api.service;

import com.faidayetu.ResponseModel.ResponseModel;
import com.faidayetu.api.LoginServiceInterface;
import com.faidayetu.api.vm.*;
import com.faidayetu.core.http.HttpFaidaService;
import com.faidayetu.core.sms.SmsOptions;
import com.faidayetu.core.sms.SmsService;
import com.faidayetu.core.utils.AppConstants;
import com.faidayetu.core.utils.HelperFunctions;
import com.faidayetu.web.corporates.entities.Corporate;
import com.faidayetu.web.corporates.repository.CorporateRepository;
import com.faidayetu.web.usermanager.entities.Users;
import com.faidayetu.web.usermanager.repository.UserRepository;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
@Transactional
public class LoginService implements LoginServiceInterface {

    @Autowired
    UserRepository userRepo;

    @Autowired
    SmsService smsService;

    @Autowired
    Environment environment;

    @Autowired
    ObjectMapper objectMapper;

   @Autowired
   HttpFaidaService httpFaidaService;

   @Autowired private CorporateRepository corporateRepository;

    @Value("${faida.member.getmember}")
    private String getmemberUrl;

    @Value("${faida.loan.loanproduct}")
    private String loanProductUri;



    @Override
    public Object activeRequest(String phoneNumber) {
        Map<String, Objects> map = new HashMap<>();

        Optional<Users> Ouser = userRepo.findByPhone(phoneNumber);


        if (!Ouser.isPresent()){
            return new ResponseModel<>("01","member not found");
        }

        Users user = Ouser.get();

        if(!user.getFlag().equals("1")){
            return new ResponseModel<>("01","contact corporate admin ");
        }

        if(user.isMobileVerified()){
            return new ResponseModel<>("01","Member is already Activated ");
        }

        return null;


    }

    @Override
    public Object setPassword(SetMobilePasswordRequest request) {
        Optional<Users> Ouser = userRepo.findByMemberNo(request.getMemberCode());

        if(!Ouser.isPresent()){
            return new ResponseModel<>("01","user not found");
        }

        Users users = Ouser.get();

        users.setPassword(HelperFunctions.hashPassword(request.getPassword()));
        users.setFirstTimeLogIn(true);

        userRepo.save(users);

        return new ResponseModel<>("00","password set sucessfully");
    }

    @Override
    public Object validateOtp(ValidateOtpRequest request) {
        Optional<Users> Ouser = userRepo.findByMemberNo(request.getMemberCode());

        if(!Ouser.isPresent()){
            return new ResponseModel<>("01","Users not found");
        }

        Users users = Ouser.get();


        if (! HelperFunctions.checkPasswords(request.getOtp(),users.getPassword())){
                return new ResponseModel<>("01","Incorrect Otp");
        }
        users.setMobileVerified(true);

        userRepo.save(users);


        return new ResponseModel<>("00","sucessfully");
    }

    @Override
    public Object resetOtp(ResetOtpRequest request) {
        Map<String, Objects> map = new HashMap<>();

        Optional<Users> Ouser = userRepo.findByMemberNo(request.getMemberCode());


        if (!Ouser.isPresent()){
            return new ResponseModel<>("01","member not found");
        }

        Users user = Ouser.get();

        if(!user.getFlag().equals("1")){
            return new ResponseModel<>("01","contact corporate admin ");
        }



        String authToken = HelperFunctions.generatePhoneToken(4);
        String message = "Welcome to Faida yetu! Your OTP is: "+ authToken +" This shall be your first time login PIN: ";

        SmsOptions smsOption = new SmsOptions();
        smsOption.setMessage(message);
        smsOption.setMobileNo(user.getPhone());

        boolean send = smsService.sendSMS(smsOption);

        if (send == false){
            return new ResponseModel<>("01","OTP not sent");
        }


        user.setPassword(HelperFunctions.hashPassword( authToken));

        userRepo.save(user);

        return new ResponseModel<>("00","reset OTP sent");


    }

    @Override
    public Object login(LoginRequest request) {

        Optional<Users> Ouser = userRepo.findByMemberNo(request.getMemberCode());

        if (!Ouser.isPresent()){
            return new ResponseModel<>("01","member not found");
        }

        Users users = Ouser.get();

        if (! HelperFunctions.checkPasswords(request.getPassword(),users.getPassword())){
            return new ResponseModel<>("01","Incorrect Password");
        }
        if (!users.getFirstTimeLogIn().equals(true)){
            return new ResponseModel<>("01","First Time login please set password");
        }
        Map<String, Object> map = new HashMap<>();

        Map<String,Object> req = new HashMap<>();
        req.put("code","LOAN");

        try {
            String uri = environment.getProperty("faida.loan.loanproduct");
            JsonNode node = objectMapper.convertValue(req, JsonNode.class);
            ResponseEntity<String> responseEntity = httpFaidaService.postRequest(uri, node);

            if(responseEntity.getStatusCode().is2xxSuccessful()){
                GetLoanProductResponse getLoanProductResponse = objectMapper.readValue(responseEntity.getBody(),GetLoanProductResponse.class);

                GetLoanProductResponse.ExportLoanTypesBean.ContentBean Loan = getLoanProductResponse.getExportLoanTypes().getContent().get(0);
                //map.put("loan",loan);



                GetLoanProductResponse.ExportLoanTypesBean.ContentBean sadvance = getLoanProductResponse.getExportLoanTypes().getContent().get(1);

                String maxS = sadvance.getValue().getMaxLoanAmount();
                Double max = Double.parseDouble(maxS.replaceAll(",",""));

                String minS = sadvance.getValue().getMinLoanAmount();
                Double min = Double.parseDouble(minS.replaceAll(",",""));
                map.put("min",min);
                map.put("max",max);

            }

        }catch (Exception e){
            e.printStackTrace();
        }




        map.put("memberCode",users.getMemberNo());
        map.put("memberName",users.getFullName());
        map.put("phoneNumber",users.getPhone());
        map.put("email",users.getEmail());
        map.put("companyCode",users.getCompanyCode());

        return new ResponseModel<>("00","Login successful",map);
    }

    @Override
    public Object registration(MemberDetailsRequest request) {
      Optional<Users> Ouser =userRepo.findByMemberNo(request.getMemberCode());

      if (Ouser.isPresent()){
          Users users = Ouser.get();
          if(users.isMobileVerified()){
              return new ResponseModel<>("01","member already exist");
          }else if(!users.isMobileVerified()){
              String authToken = HelperFunctions.generatePhoneToken(4);
              String message = "Welcome to Faida yetu! Your OTP is: "+ authToken +" This shall be your first time login PIN: ";

              SmsOptions smsOption = new SmsOptions();
              smsOption.setMessage(message);
              smsOption.setMobileNo(users.getPhone());

              smsService.sendSMS(smsOption);

              users.setPassword(HelperFunctions.hashPassword( authToken));
              userRepo.save(users);
              return new ResponseModel<>("00","OTP Sent");
          }
          return new ResponseModel<>("01","member already exist");
      }



       Map<String,Object> map = new HashMap<>();
       Map<String,Object> resp = new HashMap<>();
       map.put("memberCode",request.getMemberCode());
        Users users = new Users();


        try {
            String uri = environment.getProperty("faida.member.getmember");
            JsonNode node = objectMapper.convertValue(map,JsonNode.class);
            ResponseEntity<String> responseEntity = httpFaidaService.postRequest(uri,node);


            if (responseEntity.getStatusCode().is2xxSuccessful()){
                MemberDetailsResponse memberDetailsResponse = objectMapper.readValue(responseEntity.getBody(), MemberDetailsResponse.class);

                MemberDetailsResponse.ExportMemberDetailsBean.ContentBean value = memberDetailsResponse.getExportMemberDetails().getContent().get(0);
                users.setFullName(value.getValue().getMemberName());
                users.setPhone(value.getValue().getMobilePhoneNo());
                users.setEmail(value.getValue().getEmail());
                users.setMobileVerified(false);
                users.setFirstTimeLogIn(false);
                users.setFlag(AppConstants.STATUS_ACTIVERECORD);
                users.setUserTypeNo(4L);
                users.setMemberNo(value.getValue().getMemberNo());
                users.setCorporates(value.getValue().getCompanyName());
                users.setCompanyCode(value.getValue().getCompanyCode());
                users.createdOn( users.getId() );

                Optional<Corporate> oCorporate = corporateRepository.findByNameAndCode(value.getValue().getCompanyName(),value.getValue().getCompanyCode());
                if(!oCorporate.isPresent()){

                    Corporate corporate= new Corporate();
                    corporate.setCode(value.getValue().getCompanyCode()).setName(value.getValue().getCompanyName());
                    corporateRepository.save(corporate );
                }

                String authToken = HelperFunctions.generatePhoneToken(4);
                String message = "Welcome to Faida yetu! Your OTP is: "+ authToken +" This shall be your first time login PIN: ";

                SmsOptions smsOption = new SmsOptions();
                smsOption.setMessage(message);
//                smsOption.setMobileNo(value.getValue().getMobilePhoneNo());
               //smsOption.setPhoneNumber("0723996654");
               // smsOption.setPhoneNumber("0702931540");
                smsOption.setMobileNo("0799182092");

                smsService.sendSMS(smsOption);

                  users.setPassword(HelperFunctions.hashPassword( authToken));
                  userRepo.save(users);
                  return new ResponseModel<>("00","OTP Sent");


            }

        }catch (Exception e){
            e.printStackTrace();
        }


       return new ResponseModel<>("00","member not found");


    }
}
