package com.faidayetu.api.ApiAuthentication;

import com.faidayetu.core.http.HttpFaidaService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

@Service
@Transactional
public class AuthentationService {

    @Value("${faida.user}")
    private String user;

    @Value("${faida.password}")
    private String password;

    @Value("${faida.Domain}")
    private String domain;

    private HttpFaidaService httpFaidaService;


    public Map<String, Object> findUserAuthentication(HttpFaidaService httpFaidaService) throws NoSuchAlgorithmException{
        Map<String , Object> map = new HashMap<>();
        map.put("username",user);
        map.put("password",password);
        map.put("Domain",domain);

        return map;

    }


}
