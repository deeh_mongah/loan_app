package com.faidayetu.api;

import com.faidayetu.api.vm.*;

public interface LoginServiceInterface {

    public Object activeRequest(String phoneNumber);

    public Object setPassword(SetMobilePasswordRequest request);

    public Object validateOtp(ValidateOtpRequest request);

    public Object resetOtp(ResetOtpRequest request);

    public Object login(LoginRequest request);

    public Object registration(MemberDetailsRequest request);

    public Object validateresetotp(ValidateResetOtp request);
}
