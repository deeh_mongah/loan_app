package com.faidayetu.api.vm;


import java.util.List;

public class MemberDetailsResponse {


    private ExportMemberDetailsBean exportMemberDetails;

    public ExportMemberDetailsBean getExportMemberDetails() {
        return exportMemberDetails;
    }

    public void setExportMemberDetails(ExportMemberDetailsBean exportMemberDetails) {
        this.exportMemberDetails = exportMemberDetails;
    }

    public static class ExportMemberDetailsBean {
        private List<ContentBean> content;

        public List<ContentBean> getContent() {
            return content;
        }

        public void setContent(List<ContentBean> content) {
            this.content = content;
        }

        public static class ContentBean {


            private ValueBean value;

            public ValueBean getValue() {
                return value;
            }

            public void setValue(ValueBean value) {
                this.value = value;
            }

            public static class ValueBean {


                private String memberNo;
                private String memberName;
                private String mobilePhoneNo;
                private String email;
                private String dateofBirth;
                private String idno;
                private String companyCode;
                private String companyName;


                public String getCompanyCode() {
                    return companyCode;
                }

                public void setCompanyCode(String companyCode) {
                    this.companyCode = companyCode;
                }

                public String getCompanyName() {
                    return companyName;
                }

                public void setCompanyName(String companyName) {
                    this.companyName = companyName;
                }

                public String getMemberNo() {
                    return memberNo;
                }

                public void setMemberNo(String memberNo) {
                    this.memberNo = memberNo;
                }

                public String getMemberName() {
                    return memberName;
                }

                public void setMemberName(String memberName) {
                    this.memberName = memberName;
                }

                public String getMobilePhoneNo() {
                    return mobilePhoneNo;
                }

                public void setMobilePhoneNo(String mobilePhoneNo) {
                    this.mobilePhoneNo = mobilePhoneNo;
                }

                public String getEmail() {
                    return email;
                }

                public void setEmail(String email) {
                    this.email = email;
                }

                public String getDateofBirth() {
                    return dateofBirth;
                }

                public void setDateofBirth(String dateofBirth) {
                    this.dateofBirth = dateofBirth;
                }

                public String getIdno() {
                    return idno;
                }

                public void setIdno(String idno) {
                    this.idno = idno;
                }
            }
        }
    }
}






