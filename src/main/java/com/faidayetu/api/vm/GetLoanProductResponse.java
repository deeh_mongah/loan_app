package com.faidayetu.api.vm;

import java.util.List;

public class GetLoanProductResponse {


    private ExportLoanTypesBean exportLoanTypes;

    public ExportLoanTypesBean getExportLoanTypes() {
        return exportLoanTypes;
    }

    public void setExportLoanTypes(ExportLoanTypesBean exportLoanTypes) {
        this.exportLoanTypes = exportLoanTypes;
    }

    public static class ExportLoanTypesBean {
        private List<ContentBean> content;

        public List<ContentBean> getContent() {
            return content;
        }

        public void setContent(List<ContentBean> content) {
            this.content = content;
        }

        public static class ContentBean {


            private ValueBean value;

            public ValueBean getValue() {
                return value;
            }

            public void setValue(ValueBean value) {
                this.value = value;
            }

            public static class ValueBean {

                private String code;
                private String description;
                private String interestRate;
                private String interestCalculationMethod;
                private String insurancePercentage;
                private String repayPeriods;
                private String minLoanAmount;
                private String maxLoanAmount;
                private String loanAccount;
                private String loanInterestAccount;
                private String repaymentMethod;
                private String active;
                private String interestCalculationType;
                private String eloan;
                private String loanRepaymentType;
                private String backofficeFrontoffice;
                private String disbursementAccount;

                public String getCode() {
                    return code;
                }

                public void setCode(String code) {
                    this.code = code;
                }

                public String getDescription() {
                    return description;
                }

                public void setDescription(String description) {
                    this.description = description;
                }

                public String getInterestRate() {
                    return interestRate;
                }

                public void setInterestRate(String interestRate) {
                    this.interestRate = interestRate;
                }

                public String getInterestCalculationMethod() {
                    return interestCalculationMethod;
                }

                public void setInterestCalculationMethod(String interestCalculationMethod) {
                    this.interestCalculationMethod = interestCalculationMethod;
                }

                public String getInsurancePercentage() {
                    return insurancePercentage;
                }

                public void setInsurancePercentage(String insurancePercentage) {
                    this.insurancePercentage = insurancePercentage;
                }

                public String getRepayPeriods() {
                    return repayPeriods;
                }

                public void setRepayPeriods(String repayPeriods) {
                    this.repayPeriods = repayPeriods;
                }

                public String getMinLoanAmount() {
                    return minLoanAmount;
                }

                public void setMinLoanAmount(String minLoanAmount) {
                    this.minLoanAmount = minLoanAmount;
                }

                public String getMaxLoanAmount() {
                    return maxLoanAmount;
                }

                public void setMaxLoanAmount(String maxLoanAmount) {
                    this.maxLoanAmount = maxLoanAmount;
                }

                public String getLoanAccount() {
                    return loanAccount;
                }

                public void setLoanAccount(String loanAccount) {
                    this.loanAccount = loanAccount;
                }

                public String getLoanInterestAccount() {
                    return loanInterestAccount;
                }

                public void setLoanInterestAccount(String loanInterestAccount) {
                    this.loanInterestAccount = loanInterestAccount;
                }

                public String getRepaymentMethod() {
                    return repaymentMethod;
                }

                public void setRepaymentMethod(String repaymentMethod) {
                    this.repaymentMethod = repaymentMethod;
                }

                public String getActive() {
                    return active;
                }

                public void setActive(String active) {
                    this.active = active;
                }

                public String getInterestCalculationType() {
                    return interestCalculationType;
                }

                public void setInterestCalculationType(String interestCalculationType) {
                    this.interestCalculationType = interestCalculationType;
                }

                public String getEloan() {
                    return eloan;
                }

                public void setEloan(String eloan) {
                    this.eloan = eloan;
                }

                public String getLoanRepaymentType() {
                    return loanRepaymentType;
                }

                public void setLoanRepaymentType(String loanRepaymentType) {
                    this.loanRepaymentType = loanRepaymentType;
                }

                public String getBackofficeFrontoffice() {
                    return backofficeFrontoffice;
                }

                public void setBackofficeFrontoffice(String backofficeFrontoffice) {
                    this.backofficeFrontoffice = backofficeFrontoffice;
                }

                public String getDisbursementAccount() {
                    return disbursementAccount;
                }

                public void setDisbursementAccount(String disbursementAccount) {
                    this.disbursementAccount = disbursementAccount;
                }
            }
        }
    }
}
