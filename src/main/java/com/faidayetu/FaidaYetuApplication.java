package com.faidayetu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.ApplicationPidFileWriter;

@SpringBootApplication
public class FaidaYetuApplication {

	public static void main(String[] args) {
		SpringApplication app = new SpringApplication( FaidaYetuApplication.class );
		app.addListeners(new ApplicationPidFileWriter() );
		app.run( args );
	}

}
