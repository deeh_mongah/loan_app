ALTER TABLE `loan_application`
ADD COLUMN `STATUS` INT(11) NULL AFTER `LOAN_PURPOSE`,
ADD INDEX `fk_loan_application_1_idx` (`STATUS` ASC);
ALTER TABLE `loan_application`
ADD CONSTRAINT `fk_loan_application_1`
  FOREIGN KEY (`STATUS`)
  REFERENCES `status` (`ID`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;