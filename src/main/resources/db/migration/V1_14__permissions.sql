set foreign_key_checks = 0;
INSERT INTO permissions (id, name, app_code, created_on, updated_on, role_no) VALUES (52, 'Transactions', 'transactions', now(), now(), 6);
INSERT INTO permissions (id, name, app_code, created_on, updated_on, role_no) VALUES (53, 'Corporate Reports', 'corporate-reports', now(), now(), 7);
INSERT INTO permissions (id, name, app_code, created_on, updated_on, role_no) VALUES (54, 'Failed Reports', 'failed-reports', now(), now(), 7);

# app roles
INSERT INTO app_roles (`id`, `name`, `app_code`, `created_at`, `updated_at`, `app_function`) VALUES (6, 'Transactions', 'ROLE_TRANSACTIONS', now(), now(), 'system-admin,corporate-admin');
INSERT INTO app_roles (`id`, `name`, `app_code`, `created_at`, `updated_at`, `app_function`) VALUES (7, 'Reports', 'ROLE_REPORTS', now(), now(), 'system-admin,corporate-admin');

INSERT INTO group_permissions (permission_id, user_group_id) VALUES (52, 1);
INSERT INTO group_permissions (permission_id, user_group_id) VALUES (53, 1);
INSERT INTO group_permissions (permission_id, user_group_id) VALUES (54, 1);

set foreign_key_checks = 1;