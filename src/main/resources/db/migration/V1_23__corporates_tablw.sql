drop table if exists corporates;
create table corporates
(
  id           int(11) not null auto_increment,
  code         VARCHAR(100) DEFAULT null,
  name         varchar(100) default null,
  PRIMARY KEY (id)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;