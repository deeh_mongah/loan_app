ALTER TABLE loan_application
ADD COLUMN `LOAN_ID` INT NOT NULL AUTO_INCREMENT FIRST,
DROP PRIMARY KEY,
ADD PRIMARY KEY (`LOAN_ID`);
