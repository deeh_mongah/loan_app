ALTER TABLE users add column corporates VARCHAR(200) DEFAULT null;

ALTER TABLE transactions ADD COLUMN repayment_period INT (11),
   ADD COLUMN interest DECIMAL (18,2) DEFAULT 0.00,
   ADD COLUMN  updated_on timestamp null default current_timestamp,
   ADD COLUMN created_by  int(11)        default null,
   ADD COLUMN updated_by  int(11)        default null;