drop table if exists employee_wallet;
create table employee_wallet
(
  id                  int(11)   not null auto_increment,
  employee_id           int(11)   default null,
  corporate_id           int(11)   default null,
  created_on          timestamp null default current_timestamp,
  PRIMARY KEY (id)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;
