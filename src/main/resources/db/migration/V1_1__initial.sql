set foreign_key_checks = 0;

/*table structure for table app_settings */
drop table if exists app_settings;
create table app_settings
(
  id          int(11) not null auto_increment,
  name        varchar(150) default null,
  code        varchar(150) default null,
  flag        varchar(100) default 'settings',
  value       varchar(100) default null,
  description varchar(100) default null,
  PRIMARY KEY (id)
)
  ENGINE = InnoDB
  auto_increment = 4
  DEFAULT CHARSET = utf8;

insert into app_settings (name, code, flag, value, description) values ('Login trials', 'login_trials', 'settings', '5', 'int of allowed login attempts');
insert into app_settings (name, code, flag, value, description) values ('Lock out period', 'lock_out_period', 'settings', '30', 'period in minutes in which a locked account will be inaccessible');
insert into app_settings (name, code, flag, value, description) values ('Reset code expiry period', 'reset_expiry', 'settings', '2','period in hours in which a reset password token is valid');
insert into app_settings (name, code, flag, value, description) values ('Password expiry window','password_expiry_window','settings','90','Password expiry window in days');


drop table if exists user_types;
create table user_types
(
  id   int(11)      not null auto_increment,
  name varchar(100) not null,
  code varchar(100) not null,
  PRIMARY KEY (id)
)
  ENGINE = InnoDB
  DEFAULT charset = utf8;


# Default user_types data
INSERT INTO user_types (name, code) VALUES ('System Admin', 'system-admin');
INSERT INTO user_types (name, code) VALUES ('Corporate Admin', 'corporate-admin');
INSERT INTO user_types (name, code) VALUES ('Employee', 'employee');


# table structure for table audit_trail
drop table if exists audit_trail;
create table audit_trail
(
  id         int(11)   not null auto_increment,
  activity   longtext,
  status     varchar(45)    default null,
  created_on timestamp null default current_timestamp,
  old_values longtext,
  new_values longtext,
  log_type   varchar(100)   default null,
  user_no    int(11)        default null,
  PRIMARY KEY (id)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;


/*table structure for table maker_checker */
drop table if exists maker_checker;
create table maker_checker
(
  id      int(11) not null default '0',
  module  varchar(100)     default null,
  code    varchar(100)     default null,
  enabled bit(1)           default b'0',
  PRIMARY KEY (id)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

/*table structure for table reason_codes */
drop table if exists reason_codes;
create table reason_codes
(
  id          int(11)   not null auto_increment,
  name        varchar(100)   default null,
  description varchar(200)   default null,
  created_on  timestamp null default current_timestamp,
  updated_on  timestamp null default current_timestamp,
  created_by  int(11)        default null,
  updated_by  int(11)        default null,
  flag        varchar(20)    default '1',
  PRIMARY KEY (id)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;


/*table structure for table user_attempts */
drop table if exists user_attempts;
create table user_attempts
(
  id            int(11)   not null auto_increment,
  email         varchar(150)   default null,
  attempts      int(11)        default null,
  last_modified timestamp null default current_timestamp,
  ip            varchar(100)   default null,
  user_agent    varchar(100)   default null,
  country       varchar(100)   default null,
  region        varchar(100)   default null,
  city          varchar(100)   default null,
  PRIMARY KEY (id)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

/*table structure for table users */
drop table if exists users;
create table users
(
  id                         int(11)   not null auto_increment,
  fullname                   varchar(250)   default null,
  email                      varchar(250)   default null,
  phone                      varchar(50)   default null,
  passport                   varchar(50)   default null,
  password                   varchar(100)   default null,
  photo_url                  longtext,
  photo_key                  varchar(200)   default null,
  expiry                     timestamp null default null,
  enabled                    bit(1)         default b'0',
  nonlocked                  bit(1)         default b'0',
  created_on                 timestamp null default current_timestamp,
  updated_on                 timestamp null default current_timestamp,
  deleted_on                 timestamp null default null,
  created_by                 int(11)        default null,
  updated_by                 int(11)        default null,
  email_token                varchar(200)   default null,
  fcm_token                  varchar(600)   default null,
  reset_key                  varchar(200)   default null,
  mobile_verified            bit(1)         default b'0',
  email_verified             bit(1)         default b'0',
  flag                       varchar(20)    default '0',
  edit_data                  longtext,
  reason_code_no             int(11)        default null,
  reason_description         varchar(200)   default null,
  last_time_password_updated timestamp null default null,
  password_never_expires     bit(1)         default b'0',
  reset_req_date             timestamp null default null,
  activation_key             varchar(200)   default null,
  user_type_no               int(11)        default null,
  user_group_no               int(11)        default null,

  PRIMARY KEY (id),
  key reason_code_no (reason_code_no),
  constraint users_fk_1 foreign key (reason_code_no) references reason_codes (id),
  constraint users_fk_2 foreign key (user_type_no) references user_types (id)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

insert into users (fullname, email, phone, password, enabled, nonlocked, mobile_verified,  email_verified, flag, password_never_expires,user_type_no,user_group_no)
values ('Admin Binary', 'admin@live.com','+254720000000', '$2a$10$6E1sz77ND3I9ddL8OJRvquTAJQSaBypABZHNVACMwft8yGhl0lF0e', 1, 0, 1, 1, '1', 1,1,1);


# User login history
drop table if exists login_history;
create table login_history
(
  id           int(11) not null auto_increment,
  user_no      int(11) not null,
  ip           varchar(100) default null,
  user_agent   text,
  imei         varchar(100) default null,
  device_model varchar(100) default null,
  country      varchar(100) default null,
  region       varchar(150) default null,
  city         varchar(150) default null,
  PRIMARY KEY (id)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;
/*
* Permissions Table */
drop table if exists `permissions`;
create table `permissions` (
  `id`         int(11)   not null auto_increment,
  `name`       varchar(100)       default null,
  `app_code`   varchar(100)       default null,
  `created_on` timestamp null     default current_timestamp,
  `updated_on` timestamp null     default current_timestamp,
  `role_no`    int(11)            default null,
  PRIMARY KEY (`ID`),
  KEY `role_no` (`role_no`)
#     CONSTRAINT `permissions_fk_1` FOREIGN KEY (`role_no`) REFERENCES `app_roles` (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;


/*
* User Groups Table */
drop table if exists `user_groups`;
create table `user_groups` (
  `id`                 int(11)   not null auto_increment,
  `name`               varchar(100)       default null,
  `description`        varchar(250)       default null,
  `base_type`          varchar(100)       default null,
  `system_defined`     bit(1)             default 0,

  `flag`               varchar(20)        default '0',
  `edit_data`          longtext,
  `reason_code_no`     int(11)            default null,
  `reason_description` varchar(200)       default null,
  `created_on`         timestamp null     default current_timestamp,
  `updated_on`         timestamp null     default current_timestamp,
  `deleted_on`         timestamp null     default null,
  `created_by`         int(11)            default null,
  `updated_by`         int(11)            default null,

  PRIMARY KEY (`id`),
  KEY `reason_code_no` (`reason_code_no`)
  #   CONSTRAINT `user_groups_fk_1` FOREIGN KEY (`reason_code_no`) REFERENCES `reason_codes` (`ID`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

insert into `user_groups` (`name`, `description`, `flag`, `base_type`, `system_defined`)values ('System-Admin Group', 'System Admin', 1, 'system-admin', 1);
insert into `user_groups` (`name`, `description`, `flag`, `base_type`, `system_defined`)values ('Corporate-Admin', 'Corporate Admin', 1, 'corporate-admin', 1);


drop table if exists `app_roles`;
create table `app_roles` (
  `id`           int(11)   not null auto_increment,
  `name`         varchar(100)       default null,
  `app_code`     varchar(100)       default null,
  `created_at`   timestamp null     default current_timestamp,
  `updated_at`   timestamp null     default current_timestamp,
  `app_function` varchar(200)       default null,
  PRIMARY KEY (`ID`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 7
  DEFAULT CHARSET = utf8;

INSERT INTO app_roles (id, name, app_code, created_at, updated_at, app_function) VALUES (1, 'UserGroups', 'ROLE_USER_GROUP', now(), now(), 'system-admin');
INSERT INTO app_roles (id, name, app_code, created_at, updated_at, app_function) VALUES (2, 'Users', 'ROLE_USERS', now(), now(), 'system-admin');

/*
* Group Permissions Table */
drop table if exists `group_permissions`;
create table `group_permissions` (
  `permission_id` int(11) not null,
  `user_group_id` int(11) not null,
  PRIMARY KEY (`permission_id`, `user_group_id`),
  KEY `user_group_id` (`user_group_id`),
  CONSTRAINT `group_permissions_fk_1` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`ID`),
  CONSTRAINT `group_permissions_fk_2` FOREIGN KEY (`user_group_id`) REFERENCES `user_groups` (`ID`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

set foreign_key_checks = 1;
