set foreign_key_checks = 0;
INSERT INTO permissions (id, name, app_code, created_on, updated_on, role_no) VALUES (55, 'Loans', 'loans', now(), now(), 8);

INSERT INTO app_roles (`id`, `name`, `app_code`, `created_at`, `updated_at`, `app_function`) VALUES (8, 'Loans', 'ROLE_LOANS', now(), now(), 'system-admin,corporate-admin');

INSERT INTO group_permissions (permission_id, user_group_id) VALUES (55, 1);

set foreign_key_checks = 1;