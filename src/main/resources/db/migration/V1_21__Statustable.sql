CREATE TABLE IF NOT EXISTS `status` (
  `ID` INT NOT NULL AUTO_INCREMENT,
  `NAME` VARCHAR(45) NULL,
  PRIMARY KEY (`ID`));

INSERT INTO status(ID, NAME) VALUES (1,"STATUS_PROCESSING");
INSERT INTO status(ID, NAME) VALUES (2,"STATUS_APPROVED_NOT_REPAID");
INSERT  INTO status(ID, NAME) VALUES (3,"STATUS_DENIED");
INSERT INTO status(ID, NAME ) VALUES (4, "STATUS_REPAID")