ALTER TABLE `loan_application`
ADD COLUMN `LOAN_PRODUCT` VARCHAR(45) NULL AFTER `APPLICATION_DATE_IN_MILLISECOND`,
ADD COLUMN `CREATED_ON` VARCHAR(45) NULL AFTER `LOAN_PRODUCT`,
ADD COLUMN `UPDATED_ON` VARCHAR(45) NULL AFTER `CREATED_ON`;

