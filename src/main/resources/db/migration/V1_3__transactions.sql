
# Transactions
drop table if exists transactions;
create table transactions
(
  id                  int(11)   not null auto_increment,
  reference_no        varchar(100)   default null,
  amount              decimal(18, 2) default 0.00,
  employee_no             int(11)   not null,
  transaction_type_no        int(11)   not null,
  flag                varchar(100)   default null,
  created_on          timestamp null default current_timestamp,
  PRIMARY KEY (id)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;


drop table if exists transaction_types;
create table transaction_types
(
  id                  int(11)   not null auto_increment,
  name                varchar(100)   default null,
  code                varchar(100)   default null,
  created_on          timestamp null default current_timestamp,
  PRIMARY KEY (id)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

INSERT INTO transaction_types (name, code) VALUES ('Airtime', 'airtime');
INSERT INTO transaction_types (name, code) VALUES ('Electricity', 'electricity');
INSERT INTO transaction_types (name, code) VALUES ('Deposit', 'deposit');
INSERT INTO transaction_types (name, code) VALUES ('Transfer', 'transfer');
