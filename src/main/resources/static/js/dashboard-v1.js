{
    const startOfWeek = moment().startOf('isoWeek');
    const endOfWeek = moment().endOf('isoWeek');

    let days = [];
    let day = startOfWeek;

    while (day <= endOfWeek) {
        days.push(day.toDate());
        day = day.clone().add(1, 'd');
    }

    if( false ) {
        var chart = Morris.Area({
            element: 'morris-area-chart',
            // data: [
            //     { day: Date.parse( days[0] ), withdrawals: 50,  transfers: 80,  bills: 20  },
            //     { day: Date.parse( days[1] ), withdrawals: 130, transfers: 100, bills: 80  },
            //     { day: Date.parse( days[2] ), withdrawals: 80,  transfers: 60,  bills: 70  },
            //     { day: Date.parse( days[3] ), withdrawals: 70,  transfers: 200, bills: 140 },
            //     { day: Date.parse( days[4] ), withdrawals: 180, transfers: 150, bills: 140 },
            //     { day: Date.parse( days[5] ), withdrawals: 105, transfers: 100, bills: 80  },
            //     { day: Date.parse( days[6] ), withdrawals: 250, transfers: 150, bills: 200 }
            // ],
            data: [0, 0],
            xkey: 'day',
            xLabels: ["day"],
            ykeys: ['withdrawals', 'transfers', 'bills'],
            labels: ['Withdrawals', 'Fund Transfers', 'Bill Payments'],
            pointSize: 3,
            fillOpacity: 0,
            pointStrokeColors: ['#55ce63', '#2962FF', '#2f3d4a'],
            behaveLikeLine: true,
            gridLineColor: '#e0e0e0',
            lineWidth: 3,
            hideHover: 'auto',
            lineColors: ['#55ce63', '#2962FF', '#2f3d4a'],
            resize: true,
            xLabelFormat: function (t) {
                return ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"][t.getDay()]
            },
            redraw: true

        });
    }

    var chart = Morris.Area({
        element: 'morris-area-chart',
        data: [0, 0],
        parseTime : false,
        xkey: 'day',
        xLabels: ["day"],
        ykeys: ['withdrawals', 'transfers', 'bills'],
        labels: ['Withdrawals', 'Fund Transfers', 'Bill Payments'],
        pointSize: 3,
        fillOpacity: 0,
        pointStrokeColors: ['#55ce63', '#2962FF', '#2f3d4a'],
        behaveLikeLine: true,
        gridLineColor: '#e0e0e0',
        lineWidth: 3,
        hideHover: 'auto',
        lineColors: ['#55ce63', '#2962FF', '#2f3d4a'],
        resize: true,
        // xLabelFormat: function (t) {
        //     return ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"][t.getDay()]
        // },
        redraw: true

    });

    // Simple test of our data
    function getRandomInt(max) {
        return Math.floor(Math.random() * Math.floor(max));
    }

    let data = [
        {day: Date.parse(days[0]), withdrawals: 50, transfers: 80, bills: 20},
        {day: Date.parse(days[1]), withdrawals: 130, transfers: 100, bills: 80},
        {day: Date.parse(days[2]), withdrawals: 80, transfers: 60, bills: 70},
        {day: Date.parse(days[3]), withdrawals: 70, transfers: 200, bills: 140},
        {day: Date.parse(days[4]), withdrawals: 180, transfers: 150, bills: 140},
        {day: Date.parse(days[5]), withdrawals: 105, transfers: 100, bills: 80},
        {day: Date.parse(days[6]), withdrawals: 250, transfers: 150, bills: 200}
    ];

    fnInitChart( data );

    console.log("sample data: ", data );

    let fnParseData = o =>{
        let data = [];
        if( Array.isArray( o ) ) {
            o.forEach(function (node, index) {
                let row = {
                    day: Date.parse(days[node.day]),
                    withdrawals: node.withdrawals,
                    transfers: node.transfers,
                    bills: node.bills
                };
                data.push(row);
            });
        }

        //Fill missing gaps
        for (let i = 0; i < 7; i++) {
            if (data[i] === undefined) {
                let row = {day: Date.parse(days[i]), withdrawals: getRandomInt(10000), transfers:getRandomInt(10000), bills: getRandomInt(10000) };
                // let row = {day: Date.parse(days[i]), withdrawals: 0, transfers: 0, bills: 0};
                data.push(row);
            }
        }

        console.log("data: ", data);
    };

    // Create a function that will handle AJAX requests
    let fnChartData = (days, chart) => {
        $.ajax({
            type: "GET",
            url: window.location.pathname
            // data: { days: days }
        })
            .done(function( data ) {
                // When the response to the AJAX request comes back render the chart with new data
                data = fnParseData( data );
                chart.setData( data, true );

                // console.log("data: ", JSON.parse(data) );
                // fnInitChart( JSON.stringify(data) );
            })
            .fail(function() {
                // If there is no communication between the server, show an error
                // alert( "error occured" );
            });
    };

    //fnChartData( 7, chart);


    function fnInitChart( data ){
        var chart = Morris.Area({
            element: 'morris-area-chart',
            data: data,
            parseTime : false,
            xkey: 'day',
            xLabels: ["day"],
            ykeys: ['withdrawals', 'transfers', 'bills'],
            labels: ['Withdrawals', 'Fund Transfers', 'Bill Payments'],
            pointSize: 3,
            fillOpacity: 0,
            pointStrokeColors: ['#55ce63', '#2962FF', '#2f3d4a'],
            behaveLikeLine: true,
            gridLineColor: '#e0e0e0',
            lineWidth: 3,
            hideHover: 'auto',
            lineColors: ['#55ce63', '#2962FF', '#2f3d4a'],
            resize: true,
            // xLabelFormat: function (t) {
            //     console.log( t );
            //     console.log( t.getDay );
            //     return ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"][t.getDay()]
            // }

        });
    }

}