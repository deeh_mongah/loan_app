/**
 * Created by User on 1/9/2020.
 */

{
    //Global ajax setup
    'use strict';
    let token = $("meta[name='_csrf']").attr("content");
    let header = $("meta[name='_csrf_header']").attr("content");
    $(document).ajaxSend(function (e, xhr, options) {
        xhr.setRequestHeader(header, token);
        xhr.setRequestHeader("Accept", "application/json");
    });
    $.get("/permissions/v1/institutions").done(res => {
        // Form Params
        let ui = $('.edit-view');
        let $form = $('form', ui);

        let fnEdit = o => {
            $('input[name="name"]', ui).val(o.name);
            $('input[name="code"]', ui).val(o.code);

            $('input[name="name"]', ui).val(o.name);
            $('input[name="action"]', ui).val('edit');
            $('input[name="id"]', ui).val(o.id);
            ui.modal('show');
        };


        // Initialize table data
        let $activeTable = $('.table-active-records');
        let $newTable = $('.table-new');
        let $editedTable = $('.table-edited');
        let $inactiveTable = $('.table-inactive');
        let $deactivatedTable = $('.table-deactivated');

        const cols = $activeTable.find('thead > tr > th').length;
        const idIndex = cols - 1;

        // Active Table
        let activeTable = utils.dataTable({
            table: $activeTable,
            fnServerParams: (aoData) => {
                aoData.push(
                    {name: 'fetch-table', value: '1'},
                    {name: 'transactionType', value: $('.transaction-types > .title-btn').data('index')},
                );
            },
            fnRowCallback: (nRow, aData) => {
                //Append action buttons
                const recordId = aData[idIndex];
                const recordReference = aData[0];
                $(nRow).children('td:eq( -1 )').html(
                    utils.makerchecker.fnRowActions(recordId, recordReference, 1)
                );

                //Edit events
                $('.edit', nRow).click(function () {
                    utils.makerchecker.fnEdit($(this).data('index'), fnEdit);
                });

                //Deactivate events
                $('.deactivate', nRow).click(function () {
                    utils.makerchecker.fnDeactivate($(this).data('index'), $(this).data('name'));
                });
            }

        });

        // New Table
        let newTable = utils.dataTable({
            table: $newTable,
            fnServerParams: (aoData) => {
                aoData.push(
                    {name: 'fetch-table', value: '0'},
                    {name: 'transactionType', value: $('.transaction-types > .title-btn').data('index')},
                );
            },
            fnRowCallback: (nRow, aData) => {
                //Append action buttons
                const recordId = aData[idIndex];
                const recordReference = aData[0];
                $(nRow).children('td:eq( -1 )').html(
                    utils.makerchecker.fnRowActions(recordId, recordReference, 0)
                );

                //Handle events
                $('.approve-new', nRow).click(function () {
                    utils.makerchecker.flagRecords($(this).data('name'), $(this).data('index'), "approve-new");
                });
                $('.decline-new', nRow).click(function () {
                    utils.makerchecker.flagRecords($(this).data('name'), $(this).data('index'), "decline-new", "warning");
                });
            }

        });

        // Edited Table
        let editedTable = utils.dataTable({
            table: $editedTable,
            fnServerParams: (aoData) => {
                aoData.push(
                    {name: 'fetch-table', value: '2'},
                    {name: 'transactionType', value: $('.transaction-types > .title-btn').data('index')},
                );
            },
            fnRowCallback: (nRow, aData) => {
                //Append action buttons
                const recordId = aData[idIndex];
                const recordReference = aData[0];
                $(nRow).children('td:eq( -1 )').html(
                    utils.makerchecker.fnRowActions(recordId, recordReference, 2)
                );

                //Handle events
                $('.vedit', nRow).click(function () {
                    utils.makerchecker.fnViewChanges(recordId);
                });
            }

        });

        // Deactivated Records
        let deactivatedTable = utils.dataTable({
            table: $deactivatedTable,
            fnServerParams: (aoData) => {
                aoData.push(
                    {name: 'fetch-table', value: '3'},
                    {name: 'transactionType', value: $('.transaction-types > .title-btn').data('index')},
                );
            },
            fnRowCallback: (nRow, aData) => {
                //Append action buttons
                const recordId = aData[idIndex];
                const recordReference = aData[0];
                $(nRow).children('td:eq( -1 )').html(
                    utils.makerchecker.fnRowActions(recordId, recordReference, 3)
                );

                //Handle events
                $('.vdeactivation', nRow).click(function () {
                    utils.makerchecker.fnViewReasons(recordId);
                });
            }

        });

        // Inactive Records
        let inactiveTable = utils.dataTable({
            table: $inactiveTable,
            fnServerParams: (aoData) => {
                aoData.push(
                    {name: 'fetch-table', value: '4'},
                    {name: 'transactionType', value: $('.transaction-types > .title-btn').data('index')},
                );
            },
            fnRowCallback: (nRow, aData) => {
                //Append action buttons
                const recordId = aData[idIndex];
                const recordReference = aData[0];
                $(nRow).children('td:eq( -1 )').html(
                    utils.makerchecker.fnRowActions(recordId, recordReference, 4)
                );

                //Handle events
                $('.delete', nRow).click(function () {
                    utils.makerchecker.flagRecords($(this).data('name'), recordId, "delete", "warning");
                });
                $('.activate', nRow).click(function () {
                    utils.makerchecker.flagRecords($(this).data('name'), recordId, "activate");
                });
            }

        });

        // Function to reload data
        let reloadDataFn = () => {
            activeTable.fnDraw();
            newTable.fnDraw();
            editedTable.fnDraw();
            deactivatedTable.fnDraw();
            inactiveTable.fnDraw();
        };

        // Instruct maker-checker handler to use this reload function
        utils.makerchecker.options.refreshFn = reloadDataFn;

        //When to refresh table info
        $('[data-action="refresh"]').click(function () {
            setTimeout(function () {
                reloadDataFn();
            });
        });

        // Define the handler for filtering
        $('.btn-group > .dropdown-menu button').click(function (e) {
            e.preventDefault();
            let el = $(this).parents()[2];
            $('.title-btn', el).html($(this).html());
            $('.title-btn', el).data('index', $(this).data('index'));

            // Reload data
            reloadDataFn();
        });
        // Initialize form validation params
        FormValidation.Validator.mobile = utils.mobileValidator;

        // Validate form being sending data to server
        $form.formValidation({
            framework: 'bootstrap',
            fields: {
                name: {
                    verbose: true,
                    validators: {
                        notEmpty: {message: 'Name is required'},
                        regexp: {
                            regexp: /^[\w+](?:[\w+]|[\s](?=[\w+]))+$/,
                            message: 'Enter a valid name'
                        }
                    }
                },
                phone: {validators: {mobile: {message: 'Invalid mobile number'}}},
                contacts: {validators: {mobile: {message: 'Invalid mobile number'}}},
                groupNo: {validators: {notEmpty: {message: 'Fee group is required'}}},
                feeOriginNo: {validators: {notEmpty: {message: 'Fee origin is required'}}},
                paymentMode: {validators: {notEmpty: {message: 'Payment mode is required'}}},
                saccoAmount: {validators: {notEmpty: {message: 'SACCO amount is required'}}},
                binaryAmount: {validators: {notEmpty: {message: 'Binary amount is required'}}},
                saccoRatio: {validators: {notEmpty: {message: 'SACCO ratio is required'}}},
                binaryRatio: {validators: {notEmpty: {message: 'Binary ratio is required'}}},
            }
        }).on('success.fv.form', function (e) {
            e.preventDefault();
            utils.submitForm($(this).serializeArray(), ui);
        });

        //House keep
        ui.on('hidden.bs.modal', function () {
            utils.houseKeep(ui);
            //always have this default action
            $('input[name="action"]', ui).val('new');
        });

        let viewActiveFaida YetuPermission = res.includes("View Active Group");
        let viewNewFaida YetuPermission = res.includes("View New Group");
        let viewEditedFaida YetuPermission = res.includes("View Edited Group");
        let viewDeactivedRequestFaida YetuPermission = res.includes("View Deactivated Group");
        let viewInactivegroupsFaida YetuPermission = res.includes("View Inactive Group");
        let createNewFaida Yetu =res.includes("Create Group");

        if (viewActiveFaida YetuPermission === false) {


            $(".active-Faida Yetu-view").toggleClass("d-none", true);

        }else{
            $(".active-Faida Yetu-view").toggleClass("d-none", false);

        }
        if(viewNewFaida YetuPermission ===false){
            $(".new-Faida Yetu-view").toggleClass("d-none", true);

        }else{
            $(".new-Faida Yetu-view").toggleClass("d-none", false);

        }
        if(viewEditedFaida YetuPermission=== false){
            $(".edited-Faida Yetu-view").toggleClass("d-none", true);

        }else{

            $(".edited-Faida Yetu-view").toggleClass("d-none", false);
        }
        if(viewDeactivedRequestFaida YetuPermission=== false){
            $(".deactivated-Faida Yetu-view").toggleClass("d-none", true);

        }else{

            $(".deactivated-Faida Yetu-view").toggleClass("d-none", false);
        }
        if(viewInactivegroupsFaida YetuPermission=== false){
            $(".inactive-Faida Yetu-view").toggleClass("d-none", true);

        }else{

            $(".inactive-Faida Yetu-view").toggleClass("d-none", false);
        }
        if(createNewFaida Yetu === false){
            $(".create-Faida Yetu-view").toggleClass("d-none", true);

        }else{

            $(".create-Faida Yetu-view").toggleClass("d-none", false);
        }

    });

}
