{
    $.get("/permissions/v1/institutions").done(res => {
        let oTable = utils.dataTable({
            table: $(".table")
        });

        //When to refresh table info
        $('[data-action="refresh"]').click(function () {
            setTimeout(function () {
                oTable.fnDraw();
            });
        });

        let auditLogs=res.includes("Audit Log");

        console.log(auditLogs);

        if (auditLogs === false) {


            $(".audit-log").toggleClass("d-none", true);

        }else{
            $(".audit-log").toggleClass("d-none", false);

        }
    });

}