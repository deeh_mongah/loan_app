{
    //Global ajax setup
    'use strict';
    let token = $("meta[name='_csrf']").attr("content");
    let header = $("meta[name='_csrf_header']").attr("content");
    $(document).ajaxSend(function (e, xhr, options) {
        xhr.setRequestHeader(header, token);
        xhr.setRequestHeader("Accept", "application/json");
    });


    let ui = $('.edit-view');
    let fnEdit = o =>{
        $('[name="id"]').val(o.id);
        $('[name="module"]').val(o.module);
        $('[name="enabled"]', ui).val(o.enabled);
        $('[name="status"]', ui).prop('checked', o.enabled);
        $('[name="action"]', ui).val("edit");
        ui.modal("show");
    };

    let fnActions = o =>{
        return '<button  type="button" class="btn btn-success btn-xs edit" data-index="' + o + '">Edit</button >';
    };

    let oTable = utils.dataTable({
        table: $(".table"),
        fnRowCallback: function (nRow, aData) {
            //Process data
            let k = 0;
            $(aData).each(function (i) {
                k++;
                if (aData.length === k)
                    $(nRow).children('td:eq(-1)').html( fnActions(aData[i]) );
                else
                    aData[k] === true ? $(nRow).find('td:eq(' + k + ')').text('Yes') : $(nRow).find('td:eq(' + k + ')').text('No');
            });

            //Handle table events
            $(".edit", nRow).click(function (e) {
                let index = $(this).data("index");
                utils.http
                    .jsonRequest(".panel", {'action': 'fetch-record', 'index': index})
                    .done(function (o) {
                        if ("00" === o.status) fnEdit(o);
                        else utils.alert.error(o.message);
                    });
            });
        }
    });

    let fnReloadTables = ()=> {
        oTable.fnDraw();
    };

    //Refresh call within helpers
    utils.makerchecker.options.refreshFn = fnReloadTables;

    //When to refresh table info
    $('[data-action="refresh"]').click(function () {
        setTimeout(function () {
            fnReloadTables();
        });
    });

    $("form", ui).submit(function (e) {
        e.preventDefault();
        $('[name="enabled"]', ui).val($('[name="status"]', ui).is(":checked"));
        utils.submitForm($(this).serializeArray(), ui);
    });

}