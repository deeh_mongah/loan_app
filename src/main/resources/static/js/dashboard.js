{
    const startOfWeek = moment().startOf('isoWeek');
    const endOfWeek = moment().endOf('isoWeek');

    let days = [];
    let day = startOfWeek;

    while (day <= endOfWeek) {
        days.push(day.toDate());
        day = day.clone().add(1, 'd');
    }

    Morris.Area({
        element: 'morris-area-chart',
        data: [
            {day: Date.parse(days[0]), withdrawals: 50, transfers: 80, bills: 20},
            {day: Date.parse(days[1]), withdrawals: 130, transfers: 100, bills: 80},
            {day: Date.parse(days[2]), withdrawals: 80, transfers: 60, bills: 70},
            {day: Date.parse(days[3]), withdrawals: 70, transfers: 200, bills: 140},
            {day: Date.parse(days[4]), withdrawals: 180, transfers: 150, bills: 140},
            {day: Date.parse(days[5]), withdrawals: 105, transfers: 100, bills: 80},
            {day: Date.parse(days[6]), withdrawals: 250, transfers: 150, bills: 200}
        ],
        xkey: 'day',
        xLabels: ["day"],
        ykeys: ['withdrawals', 'transfers', 'bills'],
        labels: ['Withdrawals', 'Fund Transfers', 'Bill Payments'],
        pointSize: 3,
        fillOpacity: 0,
        pointStrokeColors: ['#55ce63', '#2962FF', '#2f3d4a'],
        behaveLikeLine: true,
        gridLineColor: '#e0e0e0',
        lineWidth: 3,
        hideHover: 'auto',
        lineColors: ['#55ce63', '#2962FF', '#2f3d4a'],
        resize: true,
        xLabelFormat: function (t) {
            return ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"][t.getDay()]
        }

    });
}