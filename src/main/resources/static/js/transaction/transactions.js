{
    $.get("/permissions/v1/institutions").done(res => {
        let fnLabels = function( status ){
            let color;
            switch( status ){
                case "Approved":
                    color = "success";
                    break;
                case "Declined":
                    color = "primary";
                    break;
                case "New":
                    color = "warning";
                    break;
                case "Paid":
                    color = "success";
                    break;
                case "Overdue":
                    color = "danger";
                    break;
            }
            return "<span class='badge badge-"+ color +"'> " + status +" </span>";
        };

        let statusIndex = 5;
        $(window).on('tableDrawing', e => {
            let row = $(e.tableRow);
            let data = e.aData;
            let status = data[ statusIndex ];
            $( row ).children('td:eq('+ statusIndex +')').html( fnLabels( status ) );
        });

        let transactions = res.includes("Transactions");


        if (transactions === false) {


            $(".view-transactions").toggleClass("d-none", true);

        } else {
            $(".view-transactions").toggleClass("d-none", false);

        }
    });

}