/**
 * Created by Deeh on 9/12/2019.
 */
(function($, window){
    'use strict';
    var token = $("meta[name='_csrf']").attr("content"),
        header = $("meta[name='_csrf_header']").attr("content");
    $(document).ajaxSend(function (e, xhr, options) {
        xhr.setRequestHeader(header, token);
        xhr.setRequestHeader("Accept", "application/json");
    });

        $(function () {



            let ui = $(".edit-view"), panel = $(".content-body"), oTable;

            //Initialize table information
            let cols = $(".table-mobile-users").find('thead > tr > th').length;
            let idIndex = cols - 1;
            let $editedTables = $('.table-mobile-users');
            let $$ = $(document);

            // // Edit handler
            // let fnEdit = function (o) {
            //
            //     $('input[name="firstName"]', ui).val(o.firstName);
            //     $('input[name="middleName"]', ui).val(o.middleName);
            //     $('input[name="surname"]', ui).val(o.surname);
            //     $('input[name="email"]', ui).val(o.email);
            //     $('input[name="phone"]', ui).val(o.phone);
            //
            //     $('input[name="action"]', ui).val('edit');
            //     $('input[name="id"]', ui).val(o.id);
            //     // $('.user-type', ui).hide();
            //     ui.modal('show');
            // };
            //
            // // Initialize table data
            // utils.makerchecker.initTables( fnEdit );

            //When to refresh table info
            $('[data-action="refresh"]').click( function(){
                setTimeout(function () {
                    utils.makerchecker.refreshMkTables();
                });
            });

                function actions(o, status, ident)
            {
                let actions = '<div class="btn-group"><button type="button" class="btn btn-primary btn-square btn-sm">Options</button>';
                actions += '<button type="button" class="btn btn-info dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>';
                actions += '<div class="dropdown-menu">';
                // var actions = "";

                // actions += '<a href="javascript:void(0)" class="dropdown-item edit" data-index="' + o + '" data-name="' + ident + '"><i class="icon-check2"></i> Edit </a>';
                actions += '<a href="javascript:void(0)" class="dropdown-item activate" data-index="' + o + '" data-name="' + ident + '"><i class="icon-check2"></i> Activate </a>';
                actions += '<a href="javascript:void(0)" class="dropdown-item suspended" data-index="' + o + '" data-name="' + ident + '"><i class="icon-cross2"></i>Suspend </a>';
                actions += '<a href="javascript:void(0)" class="dropdown-item disabled" data-index="' + o + '" data-name="' + ident + '"><i class="icon-cross2"></i> Disable </a>';
                // actions += '<a href="javascript:void(0)" class="dropdown-item vedit" data-index="' + o + '" data-name="' + ident + '"><i class="icon-check2"></i> View Changes </a>';


                return actions;
            }


            /**
             * Define the colors for each different by its glag
             *
             * @param status
             * @returns {string}
             */
            let fnLabels = function( status ){
                let color;
                switch( status ){
                    case "Active":
                        color = "success";
                        break;
                    case "Suspended":
                        color = "danger";
                        break;
                }

                let row = '<span class="badge badge-default badge-' + color + '">' + status + '</span>';
                return row;
            };
            const parent =$('.parent-panel')
            const child = $(".child-panel");
            const tableParent = $(".table-mobile-users");
            let labelIndex = 5;
            oTable = utils.dataTable({
                table: $(".table-mobile-users"),

                fnServerParams: (aoData) => {
                    aoData.push(
                        {name: 'channel', value: $('.channel > .title-btn', parent).data('index') },
                        {name: 'status', value: $('.status > .title-btn', parent).data('index') },
                        {name: 'branches', value: $('.branches > .title-btn', child).data('index') },

                    );
                    console.log("work","working");
                },


                fnRowCallback: function (nRow, aData) {

                    $(nRow).children('td:eq(-1)').html(actions(aData[idIndex]));
                    $(nRow).children('td:eq( '+labelIndex+')').html( fnLabels( aData[ labelIndex ]) );

                    // $(".edit", nRow).click(function () {
                    //     let index = $(this).data("index");
                    //     utils.http
                    //         .jsonRequest(panel, {'action': 'fetch-record', 'index': index})
                    //         .done(function (o) {
                    //             if ("00" === o.status) {
                    //                 fnEdit(o);
                    //             }
                    //             else utils.alert.error(o.message);
                    //         });
                    // });


                    $(".suspended", nRow).click(function () {
                        let index = $(this).data("index");
                        swal({
                            title: '',
                            text: 'Do you want to suspend this mobile user?',
                            type: (typeof type === 'undefined') ? 'info' : type,
                            showCancelButton: true,
                            closeOnConfirm: false,
                            showLoaderOnConfirm: true
                        }, function (isConfirm) {
                            if (isConfirm)
                                setTimeout(function () {
                                    utils.http
                                        .jsonRequest(panel, {'action': 'suspend', 'index': index})
                                        .done(function (o) {
                                            if ("00" === o.status) {
                                                utils.alert.success(o.message);
                                                oTable.fnDraw();
                                            }
                                            else utils.alert.error(o.message);
                                        });
                                }, 200);
                        });
                    });

                    $(".activate", nRow).click(function () {
                        let index = $(this).data("index");
                        swal({
                            title: '',
                            text: 'Do you want to activate this mobile user?',
                            type: (typeof type === 'undefined') ? 'info' : type,
                            showCancelButton: true,
                            closeOnConfirm: false,
                            showLoaderOnConfirm: true
                        }, function (isConfirm) {
                            if (isConfirm)
                                setTimeout(function () {
                                    utils.http
                                        .jsonRequest(panel, {'action': 'activate', 'index': index})
                                        .done(function (o) {
                                            if ("00" === o.status) {
                                                utils.alert.success(o.message);
                                                oTable.fnDraw();
                                            }
                                            else utils.alert.error(o.message);
                                        });
                                }, 200);
                        });
                    });

                    $(".disabled", nRow).click(function () {
                        let index = $(this).data("index");
                        swal({
                            title: '',
                            text: 'Do you want to disable this mobile user?',
                            type: (typeof type === 'undefined') ? 'info' : type,
                            showCancelButton: true,
                            closeOnConfirm: false,
                            showLoaderOnConfirm: true
                        }, function (isConfirm) {
                            if (isConfirm)
                                setTimeout(function () {
                                    utils.http
                                        .jsonRequest(panel, {'action': 'disable', 'index': index})
                                        .done(function (o) {
                                            if ("00" === o.status) {
                                                utils.alert.success(o.message);
                                                oTable.fnDraw();
                                            }
                                            else utils.alert.error(o.message);
                                        });
                                }, 200);
                        });
                    });


                },

            });

            console.log( "columnCount: ", cols );

            $$.on('click', '.btn-info', () => {
                if (!parent.is(":visible")) {
                    parent.show();
                    child.toggleClass("d-none");


                }
            });


            $('.btn-success', child).click(function () {
                if (child.is(":visible")) {
                    childTable.fnDraw();
                }
            });

            $('.btn-success', parent).click(function () {
                if (parent.is(":visible")) {
                    oTable.fnDraw();
                }
            });

            //When updating drop-down filters of parent panel
            $(".filter a", parent).click(function (e) {
                e.preventDefault();
                e = $(this).parents()[1];
                let css = $(this).data("filter");
                $('.title-btn', e).html($(this).html());
                $('.title-btn', e).data('index', $(this).data('index'));

                console.log("css ->", css);

                $("." + css + ' > .title-btn', child).html($(this).html());
                $("." + css + ' > .title-btn', child).data('index', $(this).data('index'));
                oTable.fnDraw();
            });

            $('form', ui).formValidation({
                framework: 'bootstrap',
                fields: {
                    name: {validators: {notEmpty: {message: 'Name is required'}}},
                    description: {validators: {notEmpty: {message: 'Description is required'}}}
                }
            }).on('success.form.fv', function (e) {
                e.preventDefault();
                ui.hide();
                utils.http.jsonRequest(panel, $(this).serializeArray())
                    .done(function (o) {
                        if (o.status === "00") {
                            utils.alert.success(o.message);
                            oTable.fnDraw();
                            ui.modal("hide");
                        }
                        else {
                            utils.alert.error(o.message, function () {
                                ui.show();
                            });
                        }
                    });
            });

            //House keep
            ui.on('hidden.bs.modal', function () {
                utils.houseKeep(ui);
                $("form", ui).data('formValidation').resetForm();
                //always have this default action
                $('input[name="action"]', ui).val('new');
            });

        })

})(jQuery, window);

