(function($, window){
    "use strict";

    FormValidation.Validator.password = utils.passwordValidator;
    let passwordView = $(".password-view");
    let profileView = $(".profile-view");

    $(".profile").click( function(){
        profileView.toggleClass("d-none", false);
        passwordView.toggleClass("d-none", true);
    });

    $(".password").click( function(){
        profileView.toggleClass("d-none", true);
        passwordView.toggleClass("d-none", false);
    });

    passwordView.formValidation({
        framework: 'bootstrap',
        fields:{
            currentPassword : { validators: { notEmpty:{ message: "Password is required"}, password: { message: "Invalid password"}}},
            newPassword : { validators: { notEmpty:{ message: "Password is required"}, password: { message: "Invalid password"}}},
            confirmPassword: { validators: { identical:{ field :"newPassword", message:"The password and its confirm do not match"}}}
        }
    });

    //Handle profile photo
    let $imagePanel = $(".image-panel");
    $(".change-photo").click( ()=>{
        $('.file-input', $imagePanel).trigger('click');
    });

    $('.file-input', $imagePanel).change(function(e){
        // Show progress
        utils.toggleLoading($imagePanel, "show");

        // Place the image
        loadImage (
            e.target.files[0],
            function (canvas) {
                // Hide progress
                utils.toggleLoading($imagePanel, "hide");

                // Set the image
                $('.uploaded', $imagePanel).html('<img src="'+canvas.toDataURL()+'" class="height-100" />');
                $('textarea[name="avatar"]', $imagePanel).html(canvas.toDataURL());
            },
            { canvas:true, maxWidth:256, maxHeight:256 }
        );
    });


})(jQuery, window);