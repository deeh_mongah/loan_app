(function($, window){
    //Global ajax setup
    'use strict';
    let token = $("meta[name='_csrf']").attr("content");
    let header = $("meta[name='_csrf_header']").attr("content");
    $(document).ajaxSend(function (e, xhr, options) {
        xhr.setRequestHeader(header, token);
        xhr.setRequestHeader("Accept", "application/json");
    });
    $.get("/permissions/v1/institutions").done(res => {
        // Page global params
        let ui = $('.edit-view');
        let $form = $('form', ui);
        let presentPermissions;

        // Edit handler
        let fnEdit = function ( o ) {
            if(o === undefined){
                return "undefined Value !!"
            }
            $('input[name="fullName"]', ui).val(o.fullName)
            $('input[name="passport"]', ui).val(o.passport);
            $('input[name="phone"]', ui).val(o.phone);
            $('input[name="email"]', ui).val(o.email);
            $('select[name="userTypeNo"]', ui).val(o.userTypeNo);
            $('select[name="userGroupNo"]', ui).val(o.userGroupNo);

            $('input[name="action"]', ui).val('edit');
            $('input[name="id"]', ui).val(o.id);
            // $('.user-type', ui).hide();
            ui.modal('show');
        };

        // Initialize table data
        utils.makerchecker.initTables( fnEdit() );

        //When to refresh table info
        $('[data-action="refresh"]').click( function(){
            setTimeout(function () {
                utils.makerchecker.refreshMkTables();
            });
        });

        // Initialize form validation params
        FormValidation.Validator.mobile = utils.mobileValidator;
        $form
            .formValidation({
                framework: 'bootstrap',
                fields: {
                    username: {validators: {notEmpty: {message: 'Username is required'}}},
                    phone: {validators: {mobile: {message: 'Invalid mobile number'}}},
                    userGroupNo: {validators: {notEmpty: {message: 'User group is required'}}},
                    userTypeNo: {validators: {notEmpty: {message: "User type is required."}}},
                    email: {
                        verbose: false,
                        validators: {
                            notEmpty: {message: "Email is required"},
                            emailAddress: {message: 'The email address is not valid'},
                            stringLength: {max: 512, message: 'Cannot exceed 512 characters'}
                        }
                    }
                }
            })
            .on('success.fv.form', function (e) {
                e.preventDefault();
                utils.submitForm($(this).serializeArray(), ui);
            });

        //House keep
        ui.on('hidden.bs.modal', function () {
            utils.houseKeep(ui);
            //always have this default action
            $('.user-type', ui).show();
            $('input[name="action"]', ui).val('new');
        });

        let viewActiveUserPermission = res.includes("View Active Users");
        let viewNewUserPermission = res.includes("View New Users");
        let viewEditedUserPermission = res.includes("View Edited Users");
        let viewDeactivedRequestUserPermission = res.includes("View Deactivated Users");
        let viewInactivePermission = res.includes("View Inactive Users");
        let createNewUser=res.includes("Create Users");

        if (viewActiveUserPermission === false) {


            $(".active-user-view").toggleClass("d-none", true);

        }else{
            $(".active-user-view").toggleClass("d-none", false);

        }
        if(viewNewUserPermission ===false){
            $(".new-user-view").toggleClass("d-none", true);

        }else{
            $(".new-user-view").toggleClass("d-none", false);

        }
        if(viewEditedUserPermission=== false){
            $(".edited-user-view").toggleClass("d-none", true);

        }else{

            $(".edited-user-view").toggleClass("d-none", false);
        }
        if(viewDeactivedRequestUserPermission=== false){
            $(".deactivated-user-view").toggleClass("d-none", true);

        }else{

            $(".deactivated-user-view").toggleClass("d-none", false);
        }
        if(viewInactivePermission=== false){
            $(".inactive-user-view").toggleClass("d-none", true);

        }else{

            $(".inactive-user-view").toggleClass("d-none", false);
        }
        if(createNewUser=== false){
            $(".create-user-view").toggleClass("d-none", true);

        }else{

            $(".create-user-view").toggleClass("d-none", false);
        }

    });

})(jQuery, window);