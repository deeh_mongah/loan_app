(function($, window){
    //Global ajax setup
    'use strict';
    let token = $("meta[name='_csrf']").attr("content");
    let header = $("meta[name='_csrf_header']").attr("content");
    $(document).ajaxSend(function (e, xhr, options) {
        xhr.setRequestHeader(header, token);
        xhr.setRequestHeader("Accept", "application/json");
    });
    $.get("/permissions/v1/institutions").done(res => {
        // Page global params
        let ui = $('.edit-view');
        let $form = $('form', ui);
        let presentPermissions;

        let $activeTable = $('.table-active-records');
        let $newTable = $('.table-new');
        let $editedTable = $('.table-edited');
        let $deactivatedTable = $('.table-deactivated');

        const cols = $activeTable.find('thead > tr > th').length;
        const idIndex = cols - 1;

        let activeTable = utils.dataTable({

            table: $activeTable,
            "destroy": true,
            'bDestory':true,
            fnServerParams: (aoData) => {
                aoData.push({name: 'fetch-table', value: '1'});
            },
            fnRowCallback: (nRow, aData) => {
                //Append action buttons
                const recordId = aData[idIndex];

                $(nRow).children('td:eq( -1 )').html(
                    utils.makerchecker.fnRowActions(recordId, undefined, 5)
                );

                //Edit events
                $('.approve', nRow).click(function () {
                    utils.makerchecker.approveLoan($(this).data('name'),$(this).data('index'),"approve");
                });

                //Deactivate events
                $('.decline', nRow).click(function () {
                    utils.makerchecker.approveLoan($(this).data('name'),$(this).data('index'),"decline");
                });
            }

        });

        const colsN = $activeTable.find('thead > tr > th').length;
        const idIndexN = colsN - 1;

        // New Table
        let newTable = utils.dataTable({
            table: $newTable,
            "destroy": true,
            'bDestory':true,
            fnServerParams: (aoData) => {
                aoData.push({'name':'fetch-table', 'value':'0'})
            },
            fnRowCallback: (nRow, aData) => {
                //Append action buttons
                const recordId = aData[idIndexN];
                $(nRow).children('td:eq( -1 )').html(
                    utils.makerchecker.fnRowActions(recordId, undefined, 6)
                );

                //Handle events
                $('.repaid', nRow).click(function () {
                    utils.makerchecker.approveLoan($(this).data('name'),$(this).data('index'),"repaid");
                });
                $('.warn', nRow).click(function () {
                    utils.makerchecker.flagRecords($(this).data('name'), $(this).data('index'), "decline-new", "warning");
                });
            }

        });
        let editedTable = utils.dataTable({

            table: $editedTable,
            "destroy": true,
            'bDestory':true,
            fnServerParams: (aoData) => {
                aoData.push({name: 'fetch-table', value: '3'});
            },

        });

        let deactivatedTable = utils.dataTable({

            table: $deactivatedTable,
            "destroy": true,
            'bDestory':true,
            fnServerParams: (aoData) => {
                aoData.push({name: 'fetch-table', value: '4'});
            },

        });



        let reloadDataFn = () => {
            activeTable.fnDraw();
            newTable.fnDraw();
            editedTable.fnDraw();
            deactivatedTable.fnDraw();

        };


        //When to refresh table info
        $('[data-action="refresh"]').click( function(){
            setTimeout(function () {
                reloadDataFn();
            });
        });

        // Initialize form validation params
        FormValidation.Validator.mobile = utils.mobileValidator;
        $form
            .formValidation({
                framework: 'bootstrap',
                fields: {
                    username: {validators: {notEmpty: {message: 'Username is required'}}},
                    phone: {validators: {mobile: {message: 'Invalid mobile number'}}},
                    userGroupNo: {validators: {notEmpty: {message: 'User group is required'}}},
                    userTypeNo: {validators: {notEmpty: {message: "User type is required."}}},
                    email: {
                        verbose: false,
                        validators: {
                            notEmpty: {message: "Email is required"},
                            emailAddress: {message: 'The email address is not valid'},
                            stringLength: {max: 512, message: 'Cannot exceed 512 characters'}
                        }
                    }
                }
            })
            .on('success.fv.form', function (e) {
                e.preventDefault();
                utils.submitForm($(this).serializeArray(), ui);
            });

        //House keep
        ui.on('hidden.bs.modal', function () {
            utils.houseKeep(ui);
            //always have this default action
            $('.user-type', ui).show();
            $('input[name="action"]', ui).val('new');
        });
        let viewActiveUserPermission = res.includes("View Active Users");
        let viewNewUserPermission = res.includes("View New Users");
        let viewEditedUserPermission = res.includes("View Edited Users");
        let viewDeactivedRequestUserPermission = res.includes("View Deactivated Users");
        let viewInactivePermission = res.includes("View Inactive Users");
        let createNewUser=res.includes("Create Users");

        if (viewActiveUserPermission === false) {


            $(".active-user-view").toggleClass("d-none", true);

        }else{
            $(".active-user-view").toggleClass("d-none", false);

        }
        if(viewNewUserPermission ===false){
            $(".new-user-view").toggleClass("d-none", true);

        }else{
            $(".new-user-view").toggleClass("d-none", false);

        }
        if(viewEditedUserPermission=== false){
            $(".edited-user-view").toggleClass("d-none", true);

        }else{

            $(".edited-user-view").toggleClass("d-none", false);
        }
        if(viewDeactivedRequestUserPermission=== false){
            $(".deactivated-user-view").toggleClass("d-none", true);

        }else{

            $(".deactivated-user-view").toggleClass("d-none", false);
        }
        if(viewInactivePermission=== false){
            $(".inactive-user-view").toggleClass("d-none", true);

        }else{

            $(".inactive-user-view").toggleClass("d-none", false);
        }
        if(createNewUser=== false){
            $(".create-user-view").toggleClass("d-none", true);

        }else{

            $(".create-user-view").toggleClass("d-none", false);
        }

    });

})(jQuery, window);